# -*- coding: utf-8 -*-

"""
MARFA Breakthrough Plotter
==========================

Authors: Jordi Sanglas (jordi.sanglas@amphos21.com)

This module provides functions to simplify reading and plotting MARFA-SP
results. Note that matplotlib is required in order to use it.

Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (FINLAND).
All rights reserved. 

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

  Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.

  Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

  Neither the names of the copyright holders, nor the names of its contributors may be
  used to endorse or promote products derived from this software without
  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import matplotlib.pyplot as plt

class BTData:
    """
    Breakthrough data class, containing all the information from a Breakthrough_Mo
    or Breakthrough_Bq file.
    """
    # Empty

class BreakthroughPlotterException(Exception):
    """
    Custom breakthrough plotter exception.
    """
    # Empty
    
def read_breakthrough(file_path):
    """
    Reads breakthrough data from a MARFA file (Breakthrough_Mo or Breakthrough_Bq).
    Return value is a dictionary with the data of the results.
    """
    
    cumul_bt = []
    insta_bt = []
    time = []
    earliest_insta = []
    earliest_cumul = []
    comment_found = False
    earliest_insta_found = False
    earliest_cumul_found = False
    data_i = 0
    units = ""
    names = []
    nonzero_data = [] # Is the data for this nuclide non-zero somewhere?
    
    with open(file_path,"r") as bt_file:
        for line in bt_file:
            index = line.find("!")
            if index!=-1:
                if not comment_found: #Start of a new nuclide
                    cumul_bt.append([])
                    insta_bt.append([])
                    time.append([])
                    
                    if len(time)>1:
                        if not earliest_cumul_found:
                            nonzero_data.append(False)
                        else:
                            nonzero_data.append(True)
                    
                    earliest_insta_found = False
                    earliest_cumul_found = False
                    data_i = 0
                    comment_found = True
                
                if "(Mole)" in line:
                    units = "mole"
                if "(Bq)" in line:
                    units = "Bq"
                
                if "Name=" in line:
                    split_line = line.split()
                    name_i = split_line.index("Name=")
                    names.append(split_line[name_i+1])
                    
            elif line.strip()!="":
                comment_found = False
                split_line = line.split()
                time[-1].append(float(split_line[0]))
                cumul_bt[-1].append(float(split_line[1]))
                insta_bt[-1].append(float(split_line[2]))
                if float(split_line[1])>0. and not earliest_cumul_found:
                    earliest_cumul_found = True
                    earliest_cumul.append(time[-1][-1])
                if float(split_line[2])>0. and not earliest_insta_found:
                    earliest_insta_found = True
                    earliest_insta.append(time[-1][-1])
                data_i += 1
                
    if len(time)>1 and not earliest_cumul_found:
        nonzero_data.append(False)
    else:
        nonzero_data.append(True)
        
    output = BTData()
    
    output.times = []
    output.cumul_bt = []
    output.insta_bt = []
    
    for i in range(len(time)):
        output.times.append(time[i])
        output.cumul_bt.append(cumul_bt[i])
        output.insta_bt.append(insta_bt[i])
    
    if len(earliest_cumul)==0:
        raise BreakthroughPlotterException("No particles of any nuclide arrived!")
    
    output.earliest_cumul = min(earliest_cumul)
    output.earliest_insta = min(earliest_insta)
    
    max_times = [time[i][-1] for i in range(len(time))]
    output.latest_time = max(max_times)
    
    output.units = units
    output.names = names
    output.nonzero_data = nonzero_data
    
    return output

def select_nuclides(data, nuclides):
    """
    Given some breakthrough data and a list of nuclides (that can be names or indices)
    provided by the user, it filters the data corresponding to those nuclides.
    """

    filtered_times = []
    filtered_insta_bt = []
    filtered_cumul_bt = []
    filtered_names = []
    filtered_nonzero = []
    
    if nuclides!=None:
        for nuc_i in range(len(nuclides)):
            if type(nuclides[nuc_i]) is str:
                nuclides[nuc_i] = data.names.index(nuclides[nuc_i])
        
        for i in range(len(nuclides)):
            filtered_times.append(data.times[nuclides[i]])
            filtered_insta_bt.append(data.insta_bt[nuclides[i]])
            filtered_cumul_bt.append(data.cumul_bt[nuclides[i]])
            filtered_names.append(data.names[nuclides[i]])
            filtered_nonzero.append(data.nonzero_data[nuclides[i]])
    else:
        filtered_times = data.times
        filtered_insta_bt = data.insta_bt
        filtered_cumul_bt = data.cumul_bt
        filtered_names = data.names
        filtered_nonzero = data.nonzero_data
        
    return filtered_times, filtered_insta_bt, filtered_cumul_bt, filtered_names, filtered_nonzero

def plot_cumul_bt(data,mode="loglog",axis=None,nuclides=None,labels=None):
    """
    Plots the cumulative breakthrough of a file read using read_breakthrough().
    """
    
    filtered_times, filtered_insta_bt, filtered_cumul_bt, filtered_names, filtered_nonzero = select_nuclides(data, nuclides)
    
    if labels==None:
        labels = filtered_names
    
    for i in range(len(filtered_times)):
        if filtered_nonzero[i]:
            # If the data is nonzero somewhere, we plot it
            if mode=="logx":
                plt.semilogx(filtered_times[i],filtered_cumul_bt[i],label=labels[i])
            elif mode=="logy":
                plt.semilogy(filtered_times[i],filtered_cumul_bt[i],label=labels[i])
            elif mode=="loglog":
                plt.loglog(filtered_times[i],filtered_cumul_bt[i],label=labels[i])
            elif mode=="uniform":
                plt.plot(filtered_times[i],filtered_cumul_bt[i],label=labels[i])
    
    if axis != None:
        plt.axis(axis)
    else:
        plt.xlim([data.earliest_cumul, data.latest_time])
    
    plt.xlabel("Time (yr)")
    plt.ylabel("Cumulative breakthrough ("+data.units+")")
    
def plot_insta_bt(data,mode="loglog",axis=None,nuclides=None,labels=None):
    """
    Plots the instantaneous breakthrough of a file read using read_breakthrough().
    """
    
    filtered_times, filtered_insta_bt, filtered_cumul_bt, filtered_names, filtered_nonzero = select_nuclides(data, nuclides)
    
    if labels==None:
        labels = filtered_names
        
    for i in range(len(filtered_times)):
        if filtered_nonzero[i]:
            # If the data is nonzero somewhere, we plot it
            if mode=="logx":
                plt.semilogx(filtered_times[i],filtered_insta_bt[i],label=labels[i])
            elif mode=="logy":
                plt.semilogy(filtered_times[i],filtered_insta_bt[i],label=labels[i])
            elif mode=="loglog":
                plt.loglog(filtered_times[i],filtered_insta_bt[i],label=labels[i])
            elif mode=="uniform":
                plt.plot(filtered_times[i],filtered_insta_bt[i],label=labels[i])
    
    if axis != None:
        plt.axis(axis)
    else:
        plt.xlim([data.earliest_insta, data.latest_time])
    
    plt.xlabel("Time (yr)")
    plt.ylabel("Instantaneous breakthrough ("+data.units+"/yr)")