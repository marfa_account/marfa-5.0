# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 09:23:50 2020

@author: Jordi Sanglas

===============================================================================

Script for the conversion of MARFA-SP v4.x input files to v5.x files.

Running the script
------------------

Before running this script, modify the input_files_path variable below with the
path to the directory that contains the MARFA v4.x input files. The script only
needs nuclides.dat and rocktypes.dat. The original rocktypes.dat will be
renamed rocktypes_v4_x.dat, and a new rocktypes.dat compatible with v5.0 will
be created.

Diffusion coefficients
----------------------

MARFA v4.x had the capability to specify an element-dependent molecular
diffusion coefficient. To do so, an additional parameter could be added at the
end of each element parameters line of a rock type block. In MARFA v5.0,
however, the molecular diffusion coefficient is the same for all the elements.

The script converts the molecular diffusion coefficients in the following way:
    
    - If no diffusion coefficients have been specified, the coefficient after
    conversion is set to 0 as expected.
    - If some (or all) of the diffusion coefficients have been specified, the
    script averages them, without taking into account the elements without a
    value. The average is used to set the v5.0 coefficient.
    
Example: In a MARFA v4.x model, we have three elements. In a rock type, the
following diffusion coefficients have been set.

Element A -> D not specified
Element B -> D = 3.16e-2  m^2/y
Element C -> D = 3.16e-2  m^2/y

The script will average B and C (ignoring A, which does not have a coefficient)
and write the resulting average. In MARFA v5.0, the resulting coefficient will
be 3.16e-2 m^2/y.


Copyright
---------

Copyright 2020.  Swedish Nuclear Fuel and Waste Management Company (SKB) and
Posiva Oy (FINLAND). All rights reserved. 

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

  Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.

  Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

  Neither the names of the copyright holders, nor the names of its contributors may be
  used to endorse or promote products derived from this software without
  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

===============================================================================
"""

import os

#-------------- Parameters ---------------

# Path to the folder with the MARFA v4.x input files
input_files_path = "C:/Users/jsalglas/Desktop/Arxius_varis/MARFA/models/tang/marfa_solution"

#-----------------------------------------

def check_file_exists(file_path):
    file_name = os.path.basename(file_path)
    if not os.path.isfile(file_path):
        raise FileNotFoundError('File "'+file_name+'" not found. Make sure that the right directory has been specified.')

# Get number of elements from nuclides.dat
nuclides_path = os.path.join(input_files_path,"nuclides.dat")

check_file_exists(nuclides_path)

with open(nuclides_path,"r") as nuclides_file:
    split_first_line = nuclides_file.readline().split()
    num_elements = int(split_first_line[0])

rocktypes_path = os.path.join(input_files_path,"rocktypes.dat")
rocktypes_old_path = rocktypes_path[:-4]+"_v4_x.dat"

check_file_exists(rocktypes_path)
    
print ("Converting rocktypes.dat from v4.x to v5.x")

os.rename(rocktypes_path,rocktypes_old_path)

rocktype_block_started = False
rocktype_block_count = 0
new_rocktype_block = None
alpha = None
stochastic = None
numdir = None
numvc = None
current_average = None
curr_num_d = None
ret_model = None

with open (os.path.join(rocktypes_old_path),"r") as old_rocktypes_file:
    with open (rocktypes_path,"w") as rocktypes_file:
        for line in old_rocktypes_file:
            if "!" in line:
                # Remove comments
                line = line[:line.index("!")]
                
            split_line = line.split()
            
            if rocktype_block_started:
                rocktype_block_count += 1
                
                if rocktype_block_count == 1:
                    # Retention model line
                    ret_model = line.rstrip()
                    
                elif rocktype_block_count == 2:
                    # Element-independent params line
                    new_rocktype_block.append(split_line)
                    
                else:
                    # Element-dependent params
                    if ((ret_model == "MD" or ret_model == "LD") and len(split_line)==3) or \
                    (ret_model == "ES" and len(split_line)==2):
                        current_average += float(split_line[2])
                        curr_num_d += 1
                        new_rocktype_block.append(split_line[:-1])
                    else:
                        new_rocktype_block.append(split_line)
                
                    if rocktype_block_count == 2+num_elements:
                        # End of the rocktypes block. Write lines to the new rocktypes.dat file
                        
                        rocktype_block_count = 0
                        rocktype_block_started = False
                        
                        current_average = current_average/curr_num_d if curr_num_d>0 else 0
                        
                        if float(alpha)!=0 or current_average!=0:
                            print ("DISPERSION",alpha,current_average,file=rocktypes_file)
                        if stochastic:
                            print ("STOCHASTIC",numdir,numvc,file=rocktypes_file)
                            
                        print (ret_model, file=rocktypes_file)
                        for block_line in new_rocktype_block:
                            print (" "+" ".join(block_line), file=rocktypes_file)
                    
            else:
                if len(split_line)>1:
                    if split_line[1].lower()=="d" or split_line[1].lower()=="s":
                        # We have found the deterministic/stochastic line. Begin new rocktypes block
                        alpha = split_line[0]
                        stochastic = split_line[1].lower()=="s"
                        if len(split_line)>=4:
                            numdir = split_line[2]
                            numvc = split_line[3]
                        else:
                            numdir = 0
                            numvc = 0
                        
                        new_rocktype_block = []
                        rocktype_block_count = 0
                        rocktype_block_started = True
                        current_average = 0
                        curr_num_d = 0
                        
                if not rocktype_block_started:
                    print (line.rstrip(),file=rocktypes_file)
                
                
print ("Done. Note that retention in stagnant water can now be added to the converted files.")