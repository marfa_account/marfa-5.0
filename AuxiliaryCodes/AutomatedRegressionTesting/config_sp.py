###################################################################################
"""
MARFA REGRESSION TESTER
Authors: Hedieh Ebrahimi (Hedieh.Ebrahimi@amphos21.com)

Configuration file for MARFA SP 
lastRevisionDate = 26/07/2019
		
Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (FINLAND).
All rights reserved. 

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

  Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.

  Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

  Neither the names of the copyright holders, nor the names of its contributors may be
  used to endorse or promote products derived from this software without
  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
####################################################################################

###marfa executables
app_dir="C:/Users/jsalglas/Desktop/Arxius_varis/MARFA/VS_projects/StaticPath_dev/Debug"
app_sp = "StaticPath_dev.exe"

### validation tests 

validation_tests_dir="C:/Users/jsalglas/Desktop/Arxius_varis/MARFA/repositories/marfa-dev/marfa-sp/RegressionValidationTests/Windows"
indep_solution_dir = "C:/Users/jsalglas/Desktop/Arxius_varis/MARFA/repositories/marfa-dev/marfa-sp/RegressionValidationTests/IndependentSolutions"
output_dir="C:/Users/jsalglas/Desktop/Arxius_varis/MARFA/validation_tests/sp"

log_scale=True
add_legend=True
num_particles=1000000

y_axis_limit={    "1": [1e-8,1e-4],
                          "2": [1e-8,1e-4],
                          "3": [1e-8,1e-4],
                          "4": [1e-10,1e-4],
                          "5": [1e-9,1e-4],
                          "6": [1e-9,1e-3],
                          "7": [1e-10,1e-4],
                          "8": [1e-9,1e-3],
                          "9": [1e-10,1e-4],
                        "9a": [1e-11,1e-4],
                        "9b": [1e-9,1e-3],
                        "10": [1e-11,1e-4],
                        "11": [1e-13,1e-4],
                      "11a": [1e-13,1e-4],
                      "11b": [1e-10,1e-4],
                        "12": [1e-8,10],
                      "12a": [1e-8,10],
                      "12b": [1e-8,10],
                      "12c": [1e-8,10],
                      "12d": [1e-8,10],             
    }

x_axis_limit={   "1":    [1e+2,1e+5],
                         "2":    [1e+2,1e+5],
                         "3":    [1e+2,1e+5],
                         "4":    [1e+2,1e+5],
                         "5":    [1e+2,1e+5],
                         "6":    [1e+2,1e+5],
                         "7":    [1e+2,1e+5],
                         "8":    [1e+2,1e+5],
                         "9":    [1e+2,1e+5],
                       "9a":    [1e+2,1e+5],
                       "9b":    [1e+2,1e+5],
                       "10":    [1e+2,1e+5],
                       "11":    [1e+1,1e+5],
                     "11a":    [1e+2,1e+5],
                     "11b":    [1e+2,1e+5],
                       "12":    [1e-2,1e+2],
                     "12a":    [1e-2,1e+2],
                     "12b":    [1e-2,1e+2],
                     "12c":    [1e-2,1e+2],
                     "12d":    [1e-2,1e+2],             
    }