####################################################################################
"""
MARFA REGRESSION TESTER
Authors: Hedieh Ebrahimi (Hedieh.Ebrahimi@amphos21.com)

This script validates MARFA NR validation tests against MARFA SP.  
lastRevisionDate = 26/07/2019

Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (FINLAND).
All rights reserved. 

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

  Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.

  Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

  Neither the names of the copyright holders, nor the names of its contributors may be
  used to endorse or promote products derived from this software without
  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
####################################################################################
import subprocess,os
import logging
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import shutil
import config_nr
####################################################################################

### LOGGER
logging_info=[]
if os.path.exists(config_nr.output_dir):
    shutil.rmtree(config_nr.output_dir,ignore_errors=False)
os.makedirs(config_nr.output_dir)

### set the logger
logger = logging.getLogger('my logger')
logger.setLevel(logging.DEBUG)

# create file handler which logs even debug messages
fh = logging.FileHandler(os.path.join(config_nr.output_dir, 'log.log'), mode='w',)
fh.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.info('Started the LOG File:')

logger.info('num_particles=')
logger.info(config_nr.num_particles)
logging_info.append('num_particles=\n')
logging_info.append(str(config_nr.num_particles) +"\n")
logging_info.append("\n" ) 

if config_nr.log_scale :
            logger.info("Log scale selected for X/Y axes ! ")
            logging_info.append('Log scale selected for X/Y axes ! \n')
            logging_info.append("\n" ) 

####################################################################################
### RUNNING MARFA validation tests 
def run_tests(config_nr):

    app_path_nr=os.path.join(config_nr.app_dir, config_nr.app_nr)
    app_path_sp=os.path.join(config_nr.app_dir, config_nr.app_sp)
    tests= os.listdir(config_nr.validation_tests_dir)
    
    output_version=True
    failed_tests_nr=[]
    for test in tests:
        subfolder=test+"/nr"
        mycwd=os.path.join(config_nr.validation_tests_dir,subfolder)
        
        try:
            
            FNULL = open(os.devnull, 'w')
            grepOut =subprocess.check_output([app_path_nr, str(config_nr.num_particles)], cwd=mycwd)      
            if output_version==True:
                exe_out=grepOut.split(b"Version")
                version_num_byte=exe_out[1][:4]
                version_num=version_num_byte.decode("utf-8") 
                output_version=False 
      
        except subprocess.CalledProcessError as grepexc:      
              
            failed_tests_nr.append(test)
            return_code=grepexc.returncode
            #logger.info(test+ " failed with return code "+str(return_code)+"\n")
    
    ##################################
    if len(failed_tests_nr) != 0: 
        logger.info(" NR validation tests failed : ")       
        logger.info(failed_tests_nr)       
        logging_info.append("NR validation tests failed : \n")    
        logging_info.append("".join(failed_tests_nr)+"\n" )  
        logging_info.append("\n" )  
        
    else :
        logger.info(" NR validation tests failed : ")       
        logger.info("No tests failed!")       
        logging_info.append("NR validation tests failed :  \n")    
        logging_info.append(" No tests failed!\n" )  
        logging_info.append("\n" )  
      
    
    failed_tests_sp=[]
    
    for test in tests:
        
        subfolder=test+"/sp"
        mycwd=os.path.join(config_nr.validation_tests_dir,subfolder)
        
        try:
            
            FNULL = open(os.devnull, 'w')
            grepOut =subprocess.check_call([app_path_sp, str(config_nr.num_particles)], cwd=mycwd)      
    
        except subprocess.CalledProcessError as grepexc:      
               
            failed_tests_sp.append(test)
            return_code=grepexc.returncode
            #logger.info(test+ " failed with return code "+str(return_code)+"\n")
    
   
    if len(failed_tests_sp) != 0: 
        logger.info(" SP validation tests failed : ")       
        logger.info(failed_tests_sp)       
        logging_info.append("SP validation tests failed : \n")    
        logging_info.append("".join(failed_tests_sp)+"\n" )  
        logging_info.append("\n" )  
        print("SP validation tests failed :  ")    
        print(failed_tests_sp)
        
    else :
        logger.info(" SP validation tests failed : ")       
        logger.info("No tests failed!")       
        logging_info.append("SP validation tests failed :  \n")    
        logging_info.append(" No tests failed!\n" )  
        logging_info.append("\n" )  
        print("SP validation tests failed :  ")    
        print("No tests failed!")
    
    
    logger.info("MARFA Version: ")       
    logger.info(version_num)    
    logging_info.append("MARFA Version:  \n")    
    logging_info.append(version_num+"\n") 
    logging_info.append("\n" )  
    
    return tests,failed_tests_nr,failed_tests_sp,version_num


####################################################################################
def draw_comparison_graphs():
    
    """  File : Breakthrough_Mo.rlt 
    For each NR validation test ,  the Instantaeous  breakthrough curve (Mole/y) 
    for species A and B is plotted vs equivalent SP validation test  """
    
    f, ax= plt.subplots(2,3, figsize=(15,15), dpi=200)
    f.subplots_adjust(hspace=0.4, wspace=0.4)
    
    num_tests=len(tests)
    for i in range (0, num_tests):
        row=i//3
        col=i%3
        test=tests[i]
        if test not in failed_tests_nr and  test not in failed_tests_sp :
            subfolder1=test+"/nr"
            subfolder2=test+"/sp"
            dir1=os.path.join(config_nr.validation_tests_dir,subfolder1)
            dir2=os.path.join(config_nr.validation_tests_dir,subfolder2)
            #################
            t1=[]
            val1=[]
            t2=[]
            val2=[]
            check=0
            with open(os.path.join(dir1,"Breakthrough_Mo.rlt"), "r") as f1:
                    
                    for line1 in f1:
                        if "(Mole)" in line1:
                            check+=1 
                            
                        if check <2: 
                            if "!" not in line1 :
                                if len(line1.strip()) != 0 :
                                    cur_line1=line1.split()
                                    t1.append(cur_line1[0])
                                    val1.append(cur_line1[2])
                        
                        if check >= 2:
                            if "!" not in line1 : 
                                if len(line1.strip()) != 0 :
                                    cur_line1=line1.split()
                                    t2.append(cur_line1[0])
                                    val2.append(cur_line1[2])
                                    
            t1_f=[float(i) for i in t1]           # NodeRouting time1
            val1_f=[float(i) for i in val1]     # NodeRouting species A value
            t2_f=[float(i) for i in t2]           # NodeRouting time2
            val2_f=[float(i) for i in val2]     # NodeRouting species B  value
    
            #################
            t3=[]               # Static Path time1
            val3=[]            # Static Path species A value
            t4=[]               # Static Path time2
            val4=[]            # Static Path species B value
            check=0
            with open(os.path.join(dir2,"Breakthrough_Mo.rlt"), "r") as f2:
                    
                    for line1 in f2:
                        if "(Mole)" in line1:
                            check+=1 
                            
                        if check <2: 
                            if "!" not in line1 :
                                if len(line1.strip()) != 0 :
                                    cur_line1=line1.split()
                                    t3.append(cur_line1[0])
                                    val3.append(cur_line1[2])
                        
                        if check >= 2:
                            if "!" not in line1 : 
                                if len(line1.strip()) != 0 :
                                    cur_line1=line1.split()
                                    t4.append(cur_line1[0])
                                    val4.append(cur_line1[2])
            
            t3_f=[float(i) for i in t3]
            val3_f=[float(i) for i in val3]
            
            t4_f=[float(i) for i in t4]
            val4_f=[float(i) for i in val4]
            
            x_min=abs(min([min(t1_f),min(t2_f), min(t3_f),min(t4_f)]))
            x_max=abs(max([max(t1_f),max(t2_f), max(t3_f),max(t4_f)]))
            y_min=abs(min([min(val1_f),min(val2_f),min(val3_f),min(val4_f)]))
            y_max=abs(max([max(val1_f),max(val2_f),max(val3_f),max(val4_f)]))
    
            #################
            
            ax[row,col].plot(t1_f, val1_f, 'bo', markersize=2,label="species A test result")
            ax[row,col].plot(t3_f, val3_f, 'b-', markersize=2,label='species A benchmark solution')
            ax[row,col].plot(t2_f, val2_f, 'ko', markersize=2,label="species B test result")
            ax[row,col].plot(t4_f, val4_f, 'k-', markersize=2,label='species B benchmark solution')
            ax[row, col].set_title(test, fontsize=5)
            ax[row, col].set_xlabel("Time(yrs)", fontsize=5)
            ax[row, col].set_ylabel("Instantaneous Breakthrough(mol/yr) ", fontsize=5)
            
            if config_nr.log_scale :
                ax[row, col].set_xscale("log") 
                ax[row, col].set_yscale("log") 
            
            test_num=test.strip("Test")
    
            if test_num in list(config_nr.x_axis_limit.keys()):
                x_ax_lmt=config_nr.x_axis_limit[test_num]
                plt.xlim( (x_ax_lmt[0], x_ax_lmt[1]) )
            else:
                plt.xlim( (x_min, x_max) )
            
            if test_num in list(config_nr.y_axis_limit.keys()):
                y_ax_lmt=config_nr.y_axis_limit[test_num]
                plt.ylim( (y_ax_lmt[0], y_ax_lmt[1]) )
            else:
                plt.ylim( (y_min, y_max) )        
    
    
    if config_nr.add_legend:
        ax[row,col].legend(bbox_to_anchor=(1.2, 1.3), loc=2, borderaxespad=1.)

    plt.savefig(os.path.join(config_nr.output_dir,"nr.png"))    
    
    return f

####################################################################################

def report_pdf(version_num,fig,text):

    with PdfPages(os.path.join(config_nr.output_dir,'MARFA_regression_report.pdf'),"w") as pdf:  
        # Page 1
        firstPage = plt.figure(figsize=(11.69,8.27))
        firstPage.clf()
        txt ="MARFA NR "+version_num+"  Regression Test Report \n"   
        firstPage.text(0.5,0.5,txt, transform=firstPage.transFigure, size=24, ha="center")
        pdf.savefig()
        plt.close()
        
        # Page 2 
        firstPage = plt.figure(figsize=(11.69,8.27))
        firstPage.clf()
        txt =text
        firstPage.text(0.1,0.5,txt, transform=firstPage.transFigure, size=12, ha="left")
        pdf.savefig()
        plt.close()
        
        #Page 3 : multi-plot figure
        pdf.savefig(fig)
                     
####################################################################################
tests,failed_tests_nr,failed_tests_sp,version_num=run_tests(config_nr)
f=draw_comparison_graphs()
logging_info_s = ''.join(logging_info)
report_pdf(version_num,f,logging_info_s)
####################################################################################

logger.info("Log/ Comparison Graphs are written to: ")
logger.info(config_nr.output_dir)  
logger.info("Finished running tests!")    
print("Finished")       
        
        
        


        
   
