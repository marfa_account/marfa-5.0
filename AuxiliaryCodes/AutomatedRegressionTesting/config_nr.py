###################################################################################
"""
MARFA REGRESSION TESTER
Authors: Hedieh Ebrahimi (Hedieh.Ebrahimi@amphos21.com)

Configuration file for MARFA NR
lastRevisionDate = 26/07/2019
		
Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (FINLAND).
All rights reserved. 

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

  Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.

  Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

  Neither the names of the copyright holders, nor the names of its contributors may be
  used to endorse or promote products derived from this software without
  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
####################################################################################

# MARFA executables
app_dir="C:/Users/jsalglas/Desktop/Arxius_varis/MARFA/executables"
app_sp = "StaticPath_dev.exe"
app_nr="NodeRouting_dev.exe"


# validation tests 
validation_tests_dir="C:/Users/jsalglas/Desktop/Arxius_varis/MARFA/repositories/marfa-dev/marfa-nr/RegressionValidationTests/Windows"

output_dir="C:/Users/jsalglas/Desktop/Arxius_varis/MARFA/validation_tests/nr"

log_scale=True
add_legend=True
num_particles=1000000

y_axis_limit={    "1a": [1e-8,5e-5],
                          "1b": [1e-8,1e-4],
                          "1c": [1e-8,1e-4],
                          "2": [1e-10,5e-5],
                          "3": [1e-8,1e-4],             
    }

x_axis_limit={   "1a":    [1e+2,1e+6],
                         "1b":    [1e+2,1e+6],
                         "1c":    [1e+2,1e+6],
                         "2":    [1e+2,1e+6],
                         "3":    [1e+2,1e+6],            
    }