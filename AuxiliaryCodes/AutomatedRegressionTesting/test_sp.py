####################################################################################
"""
MARFA REGRESSION TESTER
Authors: Hedieh Ebrahimi (Hedieh.Ebrahimi@amphos21.com)
and Jordi Sanglas (jordi.sanglas@amphos21.com)

Contains script to run regression tests to validate MARFA
validation tests against provided benchmark solutions.  
lastRevisionDate = 26/07/2019
		
Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (FINLAND).
All rights reserved. 

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

  Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.

  Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

  Neither the names of the copyright holders, nor the names of its contributors may be
  used to endorse or promote products derived from this software without
  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
####################################################################################
import subprocess,os.path
import logging
import matplotlib.pyplot as plt
import shutil
import config_sp
from matplotlib.backends.backend_pdf import PdfPages
####################################################################################

### LOGGER
logging_info=[]
if os.path.exists(config_sp.output_dir):    
    shutil.rmtree(config_sp.output_dir,ignore_errors=False)
os.makedirs(config_sp.output_dir)

logger = logging.getLogger('my logger')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler(os.path.join(config_sp.output_dir, 'log.log'), mode='w',) 
fh.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.info('Started the LOG File:')
logger.info('num_particles=')
logger.info(config_sp.num_particles)
logging_info.append('num_particles=\n')
logging_info.append(str(config_sp.num_particles) +"\n")
logging_info.append("\n" )  

if config_sp.log_scale :
            logger.info("Log scale selected for X/Y axes ! ")
            logging_info.append('Log scale selected for X/Y axes ! \n')
            logging_info.append("\n" )  
        

tests_resident=["Test12b","Test12c","Test12d"]

####################################################################################
# ### RUNNING MARFA VALIDATION TESTS
def run_tests(config_sp):

    app_path=os.path.join(config_sp.app_dir, config_sp.app_sp)
    sp_tests= os.listdir(config_sp.validation_tests_dir)
    sp_tests_to_run=[]
    for test1 in sp_tests: 
        if os.path.isfile(os.path.join(config_sp.indep_solution_dir,test1+".csv")):
            sp_tests_to_run.append(test1)
    sp_tests_to_exclude=list(set(sp_tests) - set(sp_tests_to_run))
    
    logger.info('The following tests Do NOT include independent solution and are excluded :')
    logger.info(sp_tests_to_exclude)
    logging_info.append('The following tests Do NOT include independent solution and are excluded :\n')
    logging_info.append(" ".join(sp_tests_to_exclude)+"\n")
    logging_info.append("\n" )
     
    tests_brc=sorted(list(set(sp_tests_to_run)- set(tests_resident)))
     
    output_version=True
    failed_tests=[]
    for test2 in sp_tests_to_run:
        mycwd=os.path.join(config_sp.validation_tests_dir,test2)
     
        try:
            FNULL = open(os.devnull, 'w')
            errorfile=open("errors.txt","w")
            grepOut =subprocess.check_output([app_path, str(config_sp.num_particles)],stderr=errorfile, cwd=mycwd)   
            errorfile.close()
            print(grepOut.decode("ascii"))
            if output_version==True:
                exe_out=grepOut.split(b"Version")
                version_num_byte=exe_out[1][:4]
                version_num=version_num_byte.decode("utf-8")
                output_version=False

                
        except subprocess.CalledProcessError as grepexc:
            failed_tests.append(test2)
            return_code=grepexc.returncode
            my_output=grepexc.output
            
            #Added lines:
            with open("output.txt","wb") as outputfile:
                outputfile.write(my_output)
               
    if len(failed_tests) != 0: 
        logger.info("The following validation tests have been failed : ")       
        logger.info(failed_tests)
        logging_info.append("The following validation tests have been failed : \n")    
        logging_info.append("".join(failed_tests)+"\n" )
        logging_info.append("\n" )
        print("The following validation tests have been failed : ")
        print(failed_tests)
        
    else :
        logger.info("The following validation tests have been failed : ")
        logger.info("No tests failed!")
        logging_info.append("The following validation tests have been failed : \n")
        logging_info.append(" No tests failed!\n" )
        logging_info.append("\n" )
        print("The following validation tests have been failed : ")
        print("No tests failed!")
        
    logger.info("MARFA Version: ")       
    logger.info(version_num)    
    logging_info.append("MARFA Version:  \n")    
    logging_info.append(version_num+"\n") 
    logging_info.append("\n" )  
    
    
    return tests_brc,failed_tests,version_num

####################################################################################
def draw_comparison_graphs():
    
    """  File : Breakthrough_Mo.rlt 
          Tests : 1,2,3,4,5,6,7,8,9,9a,9b, 10,11, 11a, 11b,12,12a
    For each validation test,  the Instantaeous  breakthrough curve (Mole/y) 
    for species A and B is plotted vs Independent solution """
    
    f, ax= plt.subplots(7,4, figsize=(15,15), dpi=200)
    f.subplots_adjust(hspace=0.4, wspace=0.4)
    
    num_tests_1=len(tests_brc)
    for i in range (0, num_tests_1):
        row=i//4
        col=i%4
        test3=tests_brc[i]
        if test3 not in failed_tests:
            test_num=test3.split("Test")[-1]
            dir1=os.path.join(config_sp.validation_tests_dir,test3)
             
            #################
            t1=[]
            val1=[]
            t2=[]
            val2=[]
            check=0
            with open(os.path.join(dir1,"Breakthrough_Mo.rlt"), "r") as f1:
                     
                    for line1 in f1:
                        if "(Mole)" in line1:
                            check+=1 
                             
                        if check <2: 
                            if "!" not in line1 :
                                if len(line1.strip()) != 0 :
                                    cur_line1=line1.split()
                                    t1.append(cur_line1[0])
                                    val1.append(cur_line1[2])
                         
                        if check >= 2:
                            if "!" not in line1 : 
                                if len(line1.strip()) != 0 :
                                    cur_line1=line1.split()
                                    t2.append(cur_line1[0])
                                    val2.append(cur_line1[2])
            #################
            t3=[]
            val3=[]
            val4=[]
            with open(os.path.join(config_sp.indep_solution_dir,test3+".csv"), "r")  as f2:       
                next(f2) 
                for line2 in f2:
                    splitLine = line2.split(",")
                    t3.append(float(splitLine[0]))
                    val3.append(float(splitLine[1]))   
                    val4.append(float(splitLine[2]))   
        
            #################
            t1_f=[float(i) for i in t1]
            t1_min=min(t1_f)
            t1_max=max(t1_f)
            val1_f=[float(i) for i in val1]
            val1_min=min(val1_f)
            val1_max=max(val1_f)
            
            t2_f=[float(i) for i in t2]
            val2_f=[float(i) for i in val2]
            val2_min=min(val2_f)
            val2_max=max(val2_f)
             
            t3_min=min(t3)
            t3_max=max(t3)
            val3_min=min(val3)
            val3_max=max(val3)
            val4_min=min(val4)
            val4_max=max(val4)
             
            x_min=abs(min([t1_min,t3_min]))
            x_max=abs(max([t1_max,t3_max]))
            y_min=abs(min([val1_min,val2_min,val3_min,val4_min]))
            y_max=abs(max([val1_max,val2_max,val3_max,val4_max]))
            
           
            if test_num in list(config_sp.x_axis_limit.keys()):
                x_ax_lmt=config_sp.x_axis_limit[test_num]
                ax[row,col].set_xlim(x_ax_lmt[0], x_ax_lmt[1])
            else:
                ax[row,col].set_xlim(x_min, x_max)
            
            
            if test_num in list(config_sp.y_axis_limit.keys()):
                y_ax_lmt=config_sp.y_axis_limit[test_num]
                ax[row,col].set_ylim(y_ax_lmt[0], y_ax_lmt[1])
            else:
                ax[row,col].set_ylim(y_min, y_max)
                
                
           
            ax[row,col].plot(t1_f, val1_f, 'bo', markersize=2,label="species A test result")
            ax[row,col].plot(t3, val3, 'b-', markersize=2,label='species A benchmark solution')
            ax[row,col].plot(t2_f, val2_f, 'ko', markersize=2,label="species B test result")
            ax[row,col].plot(t3, val4, 'k-', markersize=2,label='species B benchmark solution')
            ax[row, col].set_title(test3, fontsize=5)
            ax[row, col].set_xlabel("Time(yrs)", fontsize=5)
            ax[row, col].set_ylabel("Instantaneous Breakthrough(mol/yr) ", fontsize=5)
                                          
            if config_sp.log_scale :
                ax[row, col].set_xscale("log") 
                ax[row, col].set_yscale("log") 
                        
#############################################################        
    """ File : Resident Mass
         Tests : 12b, 12c, 12d
    There are two blocks of data in resident_mass.dat,
    Each block corresponds to one radionuclide. 
    In each block, the fields are time, mobile mass, immobile mass. 
    It's the mobile mass you want to compare  with the independent solution. 
    Note these are total mass [mols], not a concentration. 
    Mobile mass needs to be diveded by the cell volume (10 m3 in this case) to get 
    a concentration to compare to the independent solutions.  """
    
    num_tests_2=len(tests_resident)
    for i in range (0, num_tests_2):
        row=(i+num_tests_1)//4
        col=(i+num_tests_1)%4
        test4=tests_resident[i]
        if test4 not in failed_tests:
        
            test_num_2=test4.split("Test")[-1]
            dir1=os.path.join(config_sp.validation_tests_dir,test4) 
            #################
            t4=[]
            val4=[]
            t5=[]
            val5=[]
            with open(os.path.join(dir1,"resident_mass.dat"), "r") as f3:
                         
                for line3 in f3:
                    if "!" not in line3 :
                        if len(line3.strip()) != 0 :
                            cur_line3=line3.split()
                            col1=cur_line3[0]
                            col2=cur_line3[1]
                            col3=cur_line3[2]
                            if col1=="1":
                                t4.append(col2)
                                val4.append(col3)
                                
                            if col1=="2":
                                t5.append(col2)
                                val5.append(col3)
                        
            t4.pop(0)
            val4.pop(0)   
            t5.pop(0)
            val5.pop(0)   
        #################
            t6=[]
            val6=[]
            val7=[]
            with open(os.path.join(config_sp.indep_solution_dir,test4+".csv"), "r")  as f4:       
                next(f4)
                for line2 in f4:
                    splitLine = line2.split(",")
                    t6.append(float(splitLine[0]))
                    val6.append(float(splitLine[1]))   
                    val7.append(float(splitLine[2]))   
      
            #################
            t4_f=[float(i) for i in t4]
            x_min_3=min(t4_f)
            x_max_3=max(t4_f)
            val4_f=[float(i)/10 for i in val4]
            y_min_3=min(val4_f)
            y_max_3=max(val4_f)
            
            t5_f=[float(i) for i in t5]
            x_min_4=min(t5_f)
            x_max_4=max(t5_f)
            val5_f=[float(i)/10 for i in val5]
            y_min_4=min(val5_f)
            y_max_4=max(val5_f)
            
              
            x_min_5=min(t6)
            x_max_5=max(t6)
            y_min_5=min(val6)
            y_max_5=max(val6)
            y_min_6=min(val7)
            y_max_6=max(val7)
            
             
            x_min=abs(min([x_min_3,x_min_4,x_min_5]))
            x_max=abs(max([x_max_3,x_max_4,x_max_5]))
            y_min=abs(min([y_min_3,y_min_4,y_min_5,y_min_6]))
            y_max=abs(max([y_max_3,y_max_4,y_max_5,y_max_6])) 
            
            if test_num_2 in list(config_sp.x_axis_limit.keys()):
                x_ax_lmt=config_sp.x_axis_limit[test_num_2]
                ax[row,col].set_xlim((x_ax_lmt[0], x_ax_lmt[1]))
            else:
                ax[row,col].set_xlim((x_min, x_max))
              
             
            if test_num_2 in list(config_sp.y_axis_limit.keys()):
                y_ax_lmt=config_sp.y_axis_limit[test_num_2]
                ax[row,col].set_ylim((y_ax_lmt[0], y_ax_lmt[1]))
            else:
                ax[row,col].set_ylim((y_min, y_max))

            
            ax[row,col].plot(t4_f, val4_f, 'bo', markersize=2,label="species A test result")
            ax[row,col].plot(t6, val6, 'b-', markersize=2,label='species A benchmark solution ')
            ax[row,col].plot(t5_f, val5_f, 'ko', markersize=2,label="species B test result")
            ax[row,col].plot(t6, val7, 'k-', markersize=2,label='species B benchmark solution')
            ax[row, col].set_title(test4, fontsize=5)
            ax[row, col].set_xlabel("Time(yrs)", fontsize=5)
            ax[row, col].set_ylabel("ResConc(mol/m3)", fontsize=5)
            
            
            if config_sp.log_scale :
                ax[row, col].set_xscale("log") 
                ax[row, col].set_yscale("log") 
            
    
    if config_sp.add_legend:
        plt.legend(bbox_to_anchor=(-0.29, -0.25), loc=2, borderaxespad=1.)
    plt.savefig(os.path.join(config_sp.output_dir,"sp.png"))     
    return f
#################################################################
def report_pdf(version_num,fig,text):

    with PdfPages(os.path.join(config_sp.output_dir,'MARFA_regression_report.pdf'),"w") as pdf:  
        # Page 1
        page1 = plt.figure(figsize=(11.69,8.27))
        page1.clf()
        txt ="MARFA SP "+version_num+"  Regression Test Report \n"   
        page1.text(0.5,0.5,txt, transform=page1.transFigure, size=24, ha="center")
        pdf.savefig()
        plt.close()
        
        # Page 2 
        page2 = plt.figure(figsize=(11.69,8.27))
        page2.clf()
        txt =text
        page2.text(0.1,0.5,txt, transform=page2.transFigure, size=12, ha="left")
        pdf.savefig()
        plt.close()
        
        #Page 3 : multi-plot figure
        pdf.savefig(fig)
        
####################################################################################
tests_brc,failed_tests,version_num=run_tests(config_sp)
f=draw_comparison_graphs()
logging_info_s = ''.join(logging_info)
report_pdf(version_num,f,logging_info_s)
####################################################################################

logger.info(" Comparison Graphs are written to: ")
logger.info(config_sp.output_dir)  
logger.info("Finished running tests!")    
print("Finished")           
                    

