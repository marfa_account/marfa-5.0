(* ::Package:: *)

(*-------------------------------------------------------------------*)
(*Stehfest Method*)
(*-------------------------------------------------------------------*)
(*Stehfest,H.,``Numerical inversion of Lapalce transforms'',Comm.*)
(*ACM,v .13,47-49 and 624,1970.*)(*-------------------------------------------------------------------*)
(*Define coefficients in series*)
csteh[n_,i_]:=(-1)^(i+n/2) Sum[k^(n/2) (2 k)!/((n/2-k)! k! (k-1)! (i-k)! (2 k-i)!),{k,Floor[(i+1)/2],Min[i,n/2]}]//N

(*The inverse function*)

NLInvSteh[F_,s_,t_,n_]:= Log[2]/t Sum[csteh[n,i] F/.s->i Log[2]/t,{i,1,n}]


(* ::Input:: *)
(**)


Chi[s_, beta_,  kappa_List, H_List, xi_List, Delta_List, nlayers_]:= Module[ 
{K=Length[kappa], Q, A, B}, 

Q=Table[ Exp[ 2 Sqrt[s] H[[k]]  Delta[[m]] ], {k,1, nlayers},{m,1,nlayers}];
A=Table[ 0, {K}]; 
B=Table[ 0, {K}]; 
A[[K]] := Q[[K,K-1]] *(1+xi[[K,K-1]]) + Q[[K,K]]*(1-xi[[K,K-1]]) ; 
B[[K]] := Q[[K,K-1]] *(1-xi[[K,K-1]]) + Q[[K,K]]*(1+xi[[K,K-1]]) ; 
     Do[ 
A[[k]] = Q[[k,k-1]] *(1 + xi[[k,k-1]])*A[[k+1]] + Q[[k,k]]*(1-xi[[k,k-1]]) B[[k+1]]; 
B[[k]] = Q[[k,k-1]] *(1 - xi[[k,k-1]])*A[[k+1]] + Q[[k,k]]*(1+xi[[k,k-1]]) B[[k+1]], 
{k, nlayers-1, 2, -1}];
Exp[ -beta* kappa[[1]] Sqrt[s] * ( Q[[1,1]]* B[[2]] - A[[2]])/( Q[[1,1]]* B[[2]] + A[[2]]) ]/s  ]


Layer3[ theta_, Dp_, Rm_, Deltain_, beta_] := Module[ 
{ kappa=theta Sqrt[ Rm Dp],  H=Sqrt[ Rm/Dp],  xi, nlayers=3, eta, Delta, f, result, indx, CalcPrecision=30}, 

xi=Table[ kappa[[i]]/(kappa[[j]] ) , {i,1,nlayers},{j,1,nlayers}]; 

Delta=Deltain; 
 
 
offset=0; 
(* 
eta = Deltain[[1]]/( (Dp * theta)[[1]] * beta ) ; 
 If[ eta <= 1/1000, 
	offset = Delta[[1]]*theta[[1]]Rm[[1]] *beta ; 
	Delta[[1]]=0];  *) 

lt1=-5; 
lt2=+8; 
 
result=Table[ time=10^lt; time = SetPrecision[time,CalcPrecision];  {time+offset, NLInvSteh[ Chi[s, beta, kappa, H, xi, Delta, nlayers], s, time, 14]}, {lt,lt1, lt2, 5/100}];
f[{x_, y_}] := If[ y < 0, {x,0}, {x,y}];
result=Map[ f, result];

foo=Position[ result, {x_, y_} /; y==  0 ]; 
indx=If [foo ==  {},0,  foo [[-1,1]]];
If[indx != 0, result=Drop[result, indx-1] ];


foo=Position[ result, {x_, y_} /; y> 1 ]; 
indx=If [foo ==  {},0,  foo [[1,1]]];
If[indx != 0, result=Take[result, indx]; result[[indx,2]] = 1  ];

result ]
