(* ::Package:: *)

(* Copyright (2011).  Los Alamos National Security, LLC.  All rights reserved.
This material was produced by the Los Alamos National Laboratory, 
which is operated by Los Alamos National Security, LLC for the U.S. Department of Energy. 
The Government is granted for itself and others acting on its behalf a paid-up, 
nonexclusive, irrevocable worldwide license in this material to reproduce, 
prepare derivative works, and perform publicly and display publicly. 
Beginning five (5) years after (2011), subject to additional five-year worldwide renewals, 
the Government is granted for itself and others acting on its behalf a paid-up, nonexclusive, 
irrevocable worldwide license in this material to reproduce, prepare derivative works, 
distribute copies to the public, perform publicly and display publicly, 
and to permit others to do so. 
NEITHER THE UNITED STATES NOR THE UNITED STATES DEPARTMENT OF ENERGY, 
NOR LOS ALAMOS NATIONAL SECURITY, LLC, NOR ANY OF THEIR EMPLOYEES, 
MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LEGAL LIABILITY OR RESPONSIBILITY 
FOR THE ACCURACY, COMPLETENESS, OR USEFULNESS OF ANY INFORMATION, APPARATUS, PRODUCT, 
OR PROCESS DISCLOSED, OR REPRESENTS THAT ITS USE WOULD NOT INFRINGE PRIVATELY OWNED RIGHTS. *) 

GetKd[file_] := Module[{strm, foo, n1, Kd}, 
						strm = OpenRead[file];
						foo = Read[strm, String];
						n1 = Read[StringToStream[foo], Number];
						Read[strm, String];
						foo = ReadList[strm, {Word, Word, Number, Number, Number}, n1]; 
						Close[strm];
						{Transpose[foo][[2]], Transpose[foo][[3]], Transpose[foo][[4]], Transpose[foo][[5]]}]
 
 
CreateRetDist[density, delta1, delta2, delta3, file1, file2, file3, dirname ] := Module[{directory, theta, Dp, Rm, Delta, density = 2700},
						 
						 (* get Kd's, porosity, and diffusion coefficients *)
						
						  foo = GetKd[file1];
						  Kd1 = foo[[2]]; 
						  diff1 = foo[[3]]; 
						  phi1 = foo[[4]]; 
						  e1 = foo[[1]]; 
						  foo = GetKd[file2]; 
						  Kd2 = foo[[2]]; 
						  diff2 = foo[[3]]; 
						  phi2 = foo[[4]]; 
						  e2 = foo[[1]]; 
						  foo = GetKd[file3];
						  Kd3 = foo[[2]]; 
						  diff3 = foo[[3]]; 
						  phi3 = foo[[4]]; 
						  e3 = foo[[1]]; 
						  If[e1 != e2 || e1 != e3 || e2 != e3, Return[-1]];
						  elements = e1;
						  
						  					  
						  SetDirectory[ DirectoryName[file3]]; 

						  Delta = Drop[FoldList[Plus, 0, {delta1, delta2, delta3}], 1];
						  
						  CalcPrecision = 30;
						  Kd1 = SetPrecision[Kd1,CalcPrecision];
						  Kd2 = SetPrecision[Kd2,CalcPrecision];
						  Kd3 = SetPrecision[Kd3,CalcPrecision];
						  diff1 = SetPrecision[diff1,CalcPrecision];
						  diff2 = SetPrecision[diff2,CalcPrecision];
						  diff3 = SetPrecision[diff3,CalcPrecision];
						  phi1 = SetPrecision[phi1,CalcPrecision];
						  phi2 = SetPrecision[phi2,CalcPrecision];
						  phi3 = SetPrecision[phi3,CalcPrecision];
						  Delta = SetPrecision[Delta,CalcPrecision];
						  density = SetPrecision[density,CalcPrecision];
						  
						  (*decide who to plot*)
						  n1 = Length[Kd1];
						  plotflag = Table[False, {n1}];
						 						  
						  (*save beta array for reuse below*)
						  betaarr = Table[10^lbeta, {lbeta, 1, 7, 0.25}];
						  betaarr = SetPrecision[betaarr, CalcPrecision];
						  
						  (*loop over all elements*)
						  
						  bigresult = Table[
						  Kd = {Kd1[[i]], Kd2[[i]], Kd3[[i]]};
						  theta = {phi1[[i]], phi2[[i]], phi3[[i]]}; 
						  Dp = {diff1[[i]], diff2[[i]], diff3[[i]]}/theta;
						  Rm = 1 + Kd*density/theta;
						  result = 
						  Table[beta = betaarr[[i]]; Layer3[theta, Dp, Rm, Delta, beta],
						  {i, 1, Length[betaarr]}];
						  result, {i, 1, n1}];
						  
						  (*get directory for save*)
						  
						  flag = True; 
						  directory = dirname;  (* need directory name here *) 
						 						  
						  (*Loop over all elements and write one datafile for each*)
						  
						  Do[outfile = directory <> "retdist.dat" <> ToString[inuc];
						  strm = OpenWrite[outfile, DOSTextFormat->False];
						  Write[strm, Length[betaarr]];
						  Do[Export[strm, betaarr[[i]]//N, "List"];
						  WriteString[strm, "\n"];
						  result = SetPrecision[bigresult[[inuc, i]], 12];
						  
						  newresult = 
						  Table[If[result[[i + 1, 2]] > result[[i, 2]], result[[i + 1]], Null], {i, 1, Length[result] - 1}];
						  newresult=Prepend[newresult, result[[1]] ]; 
						  newresult = DeleteCases[newresult, Null];
						  
						 newresult = Table[If[newresult[[i , 2]] >  10^-20 , newresult[[ i ]], Null], {i, 1, Length[newresult] - 1}];
						   newresult = DeleteCases[newresult, Null]; 
						  newresult = Append[newresult, {result[[-1,1]], Chop[result[[-1,2]], 10^-20]} ]; 
						  
						  
						  newresult = Transpose[newresult];
						  newresult = Transpose[{newresult[[2]], newresult[[1]]}];
						  Write[strm, Length[newresult]];
						  Export[strm, newresult//N, "Table"];
						  WriteString[strm, "\n"], {i, 1, Length[betaarr]}];
						  Close[strm], {inuc, 1, Length[bigresult]}]; 
						  
						  (* now save another file with ka and effective diffusion coefficients *) 
						  ka = Table[0, {Length[bigresult]}]; 
						  
						  DoverR = Table[ 
						  Kd = {Kd1[[i]], Kd2[[i]], Kd3[[i]]};
						  theta = {phi1[[i]], phi2[[i]], phi3[[i]]}; 
						  Deff = {diff1[[i]], diff2[[i]], diff3[[i]]}; 
						  Rm = 1 + Kd*density/theta; 
						  Apply[Plus, Delta]/Apply[Plus, Delta *theta /(Deff/Rm)],  
						  {i, 1, Length[bigresult]}]; 
						  
						  outfile = directory <> "auxdata.dat"; 
						  strm = OpenWrite[outfile,  DOSTextFormat->False]; 
						  WriteString[strm, "! created from mathematica notebook 3layerinterface\n" ]; 
						  WriteString[strm, "! Delta\n"]; 
						  WriteString[strm, "! ka deffavg\n"]; 
						  Write[strm, Delta[[-1]]//N ]; 
						  Export[ strm, N[Transpose[ {ka, DoverR}],8] ]; 
						  Close[strm]  
						  
						  ];
						  
CreateRetDist[ density, delta1, delta2, delta3, file1, file2, file3, dirname]
						  
			
						  
					
