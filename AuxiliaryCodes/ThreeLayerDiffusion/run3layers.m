(* ::Package:: *)

(* to invoke type "math -script thisfile" *) 
(* or "MathKernel -script thisfile" *) 
(* use full filepath in file names *) 

density=2700.; 
mc=0.; 
kca=0.; 
delta1=0.005; (* size of layer 1 *) 
delta2=0.002; (* size of layer 2 *) 
delta3=10.;   (* size of layer 3 *) 
file1="./TestResults/kd1.dat"; (* kd, porosity, deff for layer 1  *) 
file2="./TestResults/kd2.dat"; (* layer 2 *) 
file3="./TestResults/kd3.dat"; (* layer 3  *) 
fracparms="./TestResults/kc.dat"; (* in-fracture parameters, ka and kc, etc *)  
dirname="/Users/spainter/Workstuff/SKB_WFO_Project/MARFA/ThreeLayerDiffusion/TestResults/"; (* note slash at end - necessary on Mac OS, probably others *) 

<<"/Users/spainter/Workstuff/SKB_WFO_Project/MARFA/ThreeLayerDiffusion/ThreeLayerRetention.m" 
<<"/Users/spainter/Workstuff/SKB_WFO_Project/MARFA/ThreeLayerDiffusion/3LayerNoInterface.m" 
