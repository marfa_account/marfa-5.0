!results.md
!file created on: 20220123 at 112716.514
!MARFA Version 5.0.3
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 1000000
!aseed=          505      761540                                                        
              1000000
           2
 A1           6.9314700000000002E-005     1000000
 B1           6.9314699999999997E-004           0
