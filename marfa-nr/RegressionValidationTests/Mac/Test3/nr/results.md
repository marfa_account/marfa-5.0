!results.md
!file created on: 20180610 at 170252.633
!MARFA Developer Version
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 500000
!aseed=           62        3844                                                        
      500000
           2
 A1           6.9314700000000002E-005      250152
 B1           6.9314699999999997E-004      249848
