!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Kate Walker and Scott Painter
!December 2009 
!The resulting file is organized as follows, for each node:       
!node number x-coordinate y-coordinate z-coordinate rocktype            
!number of downstream neighbors                                 
!list of downstream neighbors                                  
!probability of each link                                     
!time of each link                                           
!length of each link                                        
!beta of each link                                         
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
program data

implicit none

	integer(kind=8) :: np, mout, key, imodel, ntext, npouts, npart 
        integer :: i,j,k
        integer(kind=8) :: npnods, npndin, mstep, mstep2, mxlst, mpths, position , ifrac
        integer(kind=8) :: nsize, endposition, par1, par2, par3, par4, number_fracture
	character(80) :: progid, runid, text
	character(100) :: nefname, pthname  
	integer(kind=8), dimension(:), allocatable :: tmprt 
        character(len=15)  :: rocktype 
	integer(kind=8), dimension(:), allocatable :: destination, intstuff 
        integer, dimension(:), allocatable :: neighbors 
	integer(kind=8), dimension(:), allocatable :: number, ibcout,intlst, parameter
	real(kind=kind(1.0d0)), dimension(:), allocatable :: timout, flxout, flxtot, aperture, realstuff 
	real(kind=kind(1.0d0)) , dimension(:),allocatable :: ratout, disout 
        real(kind=kind(1.0e0)), dimension(:), allocatable :: time, probability, length, beta 
	real(kind=kind(1.0d0)) , dimension(:,:), allocatable :: fnodlc 
        real(kind=kind(1.0d0)) :: fractureaperture 
	integer(kind=8), dimension(:,:), allocatable :: nodpln,ndespt 
        integer :: ios ,maxsize, numlowprob=0 
        logical :: aflag=.false. 

        print *, ' enter .nef file name ' 
        read(*,*) nefname 
        print *, ' enter .pth file name ' 
        read(*,*) pthname 
        print *, ' enter 1 for ASCII output, 0 for unformatted' 
        read(*,*) ios 
        if(ios .eq. 1) aflag=.true. 

OPEN(UNIT=7, FILE=nefname)
open(unit=8,action='read',form='unformatted', file=pthname, status='old', iostat=ios)
if(aflag) then  
  open(unit=9, file='nodedata.dat')
  open(unit=10, file='flowfield.dat') 
else 
  open(unit=9, file='nodedata.dat', form='unformatted') 
  open(unit=10, file='flowfield.dat', form='unformatted') 
end if 

read(UNIT=8) key          !the below read statements obtain nodal information from formatted.dat
read(UNIT=8) imodel
read(UNIT=8) ntext
read(UNIT=8) progid
read(UNIT=8) runid
read(UNIT=8) text

read(UNIT=8) np, mout, npouts, npart, npnods, npndin, mstep, mstep2, mxlst, mpths

print *, np, mout 

if( np .ne. mpths) then 
 print *, 'mpths not equal to np' 
 print *, 'stopping' 
 stop 
end if 

allocate (ndespt(2,np))
allocate (number(np))
allocate (ratout(mout))
allocate (destination(mout))
allocate (timout(mout))
allocate (disout(mout))
allocate (flxout(mout))
allocate (flxtot(np))
allocate (ibcout(np))
allocate (intlst(mxlst))
allocate (fnodlc(3,mpths))
allocate (nodpln(2,mpths))
	

	read(UNIT=7, FMT=*) number_fracture
        allocate(aperture(number_fracture))
        allocate(tmprt(number_fracture) ) 
        allocate(realstuff(10))
        allocate(intstuff(10))


        aperture = -1.0e0 

	do i=1, number_fracture
         read(UNIT=7,FMT=*) (realstuff(j),j=1,10)
         read(UNIT=7,FMT=*) (intstuff(j),j=1,10)
         read(UNIT=7, FMT=*) (realstuff(j), j=1,10)
         aperture(i) = realstuff(10) 
         tmprt(i) = intstuff(5) 
	end do



	read(UNIT=8) ndespt
        number = ndespt(2,:) 

	read(UNIT=8) ratout
	read(UNIT=8) destination
	read(UNIT=8) timout
	read(UNIT=8) disout
	read(UNIT=8) flxout
	read(UNIT=8) flxtot
	read(UNIT=8) ibcout
	read(UNIT=8) intlst

	
	read(UNIT=8) fnodlc
	read(UNIT=8) nodpln
        close(8) 
	
	ndespt= ndespt +1
        if( aflag) then 
          write(UNIT=9, fmt=*) np
          write(unit=10, fmt=*) np 
        else 
          write(UNIT=9) int(np) 
          write(unit=10) int(np) 
        endif 
	
       maxsize=0 
       do i=1,np

	position = ndespt(1,i)
	nsize = number(i)
        if( nsize .gt. maxsize) then 
          maxsize=nsize 
        end if 

        par1=nodpln(1,i)      
        if( par1 .le. 0  ) then 
          rocktype = "0" !no fracture information   
        else 
          write(rocktype,'(i0)') tmprt( par1 )  
        end if 

        if( aflag) then 
	 write(UNIT=9,FMT="(I10,3g20.8,2a)") i, & 
           fnodlc(1,i),fnodlc(2,i),fnodlc(3,i)," ", rocktype 
         write(UNIT=10,FMT=*) number(i)
        else 
	 write(UNIT=9) i, & 
           real(fnodlc(1,i)),real(fnodlc(2,i)),real(fnodlc(3,i)),rocktype 
         write(UNIT=10) int( number(i)) 
        end if 

        if (number(i) .ne. 0) then    

         endposition=position + nsize -1
	 allocate (neighbors(nsize))
	 allocate (probability(nsize))
	 allocate (time(nsize))
	 allocate (length(nsize))
			
	 do j=position, endposition                                   
	       neighbors(j-position+1)=int( destination(j) )       !stores neighbors for each associated nodes link(s)
	       time(j-position+1)=timout(j)                 !stores time for each associated nodes link(s)
               length(j-position+1)=disout(j)               !stores length for each associated nodes link(s)
	 end do
         time=time/31536000. !convert to years 

         ! change from cumulative to not cumulative 
         probability(1)=ratout(position) 
         if(probability(1) .lt. 0.2) numlowprob=numlowprob+1  
	 do j=position+1, endposition                                   
	     probability(j-position+1)=ratout(j)-ratout(j-1)          !stores probability for each associated nodes link(s)
             if(probability(j-position+1) .lt. 0.2) numlowprob=numlowprob+1  
         end do 

         if( nsize .eq. 1 .and. probability(1) .lt. 0.99) then 
           print *, probability
           print *, neighbors
         endif 
          


         allocate (parameter(nsize))
	 allocate (beta(nsize))

	 do k=1,nsize 
          par1=(nodpln(1,i))      
          par2=(nodpln(2,i))
          par3=(nodpln(1,neighbors(k)))
          par4=(nodpln(2,neighbors(k)))
	   if (par1 .eq. par3) then
		parameter(k)=par1
	   else if (par1 .eq. par4) then
		parameter(k)=par1
	   else
	        parameter(k)=par2
           end if

           if( parameter(k) .gt. size(aperture) ) then 
             print *, 'inconsistency between nef and pth file' 
             print *, 'stopping' 
             stop 
           end if 


           if( parameter(k) .le. 0  ) then !no fracture information  
             beta(k) = time(k) 
           else 
             fractureaperture=aperture(parameter(k))     
             if( fractureaperture .gt. 0.0) then 
               beta(k)= (2*time(k))/fractureaperture        
             else 
               beta(k) = 0.0 
             end if 
           end if 

          end do

		
              	
        if( aflag) then 
         write(UNIT=10, FMT=*) neighbors
         write(UNIT=10,FMT="(99g20.8)")  probability
         write(UNIT=10,FMT="(99g20.8)") time
         write(UNIT=10, FMT="(99g20.8)") length
         write(UNIT=10,FMT="(99g20.8)") beta
        else 
         write(UNIT=10) neighbors
         write(UNIT=10) probability
         write(UNIT=10) time
         write(UNIT=10) length
         write(UNIT=10) beta
        end if 
	
	deallocate (neighbors)
	deallocate (probability)
	deallocate (time)
	deallocate (length)
	deallocate (parameter)
	deallocate (beta)

	end if
	

		
      end do
      print *, 'max number of links: ', maxsize  
      print *, 'number low probability links: ', numlowprob 

close(9)
close(8)
end program data
