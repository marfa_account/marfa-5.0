!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland).
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


! *********************************************************************************
! Program Name   :      marfa  
! Developer      :      Scott Painter 
! Developer Phone:      +1 210 722 9739 
! Version Number :      developer
! Client         :      SKB and POSIVA  
! Revision History:     Version 3.1 December 2006 
!                       Version 3.2 November 2007  
!                       Version 3.2.1 April 2008 
!                       Version 3.3 December 2009 (SRC 723)
!                       Version 3.3.1 September 2011 
!                       Became open source in 2014 - not versioning  
! *********************************************************************************

!  this code solves for radionuclide transport along a one-dimensional pathway 
!  advection, longitudinal dispersion, retention processes, 
!  decay, ingrowth, and time-dependent flow are accounted for 
!  method is by an extended time-domain random walk algorithm 
!  Painter et al., International High Level Radioactive Waste Management Conference 2006 

!  Version 3.2 allows for changes in flow velocity including flow reversal 
!  also allows for changes in sorption properties 

!  Version 3.3 allows for connect flow node routing information to be used to allow for
!  a more complete investigation of gradient changes and changes in sorption properties

! Developed by: Scott Painter and James Mancillas   

! *********************************************************************************

program marfa 
 

  use precision 
  use io 
  use nuclide_class 
  use longdisp 
  use hydrofacies
  use noderouting
  use source 
  use particle_class 
  use stack 
  use postprocess
  use loop_diagnosis

  implicit none  
 
  logical :: active ,  last, runtransport, rescalc, asciiflag
  logical :: file_exists 
  
  character(len=10) :: srctag 
  character(len=10) :: command
  character(len=80) :: dumstring
  integer :: trjtag

  integer :: i ,nn ,nelem ,next ,iunit, funit, ival, iepoch
  integer :: one=1 
  integer :: cunit , tunit, punit 
  integer :: iostat 
  integer :: isplits, nsplits !V3.3.1 
  integer, dimension(1) :: ipos 
  integer :: eflag 

  real(kind=dp) :: tau ,beta, dt ,rt  ,  ret_time , r, deltaT 
  real(kind=dp) :: seglen , s
  real(kind=dp) ::  la, taubar, betabar 
  real(kind=dp) :: frac , disp 
  real(kind=dp) :: wfac
 
  real(kind=dp), dimension(3) :: posi 

  type(particle), target :: crnt_particle 
  
  ! aliases for particle components 
  integer, pointer :: crnt_nuclide , crnt_elem
  real(kind=dp), pointer  :: time  , weight 
  real(kind=dp), pointer :: x

  ! list of nuclides, all chains  
  type(nuclide), dimension(:), allocatable, target :: nuclidelist 

  integer(kind=npk) :: npart, ipart 
  integer :: ntraced

  integer :: seedsize 
  integer, dimension(8) :: idate ! V3.3.1 
  integer, dimension(:), allocatable :: aseed !4amphos

  integer, dimension(:), allocatable :: actualsource 

#ifdef ASCII_FILE 
  asciiflag=.true. 
#else 
  asciiflag=.false. 
#endif 

! *********************************************************************************

  ! print some information to the screen QA requirement 
  call writeheader(6)
  
!  check command line arguments
  runtransport=.true.
  call getarg(1,command)
  if( len_trim(command) .gt. 0)  then
    if(command(1:3) .eq. 'ppo' .or. command(1:3) .eq. 'PPO') then
        runtransport=.false.
    else
        read(command,*,IOSTAT=eflag) npart
        if(eflag .ne. 0) then
           print *, 'please enter number of particles on command line'
           stop
        end if 
        call getarg(2,command) 
        read(command,*,IOSTAT=eflag) ntraced 
        if(eflag .ne. 0) then 
          ntraced=0 
        else 
          print *, ntraced,' particles traced' 
        end if 
    end if
  else
    print *, 'must have something on the command line'
    print *, ' number of particles or the PPO option'
    print *, ' see users manual'
    print *, 'stopping'
    stop
  end if

  if(runtransport)then

  ! load the nuclide and element list (local procedure) 
  call get_nuclides
  if( nelem .gt. 0 .and. nn .gt. 0 ) then 
    print *, "element and nuclide lists OK"
    print *, nn, " nuclides " 
    print *, nelem, " elements "  
  end if 
  allocate(actualsource(nn)) 
  actualsource = 0 

  call init_hf(nelem)                             ! load the hydrofacies 
  print *, "initialized the rocktypes module" 
  print *, size(hfset), " rock types found" 
 
  call init_node()     ! load node data files nodedata.dat, 
  print*, "initialized nodes"
 
  call init_source(nuclidelist)             ! load any source information 
                                            !  also sets the public variable npart 

  call random_seed(SIZE=seedsize)
  allocate(aseed(seedsize))
  call random_seed(GET=aseed)
  inquire(file="seedfile.dat", exist=file_exists)
  if( file_exists ) then
    iunit=openread("seedfile.dat")
    idate=0
    aseed=0
     read(iunit, *) idate(1:2)
     aseed(1:seedsize:2)=idate(1)
     aseed(2:seedsize:2)=idate(2)
    close(iunit)
  else
    call date_and_time(VALUES=idate)          ! get the time of day
    aseed(2:seedsize:2)=idate(8)*idate(7)*idate(6)
    aseed(1:seedsize:2)=idate(8)
  end if
  call random_seed(PUT=aseed)
  call random_number(r)

  funit = findunit() 
  if(asciiflag) then 
     open(unit=funit, file="results.dat") 
  else 
     open(unit=funit, form='unformatted', file="results.dat") 
  end if 

  cunit=findunit()
  open(unit=cunit, form='unformatted', file = "concresults.dat")

  if(ntraced .gt. 0) then 
    tunit=findunit() 
    open(unit=tunit, file='tmp_trajectories.dat') 
  end if 
  
  print *, "launching ", npart, " particles " 

! *********************************************************************************
! presample the source and write to file  
! *********************************************************************************
  punit = findunit() 
  open(punit, form='unformatted',file='particle_queue') 
  do ipart = 1,npart 

   call sample_source(crnt_particle,srctag,trjtag,nn,npart) ! trjtag(INT)= initial trajectory/node

   call random_number(r) 
   if( r .lt. float(ntraced)/float(npart)) crnt_particle%traced = .true. 

   call activate_node(trjtag,crnt_particle)  ! establish the active node

   crnt_nuclide => crnt_particle%nuclide 
   actualsource(crnt_nuclide)=actualsource(crnt_nuclide)+1 
   crnt_particle%elem = nuclidelist(crnt_nuclide)%ielem 

   crnt_particle%splitable = .FALSE. !V3.3.1 only split on ingrowth  

   crnt_particle%ipart = ipart 

   call write_particle( crnt_particle, punit, iostat) 
   if(iostat .ne. 0) then 
     print *, 'error in writing particle queue file' 
     stop 
   end if 
  end do 

  ipart = npart ! will keep as running sum for splits 

     

  ! loop over epoch 
  do iepoch=1,numepoch 

    call load_flowfield( iepoch ) 

  ! read and add to queue 
    rewind(punit) 
    do 
      !  add to the subparticle queue 
      call read_particle(crnt_particle, punit, iostat)   
      if( iostat .ne. 0) exit 
      call add2queue(crnt_particle) 
    end do 
    close(punit,status='delete') 
    open(unit=punit, form='unformatted',file='particle_queue') 
      
  partloop: do 
   if( emptyqueue) exit 

   ! pop from the subparticle stack 
   crnt_particle = fromqueue()

   time         => crnt_particle%time 
   weight       => crnt_particle%weight 
   x            => crnt_particle%x 
   crnt_nuclide => crnt_particle%nuclide 
   crnt_elem    => crnt_particle%elem  

   if(crnt_particle%crnt_epoch .gt. iepoch) then 
    call write_particle(crnt_particle,punit,iostat) 
    cycle partloop
   end if 

  ! keep sampling segments until monitoring boundary is reached  

  ! this loop was extensively rewritten for V3.2 
  segloop: do 

   call advance_node(crnt_particle,taubar,betabar,seglen,disp) 

   if( crnt_particle%nxtseg .le. 0) exit 

     if(seglen .gt. 0.01)then
 
         ! longitudional dispersion 
         if( disp .gt. seglen/1000.) then      !  dispersion  
           la = seglen/disp 
           tau = sample_ld(la, taubar) 
         else                                  ! almost no dispersion  
           tau = taubar
         end if 
   
         beta = betabar * tau/taubar 

         call random_number(r) 

         ret_time = sample_node_rt(crnt_particle,beta,r)  ! retention time
 
         rt = tau + ret_time                         ! total residence time 

     else
  
         beta = betabar 
         tau = taubar 
         rt = tau

     end if ! seglen <=0 
    

     dt = sample_dt(crnt_nuclide)                ! decay time  
     deltaT =epochbndr-time                      ! time to next change in flow

     frac = 1.0d0 


     do               ! loop here until segment is completed 

       if( rt .lt. dt .and. rt .lt. deltaT) exit    ! reaches end of segment 

        if( deltaT .lt. rt .and. deltaT .lt. dt) then  !flow change 

            rescalc = check_for_rescalc(crnt_particle)
            if( rescalc ) then
              write(unit=cunit) &
               crnt_nuclide, one, crnt_particle%iseg, time, & 
               time+deltaT, weight*tau/rt , weight*(1.0-tau/rt), frac
            end if

            call random_number(r) 
            ret_time = sample_node_rt2(crnt_particle, deltaT,r)  ! time required to return to the fracture 

            time = time + deltaT+1.0d-7
            time = time + ret_time                          ! update clock for time to return to fracture 

            dt = sample_dt(crnt_nuclide) 
            do                                     ! check for decay while in matrix and after flow change 
              if( dt .gt. ret_time) exit           ! advance along the chain if so  
              crnt_nuclide =  nuclidelist(crnt_nuclide)%next  
              crnt_particle%splitable = .TRUE. 
              if( crnt_nuclide .eq. 0) exit                     ! end of decay chain   
              crnt_elem = nuclidelist(crnt_nuclide)%ielem 
              dt = dt + sample_dt(crnt_nuclide)                
            end do  
 
            frac = 1.0-sqrt(deltaT)/sqrt(rt)

            if( crnt_nuclide .eq. 0) exit                     ! end of decay chain exit do loop 
            call random_number(r) 
            rt = tau + sample_node_rt(crnt_particle,beta,r) 
            rt = frac*rt

         else  ! decays 

            rescalc = check_for_rescalc(crnt_particle)
            if( rescalc ) then
              write(unit=cunit) &
               crnt_nuclide, one, crnt_particle%iseg, time, time+dt, & 
               weight*tau/rt , weight*(1.0-tau/rt), frac
            end if

            time = time + dt 

            crnt_nuclide =  nuclidelist(crnt_nuclide)%next  
            crnt_particle%splitable = .TRUE.
            if( crnt_nuclide .eq. 0) exit                     ! end of decay chain   
            crnt_elem = nuclidelist(crnt_nuclide)%ielem 

            frac=1.0d0-dt/rt                  ! fraction of full segment remaining  

            if(seglen .gt.0.01)then
               ret_time = sample_node_rt(crnt_particle,beta,r)
            else
               ret_time =0.0d0
            end if

            rt = tau + ret_time                       ! res time for full segment 
            rt = rt*frac 
            
            tau=tau*frac 
            beta=beta*frac 

         end if 

         dt = sample_dt(crnt_nuclide)   ! new decay time  
         deltaT = epochbndr-time   

     end do             ! end of segment 

     ! if here then survives the segment 

     x = x + seglen  
     time = time + rt 

   ! exit if crossing monitoring location  or if end of chain 
   active = .true.

    last= check_for_node_termination(crnt_particle)
 
   if( last .or. crnt_nuclide .eq. 0 ) active = .false.  
   if( .not.active ) exit 
   if( time .gt. 1.0d9) exit 

   rescalc = check_for_rescalc(crnt_particle)
   if( rescalc) then 
      write(unit=cunit) &
        crnt_nuclide, one, crnt_particle%iseg, time-rt, & 
        time, weight*tau/rt , weight*(1.0-tau/rt), frac
   end if
 
   !check for traced particle 
   if( crnt_particle%traced) then 
      call query_node(crnt_particle, posi) 
      write(unit=tunit,fmt=*) & 
        crnt_particle%ipart, time, crnt_nuclide, crnt_particle%iseg , posi 
   end if 
        
   ! check for split 
   !sp V3.3.1 
   nsplits=nuclidelist(crnt_nuclide)%splitatbirth
   if( nsplits .gt. 0 .and. crnt_particle%splitable) then
     weight = weight/float(nsplits+1)
     crnt_particle%splitable = .FALSE.
     do isplits=1,nsplits
      ival = crnt_particle%ipart 
      ipart=ipart+1 
      crnt_particle%ipart = ipart 
      call add2queue(crnt_particle)
      crnt_particle%ipart = ival 
     end do
   end if

   if(crnt_particle%crnt_epoch .gt. iepoch) then 
    call write_particle(crnt_particle,punit,iostat) 
    cycle partloop
   end if 



  end do segloop ! end of all segments

    if( crnt_nuclide .gt. 0) then 
    if( nuclidelist(crnt_nuclide)%splitatbirth .lt. 0 ) then !russian roulette
      call random_number(r)
      wfac=abs( nuclidelist(crnt_nuclide)%splitatbirth)
      r=r*wfac
      if( r .lt. 1.0d0) then
       weight=weight*wfac
      else
       crnt_nuclide = 0
      end if
    end if
    end if 

  
  if( crnt_nuclide .ne. 0 .and. time .lt. 1.0d9) then
     if( asciiflag ) then 
        write(funit,'(e16.9,i3,2x,e12.5,2x,a10,i10,2x,e12.5)') & 
        time,crnt_nuclide, weight , srctag ,crnt_particle%nxtseg, crnt_particle%cum_time 
     else 
        write(funit) &  
        time,crnt_nuclide, weight , srctag ,crnt_particle%nxtseg, crnt_particle%cum_time 
     end if 
  end if 

  if(crnt_particle%traced) then
     call query_node(crnt_particle, posi) 
     write(unit=tunit,fmt=*) & 
        crnt_particle%ipart, time, crnt_nuclide, crnt_particle%iseg , posi 
     if( crnt_particle%nxtseg .lt. 0 ) then
       write(unit=tunit, fmt=*) '-1'  ! looping
     else if(last) then
       write(unit=tunit, fmt=*) '0'  ! reaches boundary
     else if(crnt_nuclide .eq. 0) then
       write(unit=tunit, fmt=*) '-2' ! decayed
     else if(time .gt. 1.0d9) then
       write(unit=tunit, fmt=*) '-3' ! out of time
     else
       print *, 'this cannot happen: stopping'
     endif
   endif

  end do partloop ! end  of particle stack 
  call free_flowfield(iepoch) 
  end do ! end of epoch loop 
 
  close(funit) 
  close(cunit) 
  close(punit,status='delete') 

  ! write the metadata 
  funit=findunit()                          ! open file for results 
  open(unit=funit, file = "results.md")    ! and print some header information 
  write(dumstring,*) aseed(1:2)
  call writeheader(funit,npart,dumstring)

  write(funit,*) npart
  write(funit,*) nn 
  do i=1,nn 
   write(funit,*) nuclidelist(i)%name,nuclidelist(i)%lambda,actualsource(i) 
  end do 
  close(funit) 


  else    ! particle transport calculations not performed
    print*, "Command line argument ", command
    print*, "Performing breakthrough calculations only"
    print*, "Breakthrough calculations using previous results in 'results.dat' "
  end if   ! run transport calculation
  
! *********************************************************************
! *****                    POST PROCESSOR                     *********
! *********************************************************************

  print*, "begin post processing"
  call post_process(asciiflag)
  print*, "completed post processing"


  print*, "performing loop diagnosis"
    call run_diagnosis(asciiflag)
  print*, "completed loop diagnosis"


  if(ntraced .gt. 0) then 
    print*, "reshuffling trajectory files" 
    call reshuffle_trajectories(tunit) 
  end if

  print *, "normal termination of marfa" 

  stop 

! *********************************************************************

  contains 

! *********************************************************************
! *****               SUBROUTINES and FUNCTIONS                  ******
! *********************************************************************
    subroutine get_nuclides
 
       ! list of element names  
       character(len=10),  dimension(:), allocatable :: elementnames 
       character(len=10),  dimension(:), allocatable :: nucnames  
 
       logical :: ex 

       ! get list of elements  
       iunit=openread("nuclides.dat") 
       read(iunit,*) nelem
       allocate(elementnames(nelem)) 
       do i=1,nelem 
          read(iunit,*) elementnames(i) 
       end do 

       ! get a temporary list of nuclides
       read(iunit,*) nn
       allocate(nucnames(0:nn))
       do i=1,nn
          read(iunit,*) nucnames(i)
       end do
       nucnames(0)='NULL'  

       ! got the names, now rewind and reposition
       rewind(iunit)
       call skipheader(iunit) 
       do i=1,nelem+1
        read(iunit,*)
       end do

       ! get nuclide list
       read(iunit,*) nn
       allocate(nuclidelist(nn))
       do i=1,nn
         nuclidelist(i)=nuclide_(iunit, elementnames,nucnames,nn)
      end do

      close(unit=iunit)
      deallocate(nucnames)
      deallocate(elementnames) 


    end subroutine get_nuclides
!*********************************************************************
FUNCTION sample_dt(oldnuclide) result(dt) 
!*********************************************************************
!  this functions samples the decay time 
!  list of nuclide objects available globally 
!  input is index in that list 

 integer :: oldnuclide

 real(kind=dp) :: r , dt  

 call random_number(r)  

 dt = -log(r)/nuclidelist(oldnuclide)%lambda 

 end function sample_dt 
!**********************************************************************
subroutine reshuffle_trajectories(tunit ) 
  integer :: tunit,i,ntraced,ipart,tflag,nuclide,iseg  ,nwunit,ios 
  integer, dimension(:), allocatable :: termflag, partID
  real(kind=dp), dimension(3) :: posi 
  real(kind=dp) :: time 

    rewind(tunit) 

    i=0 
    do 
      read(unit=tunit,fmt=*,end=101) ipart
      if(ipart .le. 0) i=i+1
    end do 
101 continue
    ntraced=i 
    allocate(termflag(ntraced)) 
    allocate(partID(ntraced)) 
    rewind(tunit) 
    i = 0 
    do 
      read(unit=tunit,fmt=*,end=102) tflag 
      if(tflag .le. 0) then 
         i=i+1
         termflag(i)=tflag 
         backspace(tunit) 
         backspace(tunit) 
         read(unit=tunit,fmt=*) ipart 
         partID(i)=ipart 
         read(tunit,*) 
      end if 
    end do 
102 continue 
    if(i.ne.ntraced) then 
      print *, 'this should not happen' 
      stop 
    end if 
   
    nwunit=findunit() 
    open(unit=nwunit,file="particle_trajectories.dat") 
    write(nwunit,*) ntraced 
    do i=1,ntraced 
     rewind(tunit) 
    do 
      read(unit=tunit,fmt=*,end=103,iostat=ios) ipart 
      if(ipart .eq. partID(i) ) then   
        backspace(tunit) 
        read(unit=tunit,fmt=*) ipart,time,nuclide,iseg,posi
        write(nwunit,*) ipart,time,nuclide,iseg,posi 
      endif 
    end do 
103 continue 
    write(nwunit,*) termflag(i) 
    end do 

    close(tunit,status='delete') 
    close(nwunit) 

end subroutine reshuffle_trajectories
end program marfa  



