!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland). 
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



! *********************************************************************
! File Name        : source.f95
! Program Name     : marfa  
! Developed for    : SKB and POSIVA  
! Revision History : version 3.1beta  June 2006 Scott Painter 
!                  : version 3.1betaE Dec 2006 SP  
!                  : version 3.1      Dec 2006 Scott Painter 
!                  : version 3.2.2    November 2009 Scott Painter
!                    SCR722 items 1, 6 and 7 
!                  : version 3.3      December 2009 James Mancillas
!                    SCR723
! SRD Section      : Section 3 item 8 
! *********************************************************************

! module source  
! This module is the source 
!  two public routines : 
!  init_source reads the source information 
!  sample_source provides a starting location and starting weight   
!  one public variable: 
!  npart is number of particles  

! added importance weighting december 2006 

module source 

!  input is from source.dat 

use precision 
use io 
use particle_class
use interpolation_class 
use nuclide_class 
! SRC 723
!use streamline
use noderouting
! SCR 723 end
implicit none 

private 


type nodelist
integer, dimension(:), allocatable          :: nodes
end type nodelist

type(nodelist), dimension(:),allocatable         :: initial_nodes


integer, dimension(:), allocatable               :: valid_source ! list of active sources
type(interpolation1d), dimension(:), allocatable :: srcobj   ! sources 
character(len=10),dimension(:), allocatable      :: srcid 

real(kind=dp),dimension(:),allocatable           :: cmprob   ! cumulative prob for selecting  
real(kind=dp),dimension(:),allocatable           :: wfac     ! importance weight each nuclide  

logical :: uniform_in_time !V3.2.2 

public sample_source , init_source 

contains 
!***********************************************************************
 subroutine init_source(nuclidelist) 
  ! this subroutine reads source information 
  ! user must specify cumulative mass released for each nuclide in the chain 
  !  zero release is  allowed for some nuclides 

  ! conversion converts Mol to Bq given the lambda in yr^-1
  ! using 365 day/yr * 24 hr/day * 3600 sec/hr
  ! Mol  6.022E23   conversion=1.9095636732E16

  integer                                           :: nnuc,i,j,ndp ,nsources ,m 
  integer                                           :: isrc ,inuc ,iunit,junit 
  integer                                           :: ios, numwarnings=0 !V3.2.2 
  integer                                           :: index1, index2,tindex,total 
  type(nuclide), dimension(:)                       :: nuclidelist
  character(len=6)                                  :: unitname 
  character(len=80)                                 :: buffer ! V3.2.2  
  character(len=15)                                 :: samplehow  ! V3.2.2 
  integer                                           :: firstletter ! V3.2.2  
  integer                                           :: k, num_initial_nodes ! V3.3

  character(len=60), dimension(:), allocatable      :: pagebuff  ! V3.2.2 

  real(kind=dp)                                     :: norm,dt2 , afac
  real(kind=dp),dimension(:),allocatable            :: time 
  real(kind=dp),dimension(:,:),allocatable          :: cmass ! cumulative source  
  real(kind=dp),dimension(:,:),allocatable          :: massr ! rate at which mass is released  
  real(kind=dp)                                     :: conversion=1.9095636732E16
  real(kind=dp) :: wf 

  logical :: actflag   ! set to true if source is in Bq/yr 
  logical :: ex 

  nnuc=size(nuclidelist)

!****************************************
  iunit=openread("source.dat")
  print *, 'opening source.dat' 
  read(iunit,*) unitname  

  ! V3.2.2 
  read(iunit,*) buffer  
  firstletter = iachar( buffer(1:1) )   
  if(firstletter .ge. 65 .and. firstletter .lt. 122  ) then 
   read(buffer,*) samplehow 
   read(iunit,*) buffer 
  end if 
  uniform_in_time = .FALSE. 
  if( samplehow .eq. 'UNIFORM IN TIME') then 
    uniform_in_time = .TRUE. 
    print *, 'sources will be sampled uniformly in time' 
  end if 
  read(buffer,*) nsources 
  ! end V3.2.2 

!****************************************

  select case(unitname) 
  case('bq','Bq', 'BQ') 
    actflag = .true. 
  case('mol', 'MOL', 'Mol') 
    actflag = .false. 
  case default 
    print *, 'source specified in invalid units', unitname 
    stop 
  end select 

!****************************************

  junit=findunit( ) !sp moved from within the loop 

  allocate( srcobj(nnuc*nsources) ) 
  allocate( cmprob(nnuc*nsources) ) 
  allocate( srcid(nsources) ) 
  allocate(initial_nodes(nsources))

  m=0 
  allocate(valid_source(nsources))
  valid_source=1                     ! default all valid 
  
   do isrc=1,nsources 
  
   read(iunit,*) srcid(isrc)          ! read source name
   read(iunit,*) num_initial_nodes

   allocate(initial_nodes(isrc)%nodes(num_initial_nodes))

   do k=1,num_initial_nodes
    read(iunit,*) initial_nodes(isrc)%nodes(k)
   end do

   read(iunit,*) ndp     ! read number of time steps of source

     allocate( time(ndp) )                
     allocate( massr(ndp,nnuc) ) 
     allocate( cmass(ndp,nnuc) ) 

     allocate( pagebuff(ndp+5)) 

   
     do i=1,ndp              ! read in the source inventory
       read(iunit,*) time(i), massr(i,:)       
     end do                                    
 
     ! could check for valid release path here? 

   if( actflag ) then                              !  convert source data   
     do inuc=1,nnuc                                !  from Bq to mol if data
        afac = conversion*nuclidelist(inuc)%lambda !  it given in Bq
        massr(:,inuc) =  massr(:,inuc)/afac        !
     end do                                        !
   end if                                          



! sp added  wfac below: importance 12.05.2006
! if first,source  allocate and save a local copy of the weight 
! needed later for assigning the weight 
! why is this in the loop instead of before? 
   if( .not. allocated(wfac)) then
       allocate(wfac(nnuc))
       norm = 0.0d0
       do j=1,nnuc
        wfac(j) = nuclidelist(j)%importance
        norm = norm + wfac(j)
       end do
       wfac = wfac/norm
   end if
 
   do j=1,nnuc 
   cmass(1,j) = 0.0 
   do i=2,ndp
    dt2 = (time(i)-time(i-1))/2.0d0 
    !cmass(i,j) = cmass(i-1,j) + (massr(i-1,j)+massr(i,j))*dt2 *wfac(j) 
    cmass(i,j) = cmass(i-1,j) + (massr(i-1,j)+massr(i,j))*dt2 
   end do 
   end do 

   do j=1,nnuc 

     m=m+1 

! error check on source  
! additional error checking happens in interpolation routines  
    if(maxval(cmass(:,j)) .ne. cmass(ndp,j) ) then 
      print *, 'cumulative source not monotonic: stopping' 
      print *, j, m, maxval(cmass(:,j)), cmass(ndp,j), maxloc(cmass(:,j))  
      stop 
    end if  

 
     if( cmass(ndp,j) .gt. 0.) then 
        norm=cmass(ndp,j) 
     else 
        norm=1.
     end if 

     if( uniform_in_time ) then 

      write(pagebuff(1),*) ndp
! v3.3.1 switch to new interpolation  
!      write(pagebuff(2),*) time(ndp)
!      write(pagebuff(3),*) time(1)  
!      write(pagebuff(4),*) maxval(massr(:,j)) *wfac(j)/norm  
!      write(pagebuff(5),*) 0.0

      do i=1,ndp
        !write(pagebuff(i+1),'(2e12.5)') time(i),massr(i,j)/norm *wfac(j)
        write(pagebuff(i+1),'(2e12.5)') time(i),massr(i,j)
      end do

     else 

      write(pagebuff(1),*) ndp
! v3.3.1 switch to new interpolation  
!      write(pagebuff(2),*) cmass(ndp,j)/norm 
!      write(pagebuff(3),*) 0.0  
!      write(pagebuff(4),*) time(ndp) 
!      write(pagebuff(5),*) time(1)  

      do i=1,ndp 
       !write(pagebuff(1+i),'(2e12.5)') cmass(i,j)/norm ,time(i) 
       write(pagebuff(1+i),'(2e12.5)') cmass(i,j) ,time(i) 
      end do 

     end if  

!     srcobj(m) = interpolation1d_(pagebuff) 
!sp V3.3.1 switch to new interpolation routine 
     junit=findunit()
     open(unit=junit,file="tmpfile")
     do i=1,ndp+1
       write(junit,*) pagebuff(i)
     end do
     rewind(junit)
     srcobj(m) = interpolation1d_(junit)
     close(unit=junit,status='delete')
     !sp v3.3.1 


! calculate cumulative probability for selecting each 
     !totmass = totmass + cmass(ndp,j) 
     wf=wfac(j) 
     if( cmass(ndp,j) .le. tiny(0.0d0)) wf = 0.0 
     if( m .eq. 1) then
       cmprob(m) = wf
     else
       cmprob(m) = cmprob(m-1) + wf
     end if

    end do  ! end nuclide j loop 

    deallocate(pagebuff) 

    deallocate(cmass) 
    deallocate(massr) 
    deallocate(time) 
 
   end do  ! end source loop  

   close(iunit) 

   print *, 'initialized source module' 
   print *, nsources, ' sources found' 
   print *, 'number of active sources ',sum(valid_source(:))

   if(sum(valid_source(:)).lt.1)then
    print*,"No sources with valid release paths exist."
    print*,"Exiting ... "
    stop
   end if

   ! resize cmprob if checking for valid sources  
    cmprob = cmprob/cmprob(nnuc*nsources)

  return 
 end subroutine init_source 
!**********************************************************************
 subroutine sample_source(apart,trjtag1,trjtag2,nnuc,npart)  
  integer                                   :: crnt_nuclide
  integer                                   :: nnuc,isrc,nsrc,jsrc 
  integer(kind=npk)                         :: npart 
  type(particle)                            :: apart 
  real(kind=dp)                             :: time , r , weight 
  real(kind=dp)                             :: x , dt 
  real(kind=dp)                             :: norm 
  character(len=10)                         :: trjtag1  ! name of source
  integer                                   :: trjtag2  ! number of associated trajectory
  integer                                   :: imin, imax, itemp
 
  nsrc = size(cmprob) 

  ! select a nuclide from the cumulative probs 
  
  call random_number(r)
  do isrc=1,nsrc 
   if( r .lt. cmprob(isrc) ) exit 
  end do 


  if( isrc .eq. 1) then
   norm = cmprob(isrc)
  else
   norm = cmprob(isrc)-cmprob(isrc-1)
  end if
  norm = norm*real(npart,dp)

  ! V3.2.2 

  crnt_nuclide = mod(isrc-1,nnuc) + 1 

  call random_number(r)

  if( uniform_in_time) then 
    dt=(srcobj(isrc)%max1-srcobj(isrc)%min1)
    time = r*dt + srcobj(isrc)%min1  
    weight = interpolate(srcobj(isrc),time)*dt 
    weight = weight/norm 
  else 
    time = interpolate(srcobj(isrc),srcobj(isrc)%max1*r)  
    weight = 1.0d0  
    weight = weight * srcobj(isrc)%max1/norm
  end if 
  
  jsrc = (isrc-1)/nnuc  + 1 
  trjtag1=srcid(jsrc)    ! trjtag1 contains name of source

  x=0.0 

  ! end V3.2.2 

  apart%time = time 
  apart%weight = weight 
  apart%nuclide = crnt_nuclide 
  apart%x = x 
  apart%iseg = 0    ! current segment  
  apart%nxtseg = 1  ! next segment  
  apart%xi = 0.0d0  ! starting position on next segment  
  apart%loop_time =  0.0d0  ! init loop time to zero
  apart%cum_time  =  0.0d0  ! init cum time to zero
  apart%inloop    = .false. ! init loop status to false
  apart%traced = .false. !trace or not 

  apart%crnt_epoch = get_epoch( time ) 


! randomly select a neighboring node

       call random_number(r)

       itemp=int(r*(size(initial_nodes(jsrc)%nodes)))+1
 
       trjtag2=initial_nodes(jsrc)%nodes(itemp) ! selected node
 
  return 

  end subroutine sample_source 
!**********************************************************************
end module source 

