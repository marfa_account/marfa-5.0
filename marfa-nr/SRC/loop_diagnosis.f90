!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland). 
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


!***********************************************************************
!  File Name        : loop_diagnosis.f90
!  Program Name     : marfa 3.3.2
!  Developed for    : SKB and Posiva
!
!***********************************************************************
module  loop_diagnosis
!***********************************************************************


!***********************************************************************

use precision
use io
use qsort_module

implicit none


public run_diagnosis

private

type RejOrDel                         ! delayed or rejected particles
  integer                 :: RN 
  character(len=7)        :: source_ID
  integer                 :: node_ID
  real(kind=dp)           :: btime, looptime        ! breakthrough time, 
end type RejOrDel                                      ! looptime

type sdata
  integer                                    :: num_RN
  integer, allocatable, dimension(:)         :: total,ejected,delayed
  character(len=10),allocatable,dimension(:)  :: RN_name
  real(kind=dp),allocatable,dimension(:)     :: looptime                
end type  sdata

!***********************************************************************
contains
!***********************************************************************
subroutine run_diagnosis(asciiflag)
!***********************************************************************
type(RejOrDel), allocatable, dimension(:)       :: nodearray
type(sdata)                                  :: sum_data
integer                                      :: funit
logical :: asciiflag 

! get data from results.dat file

  call get_node_data(nodearray, sum_data,asciiflag)

  funit=findunit()
  open(funit,file="loop_diagnosis.rlt")
  call writeheader(funit)

  if(.not. allocated(nodearray))then
   write(funit, *) " <<< No Particles Delayed or Ejected >>"
   close(funit)
   return
  end if 

! simple summary of particles delayed by looping

  call summary_of_delayed_particles(nodearray,sum_data,funit)

  call source_summary(nodearray,funit)

! simple summary of ejected particles

  call summary_of_ejecting_nodes(nodearray,funit)

  close(funit)

  return
end subroutine run_diagnosis
!***********************************************************************
subroutine summary_of_ejecting_nodes(nodearray,funit)
!***********************************************************************
type(RejOrDel),dimension(:)                  :: nodearray
integer                                   :: funit

real(kind=dp),dimension(:,:), allocatable         :: tempnodelist    
real(kind=dp)                                     :: value,total_looptime                                 
integer                                           :: num_unique_RejOrDel
real(kind=dp)                                     :: rcount
integer                                           :: num_ejected
integer                                           :: i, j
real(kind=dp),dimension(:,:),allocatable          :: templist

! identify number of unique ejecting nodes 
  
       num_ejected=0
       do i=1,size(nodearray)
          if(nodearray(i)%Node_ID .lt. 0) then
           num_ejected=num_ejected + 1
          end if
       end do    

! if no particles are ejected exit

      if(num_ejected .eq. 0) then  

    write(funit,*) "<<< Loop SUMMARY >>>          " 
    write(funit,*) " Number of unique ejecting loops     :  0" 
    write(funit,*) " Total number of ejected particles   :  0"

    write(funit,*) "                               " 
    write(funit,*) "<<< LOOP DETAILS >>>           "
    write(funit,*) " NODE ID     NUMBER EJECTED            " 
    write(funit,*) " no ejected particles                  "
       return
      end if

   allocate(tempnodelist(num_ejected,2))
   
          j=0
      do i=1, size(nodearray)
          if(nodearray(i)%Node_ID .lt. 0) then
             j=j+1
             tempnodelist(j,1)= -1.0d0 *nodearray(i)%Node_ID
             tempnodelist(j,2)=nodearray(i)%looptime
          end if
      end do   

    
      call qsort_2d(tempnodelist,1)

        num_unique_RejOrDel=0
        value=0
     do i=1, num_ejected
      
          if(value .ne. tempnodelist(i,1))then
            value=tempnodelist(i,1)
            num_unique_RejOrDel=num_unique_RejOrDel+1
          end if
     end do

       allocate(templist(num_unique_RejOrDel,2))

        value         = tempnodelist(1,1)
        rcount        = 1.0d0
        j=0
     do i=2, num_ejected
          if(value .eq. tempnodelist(i,1))then
            rcount        = rcount + 1.0d0          
          else
            j=j+1
            templist(j,1)= value                  ! NODE ID
            templist(j,2)= -rcount                ! NUMBER EJECTED

            rcount        = 1.0d0
            value         = tempnodelist(i,1)

          end if
     end do

            templist(num_unique_RejOrDel,1)= value                  ! NODE ID
            templist(num_unique_RejOrDel,2)= -rcount                ! NUMBER EJECTED

     call   qsort_2d(templist,2)

            templist(:,2)=templist(:,2)* (-1.0)


    write(funit,*) "<<< Loop SUMMARY >>>          " 
    write(funit,*) " Number of unique ejecting loops     : ", num_unique_RejOrDel
    write(funit,*) " Total number of ejected particles   : ", num_ejected
    write(funit,*) "                                       "

 if(num_unique_RejOrDel .ge. 1) then
    write(funit,*) "<<< LOOP DETAILS >>>           "
    write(funit,*) " NODE ID     NUMBER EJECTED            " 

     do i=1,num_unique_RejOrDel
    write(funit,'(i8,2x,i10,15x)') int(templist(i,1)), int(templist(i,2))
     end do
 else

    write(funit,*) "        <<< NODE DETAILS >>>           "
    write(funit,*) " No Ejecting loops         " 


 end if


end subroutine summary_of_ejecting_nodes
!***********************************************************************
subroutine summary_of_delayed_particles(nodearray,sum_data,funit)
!***********************************************************************
type(RejOrDel),dimension(:)                 :: nodearray
type(sdata)                                 :: sum_data
integer                                     :: funit
integer :: nsurvive 

real(kind=dp)                               :: total_delay_time
real(kind=dp)                               :: total_trans_time
integer                                     :: part_delayed,i,j
real(kind=dp)                               :: RN,rcount 
real(kind=dp),dimension(:,:), allocatable   :: tempnodelist    


    write(funit,*) "<<< Delayed PARTICLES >>>              "
    write(funit,*) "<<< SUMMARY >>>                    " 
    write(funit,*) " Number of delayed particles           : ", sum(sum_data%delayed(:) ) 
    write(funit,*) " Number of ejected particles           : ", sum(sum_data%ejected(:) ) 
    write(funit,*) " Number of surviving pariticles        : ", sum(sum_data%total(:))- sum(sum_data%ejected(:)) 

 
    write(funit,*) "                                              " 
    write(funit,*) "<<< By Nuclide  >>>              "
    write(funit,*) " <RN>   <Number Delayed>  <Number Ejected> <Number Survived> <Average Delay Time> "             

    do i=1,size(sum_data%delayed) 
      nsurvive = sum_data%total(i)-sum_data%ejected(i)  
      write(funit,'(2x,A8,4x,i10,4x,i10,4x,i10,10x,e12.4)') & 
        sum_data%RN_name(i), sum_data%delayed(i), sum_data%ejected(i), & 
        nsurvive, sum_data%looptime(i)/max( float(nsurvive) , 1.0d0) 
    end do 
     

! identify delayed particles

       total_delay_time=0.d0
       part_delayed=0
       do i=1,size(nodearray)

          if(nodearray(i)%looptime .gt. 0.0d0) then
           part_delayed=part_delayed + 1
           total_delay_time=nodearray(i)%looptime+ total_delay_time
          end if
       end do    

   if(part_delayed .ge. 1) then

   allocate(tempnodelist(part_delayed,3))
   
          j=0
      do i=1, size(nodearray)
          if(nodearray(i)%looptime .gt. 0.0d0) Then
             j=j+1
             tempnodelist(j,1)= nodearray(i)%RN
             tempnodelist(j,2)= nodearray(i)%looptime
             tempnodelist(j,3)= nodearray(i)%btime
          end if
      end do   
    
      call qsort_2d(tempnodelist,1)


        RN               = tempnodelist(1,1)
        total_delay_time = 0.d0
        total_trans_time = 0.d0
        rcount=0.d0

     do i=1,part_delayed                      ! note tempnodelist has been ordered by RN 
          if(RN .eq. tempnodelist(i,1))then
            rcount           = rcount + 1.0d0          
            total_delay_time = tempnodelist(i,2) + total_delay_time
            total_trans_time = tempnodelist(i,3) + total_trans_time
          else
 
            RN               = tempnodelist(i,1)
            total_delay_time = tempnodelist(i,2) 
            total_trans_time = tempnodelist(i,3) 
            rcount           = 1.0d0          
          end if
     end do


    write(funit,*) "                                       " 
  else
    write(funit,*) "<<< Delayed PARTICLES >>>              "
    write(funit,*) "<<< SUMMARY >>>                    " 
    write(funit,*) " No Particles Delayed             "
    write(funit,*) "                                              " 
  end if



 return

end subroutine summary_of_delayed_particles
!***********************************************************************
subroutine source_summary(nodearray,funit)
!***********************************************************************
type(RejOrDel),dimension(:)                        :: nodearray
integer                                         :: funit

character(len=20),allocatable, dimension(:)     :: source_ID
integer,allocatable,dimension(:)                :: num_ejected
real(kind=dp),allocatable,dimension(:,:)        :: num_ejected2
integer                                         :: i,ii, j, num_sources


    allocate(source_ID(50000))
    allocate(num_ejected(50000))
    allocate(num_ejected2(50000,2))
    num_ejected=0
    source_ID="OXFORD"     ! random nonlikely string

! build list of sources

      do ii=1,size(nodearray)
        if(nodearray(ii)%node_ID .lt. 0 ) exit
      end do

! exit if no sources are identified

        if( ii .gt. size(nodearray)) then
    write(funit,*) "<<< Ejected Particles by SOURCE >>>        "
    write(funit,*) "<<< SUMMARY >>>                      " 
    write(funit,'(A45,2x,I8)') " No particles ejected by any sources " 
    write(funit,*) "                                           "
    write(funit,*) "<<< Ejected Particles by SOURCE >>>        "
    write(funit,*) "<<<  DETAILS >>>                    " 
    write(funit,*) "  Source ID         Number of ejected particles" 
    write(funit,*) " No ejected particles "
    write(funit,*) "                                           "
          return
        end if    

! else start searching for sources
      
      source_ID(1)=nodearray(ii)%source_ID

       do i=1,size(nodearray)

              if(nodearray(i)%node_ID .lt. 0) then

         do j=1, size(num_ejected) 
         
     !          if(index(source_ID(j),nodearray(i)%source_ID) .ne. 0)then
               if(index(adjustl(source_ID(j)),nodearray(i)%source_ID) .eq. 1  & 
                .and. len_trim(source_ID(j)) .eq.  len_trim(nodearray(i)%source_ID))then
                  num_ejected(j)=num_ejected(j) + 1
                  exit
              else 
                if( num_ejected(j) .eq. 0) then         
                  source_ID(j)=nodearray(i)%source_ID
                  num_ejected(j)=1
                  exit
                end if
              end if

         end do

            end if

       end do


! number of particles ejected per source

        num_sources=0
      do i=1, size(num_ejected)
         if ( num_ejected(i) .gt. 0)then
           num_sources=num_sources+1 
         end if
      end do

! evaluate index of the number of particles ejected per source, in a decending manner

    num_ejected2(:,1)=-1.0d0*num_ejected

   do i=1,size(num_ejected)
      num_ejected2(i,2)=i  
   end do

   call qsort_2d(num_ejected2,1)
   num_ejected2(:,1)=num_ejected2(:,1)*(-1.0d0)

! print out results

    write(funit,*) "<<< Ejected Particles by SOURCE >>>        "
    write(funit,*) "<<< SUMMARY >>>                      " 
    write(funit,'(A45,2x,I8)') " Number of sources with ejected particles : ", num_sources

    write(funit,*) "                                                  " 
    write(funit,*) "<<< Ejected Particles by SOURCE >>>        "
    write(funit,*) "<<<  DETAILS >>>                    " 
    write(funit,*) "  Source ID         Number of ejected particles" 
     do i=1,size(num_ejected)
       if( num_ejected(int(num_ejected2(i,2))) .gt. 0)then 
         write(funit,'(A20, 12x,i10)')  source_ID(int(num_ejected2(i,2))), num_ejected(int(num_ejected2(i,2)))
       end if
     end do
    write(funit,*) "                                     " 

    deallocate(source_ID)
    deallocate(num_ejected)
    deallocate(num_ejected2)

   return

end subroutine source_summary
!***********************************************************************
subroutine get_node_data(nodearray,sum_data,asciiflag)
!***********************************************************************
type(RejOrDel), allocatable, dimension(:)     :: nodearray
type(sdata)                                :: sum_data

integer                                    :: funit,i, counter
integer                                    :: RN, Node_ID 
character(len=10)                          :: source_ID
real(kind=dp)                              :: temp
real(kind=dp)                              :: btime,looptime
logical :: asciiflag 
!***********************************************************************

! first pass get summary of data sum_data and number of affected particles
  
   funit=openread("results.md")
   read(funit,fmt=*) 
   read(funit,fmt=*) sum_data%num_RN

   allocate(sum_data%total(sum_data%num_RN))
   allocate(sum_data%ejected(sum_data%num_RN))
   allocate(sum_data%delayed(sum_data%num_RN))
   allocate(sum_data%RN_name(sum_data%num_RN))
   allocate(sum_data%looptime(sum_data%num_RN))
   sum_data%looptime= 0.0d0
   sum_data%total= 0
   sum_data%ejected= 0
   sum_data%delayed= 0
    
   do i=1,sum_data%num_RN
      read(funit,fmt=*) sum_data%RN_name(i), temp 
   end do
   close(funit) 

   funit=findunit() 
   if(asciiflag) then 
     open(unit=funit, file='results.dat') 
   else 
     open(unit=funit, form='unformatted',file='results.dat') 
   end if 

   counter=0       ! counts the number of ejected or artifically retarded particles
99 continue   
   if(asciiflag) then 
    read(funit, fmt=*,end=101) btime, RN, temp, source_ID, Node_ID, looptime
   else 
    read(funit, end=101) btime, RN, temp, source_ID, Node_ID, looptime
   end if 

   sum_data%total(RN)= sum_data%total(RN) + 1
   if(Node_ID .ge.0 .and. looptime .gt. 0.0d0) & 
       sum_data%delayed(RN) = sum_data%delayed(RN) + 1 
   if(Node_ID .lt.0 ) &  
       sum_data%ejected(RN) = sum_data%ejected(RN) + 1 
   

   if( Node_ID .gt. 0) & 
    sum_data%looptime(RN)=sum_data%looptime(RN)+ looptime
   if(Node_ID .lt.0 .or. looptime .gt. 0.0d0) counter=counter+1          
goto 99
101 continue
 
close(funit)

! verify that some particles were rejected or delayed

        if(counter .eq. 0) then
         return
        end if


!  second pass collect information on all affected particles

      allocate(nodearray(counter))        ! place to store data

   funit=findunit() 
   if(asciiflag) then 
     open(unit=funit, file='results.dat') 
   else 
     open(unit=funit, form='unformatted',file='results.dat') 
   end if 

    counter=0     ! counts the number of ejected or artificially retarted particles
102 continue

    if(asciiflag) then 
    read(funit, fmt=*,end=103) btime, RN, temp, source_ID, Node_ID, looptime
    else 
    read(funit, end=103) btime, RN, temp, source_ID, Node_ID, looptime
    end if 
    if(Node_ID .lt.0 .or. looptime .gt. 0.0d0)then          
       counter=counter+1 
       nodearray(counter)%RN=RN
       nodearray(counter)%source_ID=source_ID
       nodearray(counter)%node_ID=Node_ID
       nodearray(counter)%btime=btime
       nodearray(counter)%looptime=looptime
    end if
goto 102          
103 continue
close(funit)

 return

end subroutine get_node_data
!***********************************************************************
!***********************************************************************
end module  loop_diagnosis
