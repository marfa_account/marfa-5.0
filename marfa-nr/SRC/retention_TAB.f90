
!Copyright 2011.  Los Alamos National Security, LLC.
!This material was produced under U.S. Government contract
!DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is
!operated by Los Alamos National Security, LLC for the U.S. Department
!of Energy.  The U.S. Government has rights to use, reproduce,
!and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS
!NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR
!ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is
!modified to produce derivative works, such modified software should be
!clearly marked, so as not to confuse it with the version available
!from LANL.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!Redistributions of source code must retain the above copyright notice,
!this list of conditions and the following disclaimer.

!Redistributions in binary form must reproduce the above copyright notice,
!this list of conditions and the following disclaimer in the documentation
!and/or other materials provided with the distribution.

!Neither the name Los Alamos National Security, nor the name
!Los Alamos National Laboratory, nor the names of its contributors may be
!used to endorse or promote products derived from this software without
!specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


! *********************************************************************
! File Name        : retention_TAB.f95
! Program Name     : marfa  
! Developed for    : SKB and POSIVA  
! Revision History : V3.2.4 initial  
!                  : V3.2.5 removed allocatable components from derived types  
!                  : 
!                  : 
!                  : 
! *********************************************************************


! module retention_TAB_class 
! This module is a general tabular  model  
! Three public routines and one public structure are provided.
! Public routines are: 
!    init_retention_TAB  a global initialization routine 
!    retention_TAB_   a constructor for the class 
!    sample_rt_TAB returns a sample from the retention time distribution 

module retention_TAB_class

  use precision 
  use io 
  use interpolation_class
  implicit none 

  private 
 
  public sample_rt_TAB, sample_rt2_TAB
  public init_retention_TAB
  public retention_TAB_ 
  public retention_TAB 

   type retention_TAB
     integer :: nstates 
     real(kind=dp)   :: Delta   ! thickness meters 
     character(len=10), dimension(:), pointer  :: statelabel
     real(kind=dp), dimension(:,:), pointer :: ka    ! dist coef for surface  sorption [1/L]
     real(kind=dp), dimension(:,:), pointer :: avgDm  ! Deff/R averaged [ T^2/Yr]  
     real(kind=dp), dimension(:), pointer :: dm  ! molecular diffusion  (not implemented) 
     type(interpolation2d),  dimension(:,:), pointer :: ptr2tab
   end type retention_TAB

   ! the following are for the transient flow mode 
   type(interpolation2d) :: fpntrt, frtrn

  contains 
!**********************************************************************
    subroutine init_retention_TAB(datadir) 
     character(len=80) :: datadir 
     character(len=132) :: filename
     integer :: il

     write(filename,'(2a)' ) trim(datadir),"LD_table1.dat"
     il = len_trim(filename)
     print *,'opening ', filename(1:il)
     fpntrt = interpolation2d_(filename(1:il))   ! penetration depth

     write(filename,'(2a)' ) trim(datadir),"LD_table2.dat" ! return time
     il = len_trim(filename)
     print *,'opening ', filename(1:il)
     frtrn = interpolation2d_(filename(1:il))


    end subroutine init_retention_TAB

!**********************************************************************
    subroutine retention_TAB_(aTAB, iunit, kdunit,nelem) 

     type(retention_TAB), pointer :: aTAB
     integer :: junit , kdunit, iunit 
     integer :: nstates , istate
     integer :: nelem, ielem , il 
     integer :: index1,index2 
     character(len=1) :: slash 
     character(len=3) :: junk
     character(len=132) :: datadir,filename   

     allocate(aTAB) 
     allocate(aTAB%dm(nelem)) 
     read(iunit, *) aTAB%Delta

     ! check for delimiter 
!     read(kdunit,*) delimiter  
!     if( trim(adjustl(delimiter)) .ne. 'kdbins' ) then 
!       print *, ' error in rocktypes.dat' 
!       print *, ' kdbins delimiter string not found' 
!     end if 

     read(kdunit,*) nstates
     aTAB%nstates=nstates
     if(nstates .lt. 1) then
       print *, 'problem in kdfile. nstates=' , nstates
       stop
     end if

     allocate(aTAB%ptr2tab(nelem, nstates) ) 
     allocate(aTAB%ka(nelem,nstates))
     allocate(aTAB%avgDm(nelem,nstates))
     allocate(aTAB%statelabel(nstates)) 
 
     do istate=1,nstates

        read(kdunit,'(A)') aTAB%statelabel(istate)
        read(kdunit,'(A)') datadir   

        ! are we on widows or unix/linux? 
        if( scan(datadir,'/') .gt. 0) then
         slash='/'    !unix
        else
         slash='\'    ! windows
        end if

        do ielem=1,nelem  
          write(filename, '(3A,i0)') trim(adjustl(datadir)),slash,"retdist.dat",ielem
          il = len_trim(filename) 
          aTAB%ptr2tab(ielem,istate) = interpolation2d_(filename(1:il)) 
        end do 

      do index2=1,nelem
         read(kdunit,*) aTAB%ka(index2,istate), aTAB%avgDm(index2,istate) 
      end do

      end do 
     

    
    end subroutine retention_TAB_ 

!*****************************************************************
    function sample_rt_TAB(aTAB,elem,beta,r,epoch)  result(rt) 
!*****************************************************************

      type(retention_TAB) :: aTAB 
      integer :: elem,i, epoch 
      real(kind=dp) :: rt , r ,beta , r1, beta1 
      type(interpolation2d), pointer :: ffunc 

      ffunc => aTAB%ptr2tab(elem,epoch)  

      ! make sure variables are in range 
   
      rt = interpolate(ffunc,beta, r) 


      rt = rt + aTAB%ka(elem,epoch)*beta   ! add time due to surface sorption  

    end function sample_rt_TAB
!*****************************************************************
    function sample_rt2_TAB(aTAB,elem,remt,r,istate,nxtstate)  result(rt) 

      type(retention_TAB) :: aTAB 
      integer :: elem, istate,nxtstate 
      real(kind=dp) :: rt , r , remt, r1 
      real(kind=dp) :: z, t0, norm_time

 
      ! normalize time
      t0 = aTAB%Delta**2/aTAB%avgDm( elem, istate) 

      norm_time=remt/t0

      ! sample a penetration depth given a normalized time and quantile(r)

      if(norm_time.gt.fpntrt%xmax)then !3.2.3
           call random_number(z)
      else
         z=interpolate(fpntrt,norm_time,r) !3.2.3
      end if

      ! sample a return time given a depth and a quantile

        call random_number(r1)
        rt=interpolate(frtrn,z,r1)  !3.2.3

        t0 = aTAB%Delta**2/aTAB%avgDm( elem, nxtstate) 
        rt = t0*rt  ! denormalize


    end function sample_rt2_TAB
!*****************************************************************

end module retention_TAB_class

