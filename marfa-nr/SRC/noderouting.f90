!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland). 
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

! *********************************************************************
! File Name        : noderouting.f90
! Program Name     : marfa
! Developed for    : SKB and POSIVA
! Revision History : version 3.3  December 2009 James Mancillas
! SRD Section      : Section 
! *********************************************************************

! module node_class
! This module is the handles the transport node network
!  public routines :
! init_node      :: reads node data files 
! advance_node   :: gets transport properties from next node
! activate_node  :: selects and readies first node in a trajectory 
! sample_node_rt :: returns sampled retention time for transport between nodes
! sample_node_rt2:: returns sampled retention time for transport between nodes with a flow change
! check_for_node_termination :: check if node is at sample surface 
! check_forsplit :: returns true if splitting surface is reached
! get_epoch 

module noderouting

use precision
use io
use particle_class
use hydrofacies
use retention_MD_class  ! matrix diffusion
use retention_ES_class  ! equilibrium sorption
use retention_LD_class  ! limited diffusion
use retention_TAB_class  ! tabular 

 
implicit none

private

public  init_node, advance_node, activate_node, query_node
public  sample_node_rt, sample_node_rt2
public  check_for_node_termination
public  epochbndr
public  numepoch 
public  check_forsplit
public  check_for_rescalc
public  get_epoch 
public  load_flowfield, free_flowfield 



type node
  real(kind=sp)                               :: x, y, z
  character(len=15) :: faciesID
  type(hf),pointer                            :: ptr_to_hf => null() 
  integer                                     :: visits, lastvisitor 
  real(kind=sp)                               :: time_FV   ! time of first visit
  integer, pointer, dimension(:) :: states ! 4amphos
  real(kind=sp), pointer, dimension (:) :: t_statechange !4amphos
  integer :: nstates 
end type node

type linkset
   integer :: nnbrs 
   real(kind=sp), allocatable, dimension(:)    :: prob, tau, beta, len
   integer, allocatable, dimension(:)          :: neighbor
end type linkset
   


!the following are for the flow fields and pointers to the flow fields 

type flowfield 
  type(linkset), dimension(:), allocatable ::  cgraph
  real(kind=dp) :: start,end 
  character(len=80) :: datalocation 
end type flowfield 

type ffptrarray 
  type(flowfield), pointer :: ptr
end type ffptrarray 

type(flowfield), allocatable, dimension(:),target :: ffarray 

type(ffptrarray), dimension(:), allocatable :: ffptrs  

type(node), dimension(:), allocatable,target :: nodearray 

integer                                       :: numff  
real(kind=dp), allocatable, dimension(:)      :: epoch
real(kind=dp)                                 :: epochbndr
integer                                       :: numepoch

! end flow field section

logical asciiflag 
 
integer, dimension(:), allocatable            :: initial_nodes
real(kind=dp)                                 :: channelfac  
integer                                       :: visits_allowed
real(kind=dp)                                 :: routing_threshold 

contains
!***********************************************************************
subroutine init_node()
  ! this subroutine reads input file files: for node data, 
  ! inital node data, and translation tables
!***********************************************************************
   integer                                    :: funit 
   integer                                    :: i
   character(len=80)                          :: datadir
   integer :: khf,nhf,maxstates  
!***********************************************

! Get beta multiplier
      call get_beta_multiplier("flowdata.dat") 

!4amphos 
      call get_routing_threshold("flowdata.dat") 
!4amphos 

! find out if node files are ascii or not 
      asciiflag = check_formatting("flowdata.dat") 


! Read node data files 
      call get_node_data("flowdata.dat") 

!4amphos 
!read state history for each node 
! get the chemistry state for each segment
     maxstates = 1
     nhf=size(hfset) 
     do khf = 1, nhf
       maxstates = max(maxstates, size( hfset(khf)%statelabel ))
     end do
     if( maxstates .eq. 1) then
       call static_kd()
       print *, 'static kds' 
     else
       call get_kd_history("kdhistory.dat")
       print *, 'success in getting kd history' 
     end if

!4amphos 

! Get number and time of flow epochs 
      call get_flow_periods("flowdata.dat")

!  Get number of repeat visits allowed 
      call get_revisits_allowed("flowdata.dat")

! Read translation data files
!      call get_translation_data("flowdata.dat")

!  This initializes the retention model
      call init_retention_models("rocktypes.dat")

! write a list of monitored nodes for resident concentration 
      call construct_monitored_list() 

   return

end subroutine init_node
!***********************************************************************
function node_(iunit) result(b)
!***********************************************************************
  type(node)                              :: b 
  integer                                 :: iunit
  integer                                 :: inode
  character(len=15) :: faciesID 
  real(kind=sp)                           :: x, y, z
!*******************************************

   if( asciiflag ) then 
   read(iunit, *) inode, x, y, z, faciesID
   else 
   read(iunit) inode, x, y, z, faciesID 
   end if 

   b%x=x 
   b%y=y
   b%z=z
   b%faciesID=faciesID 

   return
end function node_
!***********************************************************************
function linkset_(iunit,nlinks) result(b)
!***********************************************************************
  type(linkset)                           :: b 
  integer,intent(in)                      :: iunit,nlinks
  integer                                 :: index1, i, irun
  integer                                 :: nactlinks 
  real(kind=sp)                           :: sum , probx 
  integer, dimension(:), allocatable :: neighbor 
  real(kind=sp), allocatable, dimension(:) :: prob,tau,beta,len
!*******************************************


   b%nnbrs = nlinks 
   if(nlinks .ge. 1) then   
      allocate(neighbor(nlinks))
      if( asciiflag) then 
      read(iunit, *) (neighbor(i),i=1,nlinks)
      else 
      read(iunit) (neighbor(i),i=1,nlinks)
      end if 

      allocate(prob(nlinks))
      if( asciiflag) then 
      read(iunit,*) (prob(i), i=1,nlinks)
      else 
      read(iunit) (prob(i), i=1,nlinks)
      end if 
      probx=maxval(prob) 

      allocate(tau(nlinks))
      if( asciiflag) then 
      read(iunit,*) (tau(i), i=1,nlinks)
      else 
      read(iunit) (tau(i), i=1,nlinks)
      endif 

      allocate(len(nlinks))
      if( asciiflag) then 
      read(iunit,*) (len(i), i=1,nlinks)
      else 
      read(iunit) (len(i), i=1,nlinks)
      end if 

      allocate(beta(nlinks))
      if( asciiflag) then 
      read(iunit,*) (beta(i), i=1,nlinks)
      else 
      read(iunit) (beta(i), i=1,nlinks)
      endif 
    
      beta=beta*channelfac

!copy to b node, ignoring low prob links 
      ! count the active links  
      nactlinks=0 
      sum=0.
      do i=1,nlinks 
       if( prob(i)/probx .gt. routing_threshold) then 
         nactlinks=nactlinks+1 
         sum=sum+prob(i)
       endif 
      end do 

      if( nactlinks .eq. 0) then 
       print *, 'no downstream links' 
       print *, prob
       print *, neighbor
       print *, tau 
       stop 
      end if 
      allocate(b%len(nactlinks))
      allocate(b%tau(nactlinks))
      allocate(b%beta(nactlinks))
      allocate(b%prob(nactlinks)) 
      allocate(b%neighbor(nactlinks)) 

      b%nnbrs=nactlinks 

      irun=0 
      do i=1,nlinks 
       if( prob(i)/probx .gt. routing_threshold) then 
         irun=irun+1  
         b%prob(irun)=prob(i) 
         b%neighbor(irun)=neighbor(i) 
         b%len(irun)=len(i) 
         b%tau(irun)=tau(i) 
         b%beta(irun)=beta(i) 
       endif 
      end do
      
      do i=1,nactlinks 
        b%prob(i) = b%prob(i)/sum 
      end do 
      do i=2,nactlinks  
       b%prob(i)=b%prob(i-1)+b%prob(i) 
      end do 
      b%prob(nactlinks)=1.0d0 !avoids roundoff 

      deallocate(prob)
      deallocate(neighbor) 
      deallocate(len) 
      deallocate(tau)
      deallocate(beta) 

   end if

   return

end function linkset_
!
function get_epoch( time ) result(crnt_epoch) 
 integer :: i, crnt_epoch 
 real(kind=dp) :: time 
 do i=1,numepoch 
  if( time .lt. epoch(i)) exit 
 end do 
 crnt_epoch = i 
end function get_epoch 
!***********************************************************************
subroutine advance_node(apart,taubar,betabar, ell, disp) 
!***********************************************************************
   integer                      :: ipart 
   integer :: istate,nstates 
   real(kind=dp)                :: xpos,ds
   real(kind=dp)                :: taubar, betabar, disp,time
   real(kind=dp)                :: ell
   type(particle)               :: apart
   type(node)                   :: a 
   integer                      :: next ,ilink, i
   real(kind=dp)                :: r 
   type(hf), pointer            :: crnt_hf
   integer                      :: crnt_node, next_node
   integer                      :: crnt_epoch 
   type(node), pointer          :: anode 
   type(linkset), pointer          :: alinkset 
   type(flowfield), pointer :: ptr2ff 
!*********************************
! identify current epoch

  ! not sure if this is needed 
  crnt_epoch=apart%crnt_epoch  
  time=apart%time
  do i=1,numepoch 
     if(time .lt. epoch(i)) exit
  end do
  crnt_epoch=i
  apart%crnt_epoch=i 

  epochbndr=epoch(crnt_epoch)

  !4amphos 
  ptr2ff => ffptrs(crnt_epoch)%ptr
  anode=>nodearray(apart%nxtseg) 
  nstates=anode%nstates 
  do istate=1,nstates 
   if( time .lt. anode%t_statechange(istate))  exit 
  end do 
  istate=min(istate,nstates) 
  epochbndr = min( epochbndr, anode%t_statechange(istate) ) 


! if epoch changed, the node needs to be mapped to the new time domain
! the next node for apart is mapped to the next node array

!   if(crnt_epoch .gt. prev_epoch)then
!     call mapnodes(apart) 
!     if( apart%nxtseg .eq. 0) return  
!   end if

! check for wandering particle 
! exit if been here too many times 

  ipart=apart%ipart 

  if(ipart .eq. anode%lastvisitor) then
   anode%visits = anode%visits + 1                 
   apart%loop_time=(time-anode%time_FV)
   apart%inloop=.true.        ! now in a loop
  else
   anode%lastvisitor = ipart
   anode%visits = 1 
    anode%time_FV=time         ! on first visit set time of visit
    if(apart%inloop)then       ! if previously in loop then
      apart%inloop=.false.     ! change inloop status to no longer in loop
      apart%cum_time=apart%cum_time+apart%loop_time ! and update cum loop time
    end if

  end if
  if( anode%visits .gt. visits_allowed ) then 
    apart%nxtseg = -apart%nxtseg
    apart%cum_time=apart%cum_time+apart%loop_time   
    return 
  end if 
   
!************************
!   select to next node

   crnt_node = apart%nxtseg
   ptr2ff => ffptrs(crnt_epoch)%ptr
   alinkset=>ptr2ff%cgraph(crnt_node) 

   if(alinkset%nnbrs .ge. 1) then

     call random_number(r)  
     do ilink=1,alinkset%nnbrs 
       if( alinkset%prob(ilink) .ge. r ) exit  
     end do


   !**************************
     crnt_hf=>anode%ptr_to_hf
     disp    =  anode%ptr_to_hf%alpha        ! dispersivity
     betabar =  alinkset%beta(ilink)
     taubar  =  alinkset%tau(ilink)
     ell     =  alinkset%len(ilink)
     next_node = alinkset%neighbor(ilink) 
   !**************************************
   ! advance to next node

     apart%iseg=apart%nxtseg
     apart%nxtseg=next_node

   else
     ! no next segment, will be ejected in the main 
     apart%nxtseg=0
     apart%iseg=0
   end if

   return
end subroutine advance_node
!***********************************************************************
subroutine query_node(apart,posi)
  type(particle)        :: apart
  real(kind=dp), dimension (3) :: posi 
  type(node),pointer                        :: thisnode
  integer               :: crnt_epoch
  integer               :: iseg

  iseg = apart%iseg
  crnt_epoch=apart%crnt_epoch 
  thisnode=>nodearray(iseg)
  posi(1) = thisnode%x
  posi(2) = thisnode%y
  posi(3) = thisnode%z

  return 
  
end subroutine query_node
!***********************************************************************
subroutine activate_node(trjtag,apart)
  integer               :: trjtag
  type(particle)        :: apart
 
  apart%iseg=0
  apart%nxtseg= trjtag


  return

end subroutine activate_node
!***********************************************************************
function sample_node_rt(apart, beta, r) result(rt)

real(kind=dp)                             :: beta, r, rt
type(particle)                            :: apart
type(retention_MD),pointer                :: ptr_to_MD
type(retention_ES),pointer                :: ptr_to_ES
type(retention_LD),pointer                :: ptr_to_LD
type(retention_TAB),pointer               :: ptr_to_TAB
integer                                   :: istate, nstages 
integer                                   :: iseg, elem, nxtseg
integer                                   :: crnt_epoch  
integer :: nstates !4amphos 
real(kind=dp) :: time !4amphos 
type(node),pointer                        :: thisnode

iseg=apart%iseg  ! is really node number 
nxtseg=apart%nxtseg

elem=apart%elem
crnt_epoch=apart%crnt_epoch 

  thisnode=>nodearray(iseg)


  ptr_to_MD=>thisnode%ptr_to_hf%ptr_to_MD
  ptr_to_ES=>thisnode%ptr_to_hf%ptr_to_ES
  ptr_to_LD=>thisnode%ptr_to_hf%ptr_to_LD
  ptr_to_TAB=>thisnode%ptr_to_hf%ptr_to_TAB

  !4amphos
  time=apart%time
  nstates=thisnode%nstates
  do istate=1,nstates
    if( time .lt. thisnode%t_statechange(istate)) exit
  end do
  istate=min(1,nstates) 
  istate=thisnode%states(istate)

  if( associated(ptr_to_MD) )then
     rt=sample_rt_MD(ptr_to_MD,elem,beta,r,istate) 
  else if( associated(ptr_to_ES)) then
     rt=sample_rt_ES(ptr_to_ES,elem,beta,r,istate) 
  else if( associated(ptr_to_LD)) then
     rt=sample_rt_LD(ptr_to_LD,elem,beta,r,istate) 
  else if( associated(ptr_to_TAB)) then
     rt=sample_rt_TAB(ptr_to_TAB,elem,beta,r,istate) 
  else
    print*, " no associated retention model for node ", nxtseg
    stop
  end if

end function sample_node_rt
!***********************************************************************
function sample_node_rt2(apart, dt, r) result(rt)

real(kind=dp)                            :: rt, r, dt 
real(kind=dp)                            :: time , oldtime
type(particle)                           :: apart
type(retention_MD),pointer               :: ptr_to_MD
type(retention_ES),pointer               :: ptr_to_ES
type(retention_LD),pointer               :: ptr_to_LD
type(retention_TAB),pointer              :: ptr_to_TAB
integer                                  :: iseg, elem, nxtseg
integer                                  :: crnt_epoch  
integer :: i,istate,nstates  , nxtstate
type(node),pointer                       :: thisnode


! can be here for two reasons. 
! Either a flow change or a chemistry change
! must handle these differently 

  oldtime=apart%time
  time=oldtime + dt + 1.0d-3

  do i=1,numepoch 
     if(time .lt. epoch(i)) exit
  end do
  crnt_epoch=i
  apart%crnt_epoch = crnt_epoch  
  epochbndr=epoch(crnt_epoch) 


  iseg=apart%iseg
  elem=apart%elem

  thisnode=>nodearray(iseg)

  ptr_to_MD=>thisnode%ptr_to_hf%ptr_to_MD
  ptr_to_ES=>thisnode%ptr_to_hf%ptr_to_ES
  ptr_to_LD=>thisnode%ptr_to_hf%ptr_to_LD
  ptr_to_TAB=>thisnode%ptr_to_hf%ptr_to_TAB 

     nstates=thisnode%nstates
     do istate=1,nstates
      if( time .lt. thisnode%t_statechange(istate)) exit
     end do
     istate=min(istate,nstates) 
     nxtstate=thisnode%states(istate)

     time=oldtime 
     do istate=1,nstates
      if( time .lt. thisnode%t_statechange(istate)) exit
     end do
     istate=min(istate,nstates) 
     istate=thisnode%states(istate)



   if( associated(ptr_to_MD) )then
    rt = sample_rt2_MD(ptr_to_MD,elem,dt,r,istate,nxtstate) 
   else if( associated(ptr_to_ES)) then
    rt = 0.0
   else if( associated(ptr_to_LD)) then
    rt = sample_rt2_LD(ptr_to_LD,elem,dt,r,istate,nxtstate)
   else if( associated(ptr_to_TAB)) then
    rt = sample_rt2_TAB(ptr_to_TAB,elem,dt,r,istate,nxtstate) 
   else
    print*, " no associated retention model for node ", iseg 
    stop
   end if

   rt = rt + dt + 1.0d-3  

   ! update epochbndr (side effect, hacky) 
   time = oldtime  + rt 
  do i=1,numepoch 
     if(time .lt. epoch(i)) exit
  end do
  crnt_epoch=i
  apart%crnt_epoch = crnt_epoch  
  epochbndr=epoch(crnt_epoch) 
  do istate=1,nstates
   if( time .lt. thisnode%t_statechange(istate)) exit
  end do
  istate=min(istate,nstates) 
  epochbndr = min( epochbndr, thisnode%t_statechange(istate) ) 

end function sample_node_rt2

!************************************************************************
function check_formatting(filename) result(flag) 
character(len=*)                :: filename
integer                         :: iunit,i1
character(len=180)              :: aline
character(len=5),parameter     :: string2="ASCII" 
logical :: flag

iunit=openread(filename)


  flag=.false. 
! read line by line, exit upon finding flow change or at end of file
  do
    read(iunit,'(a80)', end=150) aline
    aline=adjustl(aline)
    i1 = index(aline,string2)
    if( i1 .eq. 1) flag = .true. 
  end do

150 continue 
  close(iunit)
 
  return

end function check_formatting 

!***********************************************************************
subroutine get_flow_periods(filename)
integer                         :: iunit,i,i1
character(len=180)              :: aline
character(len=12),parameter     :: string2="FLOW PERIODS" !3.3.1 
character(len=*)                :: filename
integer                         :: nm1
integer                         :: indx2ffarr  
character(len=80)               :: datalocation
iunit=openread(filename)

 nm1=0

! read line by line, exit upon finding flow change or at end of file
  do  
    read(iunit,'(a80)', end=150) aline
    aline=adjustl(aline)
    i1 = index(aline,string2)
    if( i1 .eq. 1 ) then 
      read(aline(i1+12:),*,end=150) nm1
      print*, " Number of flow periods: ", nm1
      exit
    end if
  end do


  150 continue !  end of file or numepoch

   if( nm1 .eq. 0) then 
    print *, 'must have at least 1 flow period' 
    stop 
   end if 

   numepoch=nm1  ! number of constant velocity periods

   
   allocate( epoch(numepoch))
   allocate( ffptrs(numepoch) )

   ! data stored at end of epoch
   do i=1,numepoch
      read(iunit,*) epoch(i), indx2ffarr
      if( indx2ffarr .gt. size( ffarray) ) then 
        print *, ' flow field ', indx2ffarr, ' not found'
        stop 
      else 
        ffptrs(i)%ptr => ffarray( indx2ffarr ) 
      end if 
   end do
   epoch(numepoch)=5.0e20  !maximum allowed simulation time

  close(iunit)

  return

end subroutine get_flow_periods
!***********************************************************************
subroutine get_revisits_allowed(filename)
integer                         :: iunit,i,i1
character(len=180)              :: aline
character(len=14),parameter     :: string2="VISITS ALLOWED"
character(len=*)                :: filename
character(len=80)               :: datalocation
logical                         :: found=.false.

iunit=openread(filename)
  do  
    read(iunit,'(a80)', end=150) aline
    aline=adjustl(aline) 
    i1 = index(aline,string2)

    if( i1 .eq. 1 ) then 
      found=.true.   
      read(aline(i1+14:),*,end=150) visits_allowed
      print*, " Number of visits allowed : ", visits_allowed
      exit
    end if
  end do

  150 continue !  end of file or numepoch

  if(.not.found )then
    print*, "string '",string2, "' not found in ", filename
    print*, " visits allowed has been set to 1"
    visits_allowed=1
  end if 

  if(visits_allowed .lt. 1)then
    print*, "Minimum number of node visits allowed is 1"
    print*, "visits allowed has been set to 1"
    visits_allowed=1
  end if 

close(iunit)
return
end subroutine get_revisits_allowed
!***********************************************************************
subroutine get_beta_multiplier(filename)
!**********************************************************************
integer                         :: iunit,i,i1
character(len=180)              :: aline
character(len=17),parameter     :: string2="CHANNELING FACTOR"
character(len=*)                :: filename
character(len=80)               :: buffer
logical                         :: found=.false.
!**********************************************************************

 iunit=openread(filename)
 channelfac=1.0d0                                  ! default

 do 
   read(iunit, '(a80)', end=9998) buffer
   buffer=adjustl(buffer) 
   if(index(buffer,string2) .eq. 1)then
    found=.true.
    exit
   end if
 end do
 
 9998 continue

  if(found)then
   read(buffer(18:),*) channelfac
   print*, 'Using channeling factor of ', channelfac
  end if

close(iunit)
return
end subroutine get_beta_multiplier   
!***********************************************************************
subroutine get_node_data(filename)
!***********************************************************************
integer                         :: iunit,iunit2,i,i1,j,ival,nlinks
character(len=120)              :: aline
character(len=14),parameter     :: string2="NODE DATA FILE" 
character(len=15),parameter     :: string3="FLOWFIELD FILES" 
character(len=*)                :: filename
integer                         :: nnodes , nn 
character(len=80)               :: datalocation
integer                         :: index1, nhf,khf
logical                         :: found=.false.
character(len=15), pointer :: ptr2ID

  nhf=size(hfset)


! open Flowdata.dat file

   iunit=openread(filename)

! find location of nodedata file 
      do  
        read(iunit,'(a)', end=145) aline
        aline=adjustl(aline) 
        i1 = index(aline,string2)  
        if( i1 .eq. 1 ) then 
          found=.true.
          exit
        end if
      end do

145 continue
  if(.not.found)then
    print*, "string '",string2, "' not found in ", filename
    stop
  end if 

  read(iunit,'(a80)') datalocation
  datalocation=adjustl(datalocation) 

! open nodedata file

       if( asciiflag ) then 
        iunit2=openread(trim(datalocation))
       else 
        iunit2=findunit() 
        open(unit=iunit2, form='unformatted', file=trim(datalocation)) 
       end if 
       print*, "opened ", trim(datalocation)

! allocate nodearray 
       if( asciiflag) then 
        read(iunit2,*) nnodes
       else 
        read(iunit2) nnodes 
       end if 

       print *, nnodes, ' nodes' 
       allocate(nodearray(nnodes), stat=ival)
       if( ival .eq. 0) then 
         print *, 'successfully allocated nodearray' 
       else 
         print *, 'unsuccessful at allocating nodearray' 
         stop 
       end if 

       ival = floor( real(nnodes)/10.)  
       do index1=1,nnodes
         if( mod(index1,ival) .eq. 0) & 
           print *, floor( real(index1)/real(ival) )*10., ' % read' 
         nodearray(index1)=node_(iunit2) ! change after restructuring file  
       end do 
      
       close(iunit2)
       print *, 'finished allocated nodedata ' 
    



! find location of string3 for flow fields 
      rewind(iunit)
      found=.false. 
      do  
        read(iunit,'(a)', end=155) aline
        aline=adjustl(aline) 
        i1 = index(aline,string3)  
        if( i1 .eq. 1 ) then 
          read(aline(i1+15:),*,end=155) numff 
          print *, ' getting ', numff, ' flow fields ' 
          found=.true.
          exit
        end if
      end do

155 continue
  if(.not.found)then
    print*, "string '",string3, "' not found in ", filename
    stop
  end if 

  allocate(ffarray(numff) ) 


! get the flow field locations (load when needed)   
     do j=1,numff 

! read filename of nodedata file
         read(iunit,'(a80)') datalocation
         datalocation=adjustl(datalocation) 
         ffarray(j)%datalocation = datalocation 
     end do 

  close(iunit)

! examine each node connection and set the hf pointer

    print *, 'finished reading nodes'
    print *, 'setting rocktype pointers' 
    ival = floor( real(nnodes)/10.)  
    do index1=1,size(nodearray)
      if( mod(index1,ival) .eq. 0) & 
           print *, floor( real(index1)/real(ival) )*10., ' % finished' 

      ptr2ID => nodearray(index1)%faciesID 

      do khf=1,nhf 
         if(trim(adjustl(ptr2ID)) .eq. trim(adjustl(hfset(khf)%faciesID))) exit  
      end do 

      if(khf .gt. nhf) then 
        print *, 'faciesID not found node ',index1, &
                         nodearray(index1)%faciesID 
        do khf=1,nhf 
          print *, hfset(khf)%faciesID 
        end do 
        stop 
      end if 

      nodearray(index1)%ptr_to_hf=> hfset(khf)
      nodearray(index1)%lastvisitor = 0 
      
    end do

  print *, 'finished setting nodearray'


  return

end subroutine get_node_data

!*********************************************************************** 

subroutine load_flowfield(iepoch) 

  integer :: j,numff,iunit,iepoch ,nn ,index1,ival 
  integer :: nlinks, nnodes 
  character(len=80) :: datalocation 
  type(flowfield), pointer :: ptr2ff 

       ptr2ff => ffptrs(iepoch)%ptr

! read filename of nodedata file
       datalocation = ptr2ff%datalocation

! open flowfield file

       if( asciiflag ) then
        iunit=openread(trim(datalocation))
       else
        iunit=findunit()
        open(unit=iunit, form='unformatted', file=trim(datalocation))
       end if
       print*, "opened ", trim(datalocation)

! allocate nodearray
       if( asciiflag) then
        read(iunit,*) nn
       else
        read(iunit) nn
       end if
       nnodes=size(nodearray) 
       if(nn .gt. nnodes) then
         print *, 'inconsistency between flowfield and nodedata files'
         stop
       end if

       allocate(ptr2ff%cgraph(nn))

! read data file and construct nodes

       ival = floor( real(nn)/10.)
       do index1=1,nn
         if( mod(index1,ival) .eq. 0) &
           print *, floor( real(index1)/real(ival) )*10., ' % read'
         if(asciiflag) then
           read(iunit,*) nlinks
         else
           read(iunit)  nlinks
         end if
         ptr2ff%cgraph(index1)=linkset_(iunit,nlinks)
       end do

       close(iunit)

end subroutine load_flowfield 
!*********************************************************
subroutine free_flowfield(iepoch) 
  integer :: iepoch 
  type(flowfield), pointer :: ptr2ff 


       ptr2ff => ffptrs(iepoch)%ptr
       deallocate(ptr2ff%cgraph) 

end subroutine free_flowfield 

!*********************************************************
subroutine init_retention_models(filename)
integer                         :: iunit,i,i1
character(len=180)              :: aline
character(len=*)                :: filename
character(len=80)               :: datadir

  iunit=openread(filename)

  !initialize the various retention models
   read(iunit,'(a80)')datadir
   i=len_trim(datadir)
   if( index(datadir,'/').gt. 0) then
     if(datadir(i:i) .ne. '/') datadir(i+1:i+1)='/'  ! unix
   else
     if(datadir(i:i) .ne. '\') datadir(i+1:i+1)='\'  ! windows
   endif

   call init_retention_MD(datadir)
   call init_retention_ES(datadir)
   call init_retention_LD(datadir)
   call init_retention_TAB(datadir)

   close(iunit)

end subroutine init_retention_models
!***********************************************************************
subroutine construct_monitored_list() 

integer :: funit,inode,nnodes,nmon 
type(node), pointer :: anode 

  nmon = 0 
  funit=findunit() 
  open(unit=funit,file="monitored_list") 
  nnodes = size(nodearray)   
  do inode=1,nnodes 
    anode=>nodearray(inode) 
    if( associated(anode%ptr_to_hf)) then 
    if( anode%ptr_to_hf%rescalc) then  
      write(unit=funit, fmt=*) "1 ",inode,anode%x,anode%y,anode%z 
      nmon = nmon + 1
    end if 
    end if 
  end do 
  if( nmon .eq. 0) then 
    close(unit=funit,status='delete')  
  else 
    close(funit) 
  end if 

end subroutine construct_monitored_list
!***********************************************************************
function check_for_rescalc(apart) result(monitored) 
type(particle) :: apart
type(node),pointer :: crntnode 
logical :: monitored
type(hf), pointer :: ptr_to_hf 
integer :: iseg,crnt_epoch

monitored = .false. 

iseg = apart%iseg 
crnt_epoch=apart%crnt_epoch  
crntnode => nodearray(iseg) 

ptr_to_hf => crntnode%ptr_to_hf 

monitored = ptr_to_hf%rescalc

end function check_for_rescalc 

!***********************************************************************
FUNCTION check_for_node_termination(apart) result(last)
!***********************************************************************
Logical                                 :: last 

type(particle)                          :: apart

     last =.false.

   if( apart%iseg .eq. apart%nxtseg)  last= .true. !terminal node

end FUNCTION check_for_node_termination
!***********************************************************************
function check_forsplit(apart) result(split)
! this function currently does nothing
! maintained infrastructor for segment system

  logical :: split
  type(particle) :: apart

  split =.false.

end function check_forsplit
!***********************************************************************
!**********************************************************************

!*********************************************************************
!*********************************************************************
! get kd history for each node in the flowfield 
! file must be structured the same as the flowfield file  
!*********************************************************************
subroutine get_kd_history(filename )
character(len=*)  :: filename
integer :: iunit 
integer :: nnodes, nnodes1 , inode, iff, numff  , jnode
integer :: i, j, idum 
character(len=10),dimension(:), allocatable :: cstates
type(node), pointer :: anode , bnode 

  nnodes = size(nodearray) 
  
  iunit=openread("kdhistory.dat") 
  read(iunit,*) nnodes1 
  if( nnodes .ne. nnodes1) then
    print *, ' stopping. kdhistory and nodenetwork files are not consistent'
    print *,  nnodes, nnodes1 
    stop
  end if


  

do inode=1,nnodes 

      anode => nodearray(inode) 

      read(iunit,*) idum 
      if( idum .ne. inode) then 
        print *, 'error in kdhistory file: expecting nodes to be in order' 
        stop 
      end if 
      read(iunit,*) anode%nstates
      allocate( anode%states(anode%nstates) )
      allocate( cstates(anode%nstates) )
      read(iunit,*) cstates
      ! convert from characters to integer
      do i=1,anode%nstates
        do j=1,size( anode%ptr_to_hf%statelabel )
         if( cstates(i) .eq. anode%ptr_to_hf%statelabel(j) ) exit
        end do
        if( j .gt. size( anode%ptr_to_hf%statelabel )) then
          print *, 'error in kdhistory string matching'
          print *, cstates
          print *, anode%ptr_to_hf%statelabel
          stop
         end if
        anode%states(i)= j
      end do
      deallocate(cstates)

      allocate( anode%t_statechange(anode%nstates) )
      read(iunit,*) anode%t_statechange(1:anode%nstates-1)
      anode%t_statechange(anode%nstates)=5.0d20 
    end do


  close(iunit)


end subroutine get_kd_history
!************************************************************************

subroutine static_kd()
integer :: inode,nnodes ,iff
type(node), pointer :: anode 

  nnodes = size(nodearray) 
  do inode=1,nnodes 
      anode => nodearray(inode) 
      anode%nstates = 1 
      allocate( anode%states(anode%nstates) )
      anode%states(1)=1 
      allocate( anode%t_statechange(anode%nstates) )
      anode%t_statechange(anode%nstates)=5.0d20 
  end do 

end subroutine static_kd

!***********************************************************************
subroutine get_routing_threshold(filename) 
integer                         :: iunit,i,i1
character(len=180)              :: aline
character(len=17),parameter     :: string2="ROUTING THRESHOLD" 
character(len=*)                :: filename
character(len=80)               :: datalocation
logical                         :: found=.false.

iunit=openread(filename)
  do
    read(iunit,'(a80)', end=150) aline
    aline=adjustl(aline)
    i1 = index(aline,string2)

    if( i1 .eq. 1 ) then
      found=.true.
      read(aline(i1+17:),*,end=150) routing_threshold 
      print*, " Threshold for routing to downstream neighbor : ", routing_threshold
      exit
    end if
  end do

  150 continue !  end of file or numepoch

  if(.not.found )then
    print*, "string '",string2, "' not found in ", filename
    print*, " Routing threshold set to 0.2"
    routing_threshold=0.2  
  end if

  if(routing_threshold .gt. 1.0d0 .or. routing_threshold .le. 0.0d0)then
    print*, "invalid routing threshold"
    print*, "resettig to 0.2"
    routing_threshold=0.2  
  end if

close(iunit)
return
end subroutine get_routing_threshold 


end module noderouting
