!Copyright 2011.  Los Alamos National Security, LLC.
!This material was produced under U.S. Government contract
!DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is
!operated by Los Alamos National Security, LLC for the U.S. Department
!of Energy.  The U.S. Government has rights to use, reproduce,
!and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS
!NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR
!ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is
!modified to produce derivative works, such modified software should be
!clearly marked, so as not to confuse it with the version available
!from LANL.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!Redistributions of source code must retain the above copyright notice,
!this list of conditions and the following disclaimer.

!Redistributions in binary form must reproduce the above copyright notice,
!this list of conditions and the following disclaimer in the documentation
!and/or other materials provided with the distribution.

!Neither the name Los Alamos National Security, nor the name
!Los Alamos National Laboratory, nor the names of its contributors may be
!used to endorse or promote products derived from this software without
!specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

!***********************************************************************
!  File Name:         interpolation_class.f95
!  Purpose:           contains interpolation classes
!  Written by:        S. Painter
!  History:           original code, 
!  Contents of File:  module interpolation1d_class
!                     module interpolation2d_class
!***********************************************************************
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------

!***********************************************************************
!  Module Name:         interpolation_class
!  Purpose:             
!  Date:                
!  Written by:          
!  History:             
!  Contents of Module:  type       interpolation1d
!                       function   interpolate1d
!                       function   interpolationld_
!                       subroutine free_interpolation1d
!  Contents of Module:  type       interpolation2d
!                       function   interpolate2d
!                       function   interpolation2d_
!                       subroutine free_interpolation2d 
!***********************************************************************
module interpolation_class

   use precision  
   use io 

   implicit none 
 
   private 

   type interpolation1d 
      integer :: n 
      real(kind=dp),pointer,dimension(:)   :: col1    ! pointer to array values
      real(kind=dp),pointer,dimension(:)   :: col2     ! pointer to array values
      real(kind=dp)                       :: min1        ! min value for column 1
      real(kind=dp)                       :: max1        ! max value for column 1
      real(kind=dp)                       :: min2        ! min value for column 2
      real(kind=dp)                       :: max2        ! max value for column 2
   end type interpolation1d 

   type interpolation2d 
      integer  :: n 
      type(interpolation1d),pointer, dimension(:) :: ptr2interp1d 
      real(kind=dp),pointer,dimension(:)  :: xarray        ! pointer to 1-d array
      real(kind=dp)                        :: xmin           ! minimum array1 value
      real(kind=dp)                        :: xmax           ! maximum array1 value
   end type interpolation2d 

   interface interpolate 
      module procedure interpolate1d
      module procedure interpolate2d
   end interface interpolate 

   interface interpolation1d_ 
      module procedure interpolation1d_  
      module procedure interpolation1d__ 
   end interface interpolation1d_  

   public interpolation1d, interpolation1d_, interpolation2d, interpolation2d_, interpolate 
   public free_interpolation1d, free_interpolation2d 

   contains

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
   function interpolation1d_(fn) result(aobj) 

!-----------------------------------------------------------------------
!   Name:     interpolation1d 
!
!   Purpose:  Constructor for 1d interpolation object. 
!             Read a two-column table from a file and assign values
!             to arrays.
!
!-----------------------------------------------------------------------

      integer :: len                         ! length of character string
      integer :: num                       ! number of lines of data
      integer :: n 

!input/output variables:

      character(len=*),optional :: fn        ! name of file to read

      type (interpolation1d)    :: aobj      ! structure to hold data


!local variables:

      integer   :: alloc_stat                    ! allocation status
      integer   :: funit                       ! unit number for opening file
      integer   :: read_ios                     

      character(len=80) :: aline                 ! input line buffer


!  if fn was passed in (present) - read file, store values in arrays
      if (.not. present(fn)) then
         print *, 'ERROR:  No filename given.'
         STOP
      end if

!  open file for reading 
      funit = openread(fn) 

!  skip header
         aline(1:1) = '!'
         do while (aline(1:1) .eq.'!')                   ! header line
               read(funit, *, iostat=read_ios) aline
         end do

! backspace and then use module procedure to read from open file 
         backspace(funit) 
         aobj = interpolation1d__(funit) 

         close(funit)                       ! close file 
      
         return 

   end function interpolation1d_
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
   function interpolation1d__(funit) result(aobj)

!-----------------------------------------------------------------------
!   Name:     interpolation1d
!
!   Purpose:  Constructor for 1d interpolation object.
!             Read a two-column table and assign values
!             to arrays. 
!             This version reads from a file already open. Not public.  
!
!-----------------------------------------------------------------------


      type (interpolation1d)    :: aobj      ! structure to hold data

      integer :: funit  
      integer   :: i, j, n 
      integer   :: read_ios


!  get number of data lines 
         read(funit, *) n
         aobj%n = n 
 
!  allocate 
         allocate( aobj%col1(n) ) 
         allocate( aobj%col2(n) ) 

!  read in data lines and store values in arrays
         do i=1,n  
            read(funit, *, iostat=read_ios) aobj%col1(i), aobj%col2(i) 
         end do

!  set  minimum & maximum values 
         aobj%min1=aobj%col1(1) 
         aobj%max1=aobj%col1(n) 
         aobj%min2=aobj%col2(1) 
         aobj%max2=aobj%col2(n) 

         return

   end function interpolation1d__ 


!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
    subroutine free_interpolation1d(aobj)
    !    destructor

      type(interpolation1d) :: aobj     ! structure to hold data
      integer :: de_stat  ! status flag

      deallocate(aobj%col1,stat=de_stat)
      deallocate(aobj%col2,stat=de_stat)

    end subroutine free_interpolation1d  


  
!-----------------------------------------------------------------------
!   Name:     interpolate1d
!
!   Purpose:  1-D linear interpolation
!
!-----------------------------------------------------------------------
   function interpolate1d(aobj,x) result(z) 

      use precision
 
! input/output arguments:
      real(kind=dp),intent(in)         :: x        ! x coordinate
      real(kind=dp)                    :: z        ! z coordinate to find
      type(interpolation1d),intent(in) :: aobj     ! lookup table

! local variables:

      integer     :: i                    ! loop control variable
      integer     :: ihigh                ! index of xhigh

      logical     :: found 

      real(dp)    :: deltax               ! difference in x values
      real(dp)    :: deltaz               ! difference in z values
      real(dp)    :: xhigh                ! col 1 value just greater than x
      real(dp)    :: xlow                 ! col 1 value just less than x
      real(dp)    :: xratio               ! ratio of difference xhigh - x to deltax
      real(dp)    :: zhigh                ! higher z value

! start here - initialize
      found = .false.
      xlow  = aobj%min1
      xhigh = aobj%max1
      z     = 0

      if (x .le. aobj%min1) then
         z = aobj%min2
      else if (x .ge. aobj%max1) then
         z = aobj%max2
      else ! calculate z based on x: linear interpolation
         i = 2                                       ! i=1 already taken care of above
         do while ((i .le. size(aobj%col1)) .and. (.not. found))
            if (x .lt. aobj%col1(i)) then
               ihigh = i                             ! index of next higher x value
               xhigh = aobj%col1(i)                  ! higher x value
               xlow  = aobj%col1(i-1)                ! lower x value
               found = .true.
            end if
            i = i + 1                                ! loop counter
         end do
         if( ihigh .gt. size(aobj%col1) .or. ihigh .lt. 1) then 
            print *, 'error in interpolate1d' 
            print *, ihigh, aobj%min1,x,aobj%max1 
            stop 
         end if  
         zhigh  = aobj%col2(ihigh)
         deltax = xhigh - xlow
         deltaz = aobj%col2(ihigh) - aobj%col2(ihigh-1)
         xratio = (xhigh - x) / deltax
         z      = zhigh - (xratio * deltaz)

      end if

   end function interpolate1d 

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
   function interpolation2d_(fn) result(aobj) 

!-----------------------------------------------------------------------
!  
!
!   Purpose:  Constructor for interpolation2d object 
!             Reads data from file, allocates components of object 
!
!             File should be formatted as follows:
!             Optional header lines where 1st character must be '!'
!             integer number of values in xarray nx 
!
!   Method: series of 1-D interpolations 
!
!-----------------------------------------------------------------------

!     input/output variables:
      character(len=*)      :: fn 
      type(interpolation2d) :: aobj 


      integer   :: alloc_stat                    ! allocation status
      integer   :: funit                         ! unit number for opening file
      integer   :: i                             ! loop counter
      integer   :: j                             ! loop counter
      integer   :: n 
      integer   :: open_ios                      ! i/o status for open command
      integer   :: read_ios                      ! i/o status for read command

      character(len=80) :: aline                 ! input line buffer
      character         :: marker                ! one-character label signifying comment
                                                 !  or used as delimiter in file

!  open file
      funit=openread(fn) 
    

      aline(1:1) = '!'
      do while (aline(1:1) .eq.'!')
        read(funit, *, iostat=read_ios) aline
      end do

!  get number of values in array1
      read(aline, *) n
      aobj%n=n

!  allocate arrays   
           allocate(aobj%ptr2interp1d(n),stat=alloc_stat)
           allocate(aobj%xarray(n),stat=alloc_stat)
 
           do i=1,n 
             read(funit, *) aobj%xarray(i)  
             aobj%ptr2interp1d(i) = interpolation1d__(funit) 
           end do 
           aobj%xmin=aobj%xarray(1) 
           aobj%xmax=aobj%xarray(n) 

           close(funit)

   end function interpolation2d_ 

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
   subroutine free_interpolation2d(aobj)
!   destructor

      type(interpolation2d)  :: aobj      ! structure to hold data
      integer :: de_stat   ! deallocation status
      integer :: i 
      !   deallocate space

         deallocate(aobj%xarray, stat=de_stat) 
         do i=1,aobj%n 
           call free_interpolation1d(aobj%ptr2interp1d(i)) 
         end do 

   end subroutine free_interpolation2d

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
    function interpolate2d(aobj,x,y) result(z) 

       use precision

       real(kind=dp) :: x
       real(kind=dp) :: y 
       real(kind=dp) :: z
       real(kind=dp) :: z1, z2, x1, x2  

       integer :: n  , j, j1, j2 

       type(interpolation2d) :: aobj 
 
       n=aobj%n 

       if( x .le. aobj%xmin) then 
         z=interpolate1d( aobj%ptr2interp1d(1), y)  
         return 
       end if 

       if( x .ge. aobj%xmax) then 
         z=interpolate1d( aobj%ptr2interp1d(n), y)  
         return 
       end if 

       j=1 
       do while(j .lt. aobj%n .and. aobj%xarray(j) .le. x)
        j=j+1  
       end do
       j1=j-1
       j2=j   

! interpolate: 
    z1=interpolate1d( aobj%ptr2interp1d(j1), y)  
    z2=interpolate1d( aobj%ptr2interp1d(j2), y)  
    x1=aobj%xarray(j1) 
    x2=aobj%xarray(j2) 
    z = z1 + (x-x1)/(x2-x1)*(z2-z1)  

  
    end function interpolate2d 

end module interpolation_class
!***********************************************************************

!-----------------------------------------------------------------------
!  end of file
!-----------------------------------------------------------------------
