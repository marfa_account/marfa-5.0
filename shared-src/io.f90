!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland). 
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

! *********************************************************************
! File Name        : io.f95  
! Program Name     : marfa  
! Developed for    : SKB and POSIVA  
! Revision History : Version 3.1 December 2006 James Mancillas 
!                  : Version 3.1.1 September 2007 Scott Painter  
!                      minor bug fix in write statements  
!                  : Version 3.2.2 update dates 
!                  : Version 3.2.3 
! SRD Section      : N/A  
! *********************************************************************

! module io
! utility module for opening and closing a file  

module io  

use precision

interface writeheader  
   module procedure writeheader
end interface writeheader  

private 

public  findunit   , openread , skipheader ,writeheader 

integer, parameter :: maxunit = 99 


contains 
!**********************************************************************
 function findunit() result(iunit) 
!**********************************************************************

  integer :: iunit 
  logical :: op 
 
  do iunit=1,maxunit 
    inquire(unit=iunit, opened = op) 
    if( .not. op )return 
  end do 

  print *, ' all file unit numbers used' 
  stop 


 end function findunit 

!**********************************************************************
 function openread(filename) result(iunit) 
!**********************************************************************

   ! finds an unused unit number and 
   ! opens an existing file 
   ! if successful 
   !  positions file after end of comment block 
   !    and returns the unit number 
   ! else 
   !  prints an errors and stops 

   character(len=*) :: filename 
   integer :: iunit,ios
   logical :: ex , op 
 
   inquire(file=filename, exist=ex, opened = op) 

   if( .not.ex ) then 
     print *, 'error: file does not exist ', filename 
     stop 
   end if 

   if(op) then 
     print *, 'error: file already opened ', filename 
     stop 
   end if 

   iunit = findunit( ) 

   open( unit=iunit, file=filename, iostat=ios, status='old')  
   if( ios .ne. 0) then 
     print *, 'error opening ', filename 
   end if 

   ! skip over the header lines 
   call skipheader(iunit) 

   return 

 end function openread 

!**********************************************************************
 subroutine skipheader(iunit) 
!**********************************************************************

   integer :: iunit
   integer :: str_len 
   character(len=132) :: filename 
   character(len=80) :: aline 
   character(len=80) :: foo 
   
  
   do 
     read(iunit,fmt='(a80)',end=120) aline 
     foo=adjustl(aline) 
     str_len=len_trim(aline)
     if( foo(1:1) .ne. '!' .and. str_len .ge. 1 ) exit  
   end do 
   backspace(unit=iunit) 
   return 

  

!120 inquire(iunit,name=filename) 
!    print *, 'end of file ', filename 
!
!   stop 
 120 continue
    return
 end subroutine skipheader 

!**********************************************************************
 subroutine writeheader(funit, npart, optstring)
!**********************************************************************
  IMPLICIT NONE

  INTEGER, intent(in)                      :: funit
  integer(kind=npk), intent(in), optional  :: npart  
  character(len=*), intent(in), optional   :: optstring 

  CHARACTER(len=8)                    :: Date
  CHARACTER(len=10)                   :: clocktime
  CHARACTER(len=132)                  :: file_name
  LOGICAL                             :: ex
  

  ex=.false.
  call date_and_time(date,clocktime)

  inquire(unit=funit,name=file_name,exist=ex)

! v3.1.1 sp fixed nonstandard format for write statements below 
!  statements now adhere to fortran 95 standard  
! v3.2 updated version number in title 
!sp V3.2.1 updated version number in title 
!sp V3.2.2 updated version number in title 
!sp V3.2.3 updated version number in title 
  if(.not.ex) then
       print*, 'writeheader funit not assigned properly'
       stop
  else
       write(funit,'(A,A)') '!',trim(file_name)
       write(funit,'(A,A,A,A)') '!file created on: ', date,' at ',clocktime
       write(funit,'(A)') '!MARFA Version 5.0.3'  
       write(funit,'(A)') '!Migration Analysis of Radionuclides in the FAr field'
       write(funit,'(A)') '!Developed by Scott Painter and James Mancillas' 
       write(funit,'(A)') '!Center for Nuclear Waste Regulatory Analyses' 
       write(funit,'(A)') '!Southwest Research Institute' 
       write(funit,'(A)') '!and Los Alamos National Laboratory' 
       write(funit,'(A)') & 
        '!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)'  
       if(present(npart)) then 
         write(funit,'(A,i0)') '!npart= ', npart 
       end if 
       if(present(optstring)) then 
         write(funit,'(A,A)') '!aseed= ', optstring 
       end if 
  end if

  return
 end subroutine writeheader
!**********************************************************************
end module io  
!**********************************************************************
!EOF
