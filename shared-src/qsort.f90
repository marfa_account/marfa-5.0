!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland). 
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

!*********************************************************************
! Recursive Fortran 95 quicksort routine
! sorts from smallest to largest
! this is a generic, non optimized, quick sort algorithm
! Reference :: 'Data Structures and Algorithm Analysis in C' Mark Allen Weiss 
!               Addison Wesley press
! input  :: A single column array of real(kind=dp)
! output :: A single column array of sorted data writen over the input array
!
! James Mancillas  1-27-06
!**********************************************************************

module qsort_module

!**********************************************************************
use precision
implicit  none

public    qsort_1d
public    qsort_2d

!**********************************************************************
contains

!**********************************************************************
recursive subroutine qsort_1d(array)
   real(kind=dp), intent(inout), dimension(:) :: Array
   real(kind=dp) :: temp, value
   integer   :: left, pivot, right,  A_size

! initialize search parameters 

   A_size=size(Array)            
   left  = 1
   right = A_size
   pivot = int(A_size/2)+1

   value=Array(pivot)

! begin ordering of values with respect to the pivot value 

   do while (right>left)

        do while(value>Array(left))   ! search left to right towards the pivot 
           left=left+1     
        end do

        do while(value<Array(right))  ! search right to left towards the pivot
           right=right-1
        end do


       if(left<=right)then            ! swapping array values 
         if(left /= right) then
           temp=Array(right)
           Array(right)=Array(left)
           Array(left)=temp
         end if
         left=left+1
         right=right-1
       end if

   end do

! end ordering of values with respect to the pivot value 

! partition the array arround the position of the pivot (it may not be in its inital position)

   if(right>1) call qsort_1d(Array( :right))      ! Leftmost portion of the array 
                                                  ! at this time 'right'= 'pivot'-1 ??
    
   if(left<A_size) call qsort_1d(Array(left: ))   ! Rightmost portion of the array 
                                                  ! at this time 'left' = 'pivot'+ 1 ??

end subroutine qsort_1d
!**********************************************************************
recursive subroutine qsort_2d(array,sort_column)
   real(kind=dp), intent(inout), dimension(:,:) :: Array
   integer, intent(in)                          :: sort_column
   real(kind=dp), allocatable, dimension(:,:)   :: temp

   real(kind=dp) ::  value
   integer   :: left, pivot, right,  A_size_1, A_size_2


! initialize search parameters

   A_size_1 = size(Array(:,1))
   A_size_2 = size(Array(1,:))
   left  = 1
   right = A_size_1
   pivot = int(A_size_1/2)+1


   value=Array(pivot,sort_column)
   allocate(temp(1,A_size_2))

! begin ordering of values with respect to the pivot value

   do while (right>left)

        do while(value>Array(left,sort_column))   ! search left to right towards the pivot
           left=left+1
        end do

        do while(value<Array(right,sort_column))  ! search right to left towards the pivot
           right=right-1
        end do


       if(left<=right)then            ! swapping array values
         if(left /= right) then

           temp(1,:)=Array(right,:)
           Array(right,:)=Array(left,:)
           Array(left,:)=temp(1,:)
         end if
         left=left+1
         right=right-1
       end if

   end do

! end ordering of values with respect to the pivot value

! partition the array arround the position of the pivot (it may not be in its inital position)

   if(right>1) call qsort_2d(Array( :right,:),sort_column)        ! Leftmost portion of the array
                                                                  ! at this time 'right'= 'pivot'-1 ??

   if(left<A_size_1) call qsort_2d(Array(left:,:),sort_column)    ! Rightmost portion of the array
                                                                  ! at this time 'left' = 'pivot'+ 1 ??
   deallocate(temp)

end subroutine qsort_2d


!**********************************************************************
end module qsort_module

!**********************************************************************
! end of file
