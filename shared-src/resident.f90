module calculate_resident 

use precision
use io 

implicit none 

private 

public calc_and_print_cres 

contains 

subroutine calc_and_print_cres(nnuc) 

integer :: funit,outunit 
integer :: nnuc ,inuc ,imon  , nmon 
integer :: iseg, itraj, ios ,n ,i 
integer :: itt,ntt 
integer :: ier 
logical :: file_exist 
character(len=1) :: assignhow 
character(len=132) :: buffer 
character(len=132) :: key,keyword 
real(kind=dp) :: timein, timeout, weightm, weightim, frac
real(kind=dp) :: stime,etime,dt 
real(kind=dp), dimension(:), allocatable :: tt 
real(kind=dp), dimension(:,:), allocatable :: cm,cim
integer, dimension(:,:), allocatable :: mon 
real(kind=dp), dimension(:,:), allocatable :: posi 

 ! check to see if we need to do anything 
 inquire(file="monitored_list",exist=file_exist)
 if( .not.file_exist) return 

 ! construct the list of monitored nodes 
 funit=findunit() 
 open(unit=funit, file="monitored_list") 

 ! first count 
 imon=0 
 do 
   read(unit=funit,fmt=*, iostat=ios) iseg 
   if(ios .ne. 0) exit 
   imon = imon + 1 
 end do 
 nmon = imon 
 allocate( mon(2,nmon)) 
 allocate( posi(3,nmon) )  
 close(funit) 
 open(unit=funit, file="monitored_list") 
 do imon=1,nmon 
  read(funit,*) mon(:,imon),posi(:,imon) 
 end do 
 close(unit=funit, status="delete") 
 

! get the conc output times 
inquire(file="rc_control.dat",exist=file_exist)
if( .not.file_exist) return 
funit=findunit() 
open(unit=funit,file="rc_control.dat") 
read(funit,*) n 
do i=1,n 
 read(funit,*) buffer 
end do 

read(funit,*) assignhow 
if(assignhow .eq. 'u' .or. assignhow .eq. 'U') then 
 read(funit,*) ntt 
 read(funit,*) stime, etime 
 dt=(etime-stime)/(ntt-1) 
 allocate(tt(ntt)) 
 do itt=1,ntt 
  tt(itt) = stime+(itt-1)*dt 
 end do 
else if(assignhow .eq. 'm' .or. assignhow .eq. 'M') then 
 read(funit,*) ntt 
 allocate(tt(ntt)) 
 do itt=1,ntt 
   read(funit,*) tt(itt) 
 end do 
else if(assignhow .eq. 'l' .or. assignhow .eq. 'L') then 
 read(funit,*) ntt 
 read(funit,*) stime, etime 
 stime=log(stime) 
 etime=log(etime) 
 dt=(etime-stime)/(ntt-1) 
 allocate(tt(ntt)) 
 do itt=1,ntt 
  tt(itt) = exp(stime+(itt-1)*dt) 
 end do 
else 
 print *, 'error in the file rc_control.dat '
 stop 
end if 
close(funit) 

allocate(cm(nnuc,ntt)) 
allocate(cim(nnuc,ntt)) 

outunit=findunit() 
open(unit=outunit, file="resident_mass.dat") 
call writeheader(outunit, int(-1, npk)) 


 
 funit=findunit() 
 do imon=1,nmon 
  open(unit=funit, form='unformatted', file="concresults.dat") 
  cm=0.0d0 
  cim=0.0d0 
  do 
   read(funit,iostat=ier,end=150) inuc, itraj,iseg, timein, timeout, weightm,weightim , frac 
   if( ier .ne. 0) exit 
   if( iseg .ne. mon(2,imon) .or. itraj .ne. mon(1,imon) ) cycle 
   do itt=1,ntt 
     if( tt(itt) .ge. timein .and. tt(itt) .lt. timeout ) then 
       cm(inuc,itt) = cm(inuc,itt) + weightm
       cim(inuc,itt) = cim(inuc,itt) + weightim
     end if 
   end do 
  end do 
150 close(funit) 

  ! now write 
  write(outunit,202) mon(:,imon),' position = ', posi(:,imon) 
202 format(2I9,a10,3E15.5) 
  do inuc=1,nnuc 
   do itt=1,ntt 
    write(outunit,201), inuc, tt(itt), cm(inuc,itt) , cim(inuc,itt) 
   end do 
  end do 
201 format(i10,3E15.5) 

 end do !end imon loop 
 close(outunit) 

end subroutine calc_and_print_cres  

end module calculate_resident 

