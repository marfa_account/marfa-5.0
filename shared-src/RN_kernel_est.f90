!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland). 
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

!***********************************************************************
! Density Estimation using a simple kernel, nearest neighbour method 
!         or adaptive kernel methods
!
! Ref :: Density Estimation for Statistics and Data Analysis
!        B.W. Silverman
!
!
!**********************************************************************

module kernel_estimators

!**********************************************************************
! originally developed for TDRW Version 2.0 
! reused for marfa 3.1beta 
!  marfa version 3.1 December 2006
!  James Mancillas 

! modified for marfa version 3.2 Novemver 2007 
! modified April 2008 version 3.2.1 
!  major rewrite November 2009 

use precision
use qsort_module

implicit none

public adaptive_kernel

!**********************************************************************

contains

subroutine adaptive_kernel(time,weight,B,num_NN,alpha)
   real(kind=dp), intent(in), dimension(:)   :: time   !  arrival  times
   real(kind=dp), intent(in), dimension(:)   :: weight !  statistical weights 
   !  probability locations (x) and estimated P(x) 
   real(kind=dp), intent(inout), dimension(:,:) :: B 
   real(kind=dp)  :: h !  global bandwidth
   real(kind=dp), intent(in)                                  :: alpha
   integer, intent(in)                                        :: num_NN

   real(kind=dp)                                :: den , wfac , x , sum , bwidth 
   integer                                      :: num_observations
   integer                                      :: num_prob
   integer                                      :: i,j,m 

   real(kind=dp), dimension(:),allocatable   :: pilot ! pilot density 

   num_observations=size(time)

   allocate(pilot(num_observations) ) 

   h=opt_bandwidth(time,weight,alpha)  
   call NN_method(time,weight,pilot,num_NN) 


   num_prob=size(B(:,1))        

   ! use pilot data to calculate sensitivity value to adjust bandwidth 
   ! reuse pilot array 
   ! normalize to unity 
 
    sum=0.0d0 
    do i=1,num_observations 
      pilot(i) = 1.0d0/pilot(i)**alpha 
      sum = sum + pilot(i) 
    end do  
    sum=sum/real(num_observations) 
    do i=1,num_observations 
     pilot(i)=pilot(i)/sum 
    end do 


    do i=1,num_prob

       call find_index(time, B(i,1), m) 

       den = 0.0d0 
       do j=m,1,-1 
         bwidth = min( h*pilot(j) , 50.0*h) ! truncate bandwidth if too large 
         x = (B(i,1)-time(j))/bwidth 
         if( abs(x) .gt. 10.) exit 
         den = den + weight(j)*kernel(x)/bwidth 
       end do 
       do j=m+1,num_observations
         bwidth = min( h*pilot(j) , 50.0*h) 
         x = (B(i,1)-time(j))/bwidth 
         if( abs(x) .gt. 10.) exit 
         den = den + weight(j)*kernel(x)/bwidth 
       end do 

       B(i,3)=den

    end do

    deallocate(pilot) 
    return

end subroutine adaptive_kernel

!**********************************************************************

subroutine NN_method(time,weight,pilot, num_NN) 

   real(kind=dp), intent(in), dimension(:)     :: time        !  individual arrival times 
   real(kind=dp), intent(in), dimension(:)     :: weight      !  individual weight  
   real(kind=dp), dimension(:), intent(inout) :: pilot 
   integer, intent(in)     :: num_NN          !  number of nearest neighbours
   integer :: nnn 

   integer      :: num_observations, num_prob_pts
   integer      :: i,j                          

   real(kind=dp) :: nndist ,x, wfac 

   num_observations=size(time)

    nnn =  min( real( num_NN ),  sqrt(real( num_observations)))     
    do i=1,num_observations 
    
       pilot(i) = 0.0d0 
       nndist = distance_NN(time,i,nnn) 

       pilot(i) = real(nnn) / & 
           (2.0d0*real(num_observations)*nndist)  

     end do

   return

end subroutine NN_method 

!***********************************************************************
function kernel(value)  result(return_value)   ! this kernel is a gaussian estimation!    
!***********************************************************************
   real(kind=dp), intent(in) :: value  
   real(kind=dp)             :: return_value 
  
   real(kind=dp),parameter   :: one_over_sqrt2pi=0.398942280402
    
        if(abs(value)>5)then                            ! use a cutoff value to minimize 
          return_value=0                                ! computational cost by truncating 
        else
          return_value=one_over_sqrt2pi*exp(-value*value*0.5)
        end if

      return

end function kernel 

!**********************************************************************
!**********************************************************************

function distance_NN(A,pos,num_NN)  result(dk) 

  ! NN function which calculated the distance to the k^th nearest neighbour
  real(kind=dp), intent(in), dimension(:) :: A              ! array containing observations
  integer, intent(in) :: pos  !  position of interest in array 
  real(kind=dp)           :: dk             ! distance to the K^th nearest neighbour
  integer, intent(in)     :: num_NN         ! number of nearest neighbour to include in window

  real(kind=dp),dimension(2*num_NN+1) :: adist 
  integer                 :: m,upper_bound, lower_bound ,i 
 

    upper_bound=min( size(A), pos+num_NN) 
    lower_bound=max( 1, pos-num_NN) 

    m=0  
    do i=lower_bound, upper_bound, 1 
      m=m+1 
      adist(m) =  abs( A( i ) - A(pos) ) 
    end do 
    call qsort_1d(adist(1:m))

    dk=adist(num_NN + 1) 

    if(dk.le.0) dk= 0.0001  ! prevent divide by zero


end function distance_NN

!**********************************************************************

subroutine find_index(A,t,index)                 
   ! this function finds the 'index' of the array element A[i] which 
   ! immediately preceeds the value given for 't' 
   real(kind=dp), intent(in), dimension(:)  :: A 
   integer                                  :: max
   real(kind=dp), intent(in)                :: t
   integer      , intent(inout)             :: index

    max=size(A)

    index = 1 
    do  
      if( t < A(index)) exit 
      index = index+1 
      if(index > max) exit 
    end do 
    index=index-1 
       
    return

end subroutine find_index

!**********************************************************************

function opt_bandwidth(A,weight,sp) result(optbw)

real(kind=dp), dimension(:), intent(in) :: A   ! sorted array ! 
real(kind=dp), dimension(:), intent(in) :: weight   ! weights  

real(kind=dp), dimension(:), allocatable :: wcm ! cumuulative weights  

real(kind=dp)                           :: optbw
real(kind=dp)                           :: iqd   ! interquartile distance  ! 
real(kind=dp)                           :: sig   ! sigma                   !
real(kind=dp)                           :: sum,mean,wsum 
real(kind=dp)                           :: sp  ! V3.2 
integer                                 :: num,i,i1,i2 

    num=size(A)


   allocate( wcm(0:num) ) 
   wcm(0)=0.0 
   do i=1,num 
     wcm(i) = wcm(i-1) + weight(i) 
   end do 
   wsum = wcm(num) 

   do i=1,num 
     if( wcm(i) .lt. 0.75*wsum ) cycle  
     i2 = i 
     exit 
   end do 

   do i=1,num 
     if( wcm(i) .lt. 0.25*wsum ) cycle  
     i1 = i 
     exit  
   end do 

if(i2-i1 .gt. 3)then

   iqd=( A(i2)-A(i1) )/1.34 

else

   do i=1,num 
     if( wcm(i) .lt. 0.5*wsum ) cycle  
     i1 = i 
     exit  
   end do 

   iqd = A(i1)/10.  
   
end if

   deallocate(wcm) 
   
    sum=0 
    do i=1,num
    sum=sum+A(i)*weight(i) 
    end do
    mean=sum/wsum 

    sum=0
    do i=1,num
     sum=sum+weight(i)*(mean-A(i))**2
    end do
    sig=sqrt(sum/wsum)    


    if(iqd.le.sig)then
     optbw=iqd*0.9/real( num, dp )**sp  !V3.2 changed from 0.2 
    else
     optbw=sig*0.9/real( num, dp )**sp  !V3.2 changed from 0.2 
    end if

    return

end function opt_bandwidth


!***********************************************************************
!***********************************************************************
!***********************************************************************

end module kernel_estimators

!***********************************************************************
! EOF


