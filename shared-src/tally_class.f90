!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland). 
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


! *********************************************************************
! File Name        : tally_class.f95 
! Program Name     : marfa 3.2.1 
! Developed for    : SKB and Posiva 
! Revision History : Version 3.1 reused from tdrw Version 2.0 December 2006 
!                  : Version 3.1.1 September 2007 
!                       fix error in write statement Sept 2007 Scott Painter
!                       fix logic error in file close(funit) operation Sept 2007 J Mancillas 
!                  : Version 3.2  added sensitivity parameter as input J. Mancillas 
!                  : Version 3.2.1 SCR716 item 1 S. Painter April 2008    
!                  : Version 3.2.2 SCR722 item x S. Painter November 2009 
!                  :    Version 3.2.2 is significant re-write from previous versions 
!                  :    now has little in common with tdrw  
! SRD Section      : Section 3 
! *********************************************************************


module tally_class

!**********************************************************************
! this module part of post processing package for marfa 
! originally developed for tdrw version 2 
! James Mancillas April 2006 
! minor revision September 2007 
! major revision in November 2009 Scott Painter 
!**********************************************************************


use precision

use qsort_module
use kernel_estimators
use io

implicit none

private 


public tally 
public calculate_breakthru 
public get_results_data
public get_results_metadata
public get_usr_prob
public save_results

!**********************************************************************
!**********************************************************************

type tally
  real(kind=dp), dimension(:),    pointer     :: part_time
  real(kind=dp), dimension(:),    pointer     :: part_weight
  real(kind=dp), dimension(:,:),  pointer     :: prob
  real(kind=dp)                                   :: decay_const
  real(kind=dp)                                   :: sensitivity
  real(kind=dp)                                   :: t_offset
  integer                                         :: num_parts_used
  integer                                         :: id
  integer                                         :: num_part
  character(len=10)                                :: name
  
end type tally

!**********************************************************************
contains

!**********************************************************************
!**********************************************************************

function tally_(time,weight,id,name,decay_const) result(nuc) 

real(kind=dp),dimension (:),intent(in) :: time
real(kind=dp),dimension (:),intent(in) :: weight
type(tally)                :: nuc
integer,intent(in)                       :: id
character(len=20), intent(in)            :: name
real(kind=dp),intent(in)                 :: decay_const

integer                                  :: num_part

   num_part=size(time)

   if(num_part.ge.1)then

     allocate(nuc%part_time(num_part))
     allocate(nuc%part_weight(num_part))

       nuc%part_time=time
       nuc%part_weight=weight
       nuc%id=id
       nuc%num_part=num_part
       nuc%name=name(1:10)
       nuc%decay_const=decay_const

   end if

   return

end function tally_

!**********************************************************************

subroutine calculate_breakthru(nuc)
type(tally), intent(inout)                      :: nuc

integer                                         :: num_prob
integer                                         :: num_NN,i,j,index1,index2
real(kind=dp)                                   :: min_time, max_time
real(kind=dp)                                   :: alpha
real(kind=dp)                                   :: binwidth
real(kind=dp)                                   :: begining
real(kind=dp)                                   :: max
real(kind=dp)                                   :: min

real(kind=dp),dimension(:), allocatable         :: log_time
real(kind=dp),dimension(:,:), allocatable       :: log_prob

! initialize NN variables

num_NN=25
alpha=nuc%sensitivity 
num_prob=0
nuc%num_parts_used = 0

! check to ensure real particles exist
if(nuc%part_weight(1) .gt. 0) then

!  if the number of particle that breaks through is less than
!  50, then the num_NN will be reduced

     if(nuc%num_part.lt.50)then
         num_NN=(nuc%num_part)/3
         if(num_NN.le.2) num_NN=1
     end if

! log transform the time data and the probability estimation points

   num_prob=size(nuc%prob(:,3))
   min_time = nuc%prob(1,1)
   max_time = nuc%prob(num_prob,1)
   
    allocate(log_time(nuc%num_part))
    allocate(log_prob(num_prob,3))

! transform particle time to log time scale
! and check if particle is inside the specified "time window"

   do index1=1,nuc%num_part
     if (nuc%part_time(index1)>=min_time .and. nuc%part_time(index1)<=max_time) then
        nuc%num_parts_used = nuc%num_parts_used + 1
     end if
   
     log_time(index1)=log(nuc%part_time(index1))
   end do

! transform probability times to log time scale

   do index1=1,num_prob
     log_prob(index1,1)=log(nuc%prob(index1,1))
     log_prob(index1,2)=0.0
     log_prob(index1,3)=0.0
   end do




! calls the adaptive kernel if there are at least two particles that breakthrough for the RN

   if( nuc%num_part.gt.1 )then
    call adaptive_kernel(log_time(:),nuc%part_weight,log_prob,num_NN,alpha)
   end if

! back trasform log_prob   f(x)=g(t(x))t`(x)

    do index1=1,num_prob
      nuc%prob(index1,3)=log_prob(index1,3)/nuc%prob(index1,1)
    end do

end if ! no real particles breakthrough

! calculate cumulative breakthrough curve
! note: remember that if no particles broke through, an imaginary particle of
! time=-1.00 and weight=-1.00 was created => always have at least one particle
!
      j=1
   do i=1,num_prob 
      nuc%prob(i,2)=0.0d0

      if(i.gt.1)then
         nuc%prob(i,2)=nuc%prob(i-1,2)
      end if

      if(j.le.nuc%num_part )then

         do while( ( j.le.nuc%num_part) .and. (nuc%part_time(j) .lt. nuc%prob(i,1)) )

            if(nuc%part_weight(j).gt.0)then
               nuc%prob(i,2)=nuc%prob(i,2)+nuc%part_weight(j)
            end if
             j=j+1
            if(j.gt.nuc%num_part) goto 1122

         end do

      end if
  1122 continue

   end do
! end cumulative breakthrough curve

   if(allocated(log_time)) deallocate(log_time)
   if(allocated(log_prob)) deallocate(log_prob)

   return

end subroutine calculate_breakthru 

!**********************************************************************

subroutine print_mass_breakthru(nuc,funit)
type(tally),intent(in)                      :: nuc
integer, intent(in)                         :: funit

integer                                     :: num_prob
integer                                     :: i

num_prob=size(nuc%prob(:,1))

  if(nuc%part_weight(1).gt.0.0d0)then
    write(unit=funit,fmt=*) '! ID= ',  nuc%id ,'Name= ', nuc%name , '# of particles= ', nuc%num_part
    write(unit=funit,fmt=*) "! # of particles inside time window= ", nuc%num_parts_used
  else
    write(unit=funit,fmt=*) '! ID= ',  nuc%id ,'Name= ', nuc%name , '# of particles= ', nuc%num_part -1
    write(unit=funit,fmt=*) "! # of particles inside time window= ", nuc%num_parts_used
  end if

     !sp v3.2.1 corrected units in inst breakthrough 
     write(unit=funit,fmt=*) '! Time           Cumulative      Instantaeous '
     write(unit=funit,fmt=*) '! (yrs)          Breakthrough    Breakthrough '
     write(unit=funit,fmt=*) '!                (Mole)           (Mole/yr)   '
    do i=1,num_prob
      write(unit=funit, FMT=77 ) nuc%prob(i,1), nuc%prob(i,2),nuc%prob(i,3)
    end do
    write(unit=funit,fmt=*) " "
    return
 77 FORMAT (E13.4, 6x, E15.7, 4x, E15.7 )

end subroutine print_mass_breakthru

!**********************************************************************

subroutine print_Bq_breakthru(nuc,funit)

type(tally),intent(in)                      :: nuc
integer, intent(in)                         :: funit

integer                                     :: num_prob
integer                                     :: i

real(kind=dp)                             :: conversion 

! convertion converts Mol to Bq given the lambda in yr^-1
! using 365 day/yr * 24 hr/day * 3600 sec/hr 
! Mol  6.022E23 
conversion=1.9095636732E16

num_prob=size(nuc%prob(:,1))

  if(nuc%part_weight(1).gt.0.0d0)then
    write(unit=funit,fmt=*) '! ID= ',  nuc%id ,'Name= ', nuc%name , '# of particles= ', nuc%num_part
    write(unit=funit,fmt=*) "! # of particles inside time window= ", nuc%num_parts_used
  else
    write(unit=funit,fmt=*) '! ID= ',  nuc%id ,'Name= ', nuc%name , '# of particles= ', nuc%num_part -1
    write(unit=funit,fmt=*) "! # of particles inside time window= ", nuc%num_parts_used
  end if

  
     !sp v3.2.1 corrected units in breakthrough 
     write(unit=funit,fmt=*) '! Time           Cumulative      Instantaeous '
     write(unit=funit,fmt=*) '! (yrs)          Breakthrough    Breakthrough '
     write(unit=funit,fmt=*) '!                (Bq)            (Bq/yr)      '
    do i=1,num_prob
      write(unit=funit, FMT=77 ) nuc%prob(i,1), nuc%prob(i,2)*conversion*nuc%decay_const,nuc%prob(i,3)*conversion*nuc%decay_const
    end do
    write(unit=funit,fmt=*) " "
    return
 77 FORMAT (E13.7, 6x, E15.7, 4x, E15.7 )

end subroutine print_Bq_breakthru 

!**********************************************************************

subroutine save_results(nuc,npart)

type(tally),dimension(:), intent(inout)  :: nuc  !V3.2.2 changed intent 
integer(kind=npk) :: npart 

integer                                         :: RN,i
integer                                         :: funit1,funit2
RN=size(nuc)

  print*, "Saving results"

  ! add the offset V3.2.2 
  do i=1,RN
    nuc(i)%prob(:,1) = nuc(i)%prob(:,1) + nuc(i)%t_offset  
  end do


  funit1=findunit() 
  open(unit=funit1,file="Breakthrough_Mo.rlt")
  call writeheader(funit1,npart)

  do i=1,RN
    call print_mass_breakthru(nuc(i),funit1)
  end do
  close(funit1)

  funit2=findunit() 
  open(unit=funit2,file="Breakthrough_Bq.rlt")
  call writeheader(funit2,npart)

  do i=1,RN
    call print_Bq_breakthru(nuc(i),funit2)
  end do
  close(funit2)

  return

end subroutine save_results

!**********************************************************************
function get_results_metadata() result(npart) 
  integer(kind=npk) :: npart 
  integer :: funit 
  funit=openread('results.md')              
  read(unit=funit,fmt=*) npart 
  close(funit) 
end function get_results_metadata 

function get_results_data(asciiflag) result(results) 

logical :: asciiflag 
type(tally), dimension(:),  pointer           :: results
integer                                       :: RN

character(len=20),dimension(:),allocatable    :: RN_names
real(kind=dp), dimension(:),allocatable       :: Lambda
integer                                       :: funit,loop_count,type_RN
integer,dimension(:), allocatable             :: counter
real(kind=dp), dimension(:,:), allocatable    :: tmparr  ! 2-d array containing the particles
real(kind=dp)                                 :: time, weight   ! breakthrough time and weights
character(len=10)                             :: source
integer                                       :: type_traj
integer                                       :: i,j
integer                                       :: sort_column
integer(kind=npk)                             :: npart 
character(len=80)                             :: linebuffer

INTEGER, dimension(:,:), pointer              :: selected_traj

! initialize qsort column

 sort_column=1   

     ! open up postprocessor.dat and determine which trajectories to analyize

      call  get_selected_traj(selected_traj)


       funit=openread('results.md')              ! on first pass determine the number of RN
       read(unit=funit,fmt=*) npart 
       read(unit=funit,fmt=*) RN                  ! and allocate for the 'counter' array and the
       if(RN.lt.1)then                            ! the 'RN_names' array and 
         print*,'No Radionuclides in results.dat' ! tally type array "results" 
         stop                                     !
       end if                                     !
       allocate(RN_names(RN))                  
       allocate(lambda(RN))                   

       do i=1,RN                       
         read(unit=funit,fmt=*) RN_names(i) , Lambda(i)
       end do

       close(funit) !done with metadata  

     ! allocate arrays 
     allocate(counter(RN))
     allocate(results(RN))


    ! now for the data 

     funit=findunit() 
     if( asciiflag) then 
       open(unit=funit, file='results.dat') 
     else 
       open(unit=funit, form='unformatted', file='results.dat') 
     end if 

    ! scan through the file and count 

     counter=0                       
     do 
       if( asciiflag) then 
        read(unit=funit,fmt=*,END=500) time, type_RN , weight, source, type_traj 
       else 
        read(unit=funit,END=500) time, type_RN , weight, source, type_traj 
       endif 
       if(use_traj(selected_traj,type_traj) .and. weight .gt. 0.0d0) &  
         counter(type_RN)=counter(type_RN)+1                                
     end do 


500  rewind(unit=funit)  
     !call skipheader(funit) 

!      scan through the input file and collect time and weight
!      starting at the begining of the data file
!      skips header and RN+2 lines 

 do i=1,RN
   if(counter(i).gt.0)then
     allocate( results(i)%part_time(counter(i))) 
     allocate( results(i)%part_weight(counter(i))) 
   else 
     allocate( results(i)%part_time(1)) 
     allocate( results(i)%part_weight(1)) 
   end if 
 end do 

!    do j=1,2+RN                          ! skips RN name data lines
!      read(unit=funit,fmt=*)                 ! for "results.dat"
!    end do

    counter=0  
    do 
       if(asciiflag) then 
         read(unit=funit,fmt=*,END=302) time, type_RN, weight, source,type_traj
       else 
         read(unit=funit,END=302) time, type_RN, weight, source,type_traj
       end if 
       if(use_traj(selected_traj,type_traj) .and. weight .gt. 0.0d0)then
            counter(type_RN) = counter(type_RN) + 1 
            results(type_RN)%part_time( counter(type_RN) ) = time  
            results(type_RN)%part_weight( counter(type_RN) ) = weight 
        end if
    end do  
302 continue


     do i=1,RN
       if( counter(i) .eq. 0) then 
        counter(i) = 1 
        results(i)%part_time(1)=-1.0 
        results(i)%part_weight(1)=-1.0 
       end if 
     end do 

     do i=1,RN 
      allocate( tmparr( counter(i),2) )  
      tmparr(:,1)=results(i)%part_time
      tmparr(:,2)=results(i)%part_weight 
      call qsort_2d(tmparr,sort_column)                     
      results(i)%part_time  =tmparr(:,1) 
      results(i)%part_weight=tmparr(:,2) 
      deallocate( tmparr ) 
      results(i)%decay_const = lambda(i) 
      results(i)%id = i 
      results(i)%name = RN_names(i)
      results(i)%num_part = counter(i) 
     end do 
       
! ends collecting data from file and copying to 'results'

  deallocate(counter)
  deallocate(RN_names)
  deallocate(lambda)

  close(funit) ! SCR722 

  return
end function get_results_data

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

subroutine get_usr_prob(results)

! Version 3.2.2 complete rewrite by Scott Painter
!
!


type(tally), dimension(:), pointer  :: results
type(tally), pointer :: atc ! V3.2.2  

logical                                    :: file_exist
logical :: nptime ! V3.2.2 
integer                                    :: num_prob,p_count
integer                                    :: funit,i, num_ranges
integer                                    :: RN, index1,int_val
integer  :: ios,isize ! V3.2.2 
real(kind=dp),dimension(:,:), allocatable  :: prob
real(kind=dp)                              :: min,max
real(kind=dp)                              :: begining,binwidth
real(kind=dp)                              :: sens_parameter ! V3.2 
real(kind=dp)                              :: t_offset !V3.2.2 
CHARACTER(len=80)                          :: temp
CHARACTER(len=1)                           :: selection

RN=size(results)
num_prob=0
selection="z"
sens_parameter=0.2
t_offset = 0.0

! check for postprocessor.dat file

inquire(file="postprocessor.dat",exist=file_exist)

!*************************************
!  if file is not present, allow kernel estimators to use default 
!  calculation points


if( .not.file_exist) then 

  selection="a"
  sens_parameter=0.2
  t_offset = 0.0

else  ! file exists  

  funit=openread("postprocessor.dat")

  ! get sensitivity parameter and offset data  

  call skipheader(funit)

  read(unit=funit, fmt='(a)') temp
  temp=adjustl(temp) 
  read(temp,*) sens_parameter
  isize=index( temp , ' ')
  read(unit=temp(isize:) , fmt=*,iostat=ios) t_offset
  if(ios .ne. 0) then
      print *, 'no time offset present. using default value of zero'
      t_offset = 0.0
  end if

  ! the next read relates to set of trajectories to be analyzed 
  !  data have already been read and assigned 
  read(funit,*) selection 
  do
      call skipheader(funit)
      read(funit,*) selection
      int_val=iachar(selection)
      if( int_val .le. 57 .and. int_val .ge. 48 ) cycle  
      backspace(funit)
      exit 
  end do

  call skipheader(funit)
  read(funit,*) temp
  read(temp,'(a)') selection
  call skipheader(funit)

end if 

! selection should contain A, M or L 

if(selection .eq. "A" .or. selection .eq. "a") then

num_prob=200                                           ! default number of calculation points

print*, "Default breakthrough calculation points selected"
print*, "200 points uniformly distributed from min to max times"
    
do index1=1,RN                                          ! scan through all RN
   allocate(results(index1)%prob(num_prob,3))

   if(results(index1)%part_weight(1).gt. 0.00)then      ! ensure at least one real particle
     if(results(index1)%num_part .gt. 1) then           ! if more than one real particle exists
       min=results(index1)%part_time(1)*0.95            ! assign times based on min and max times
       max=results(index1)%part_time(results(index1)%num_part)*1.01
       binwidth=(max-min)/num_prob
       begining=min
     else
       min=results(index1)%part_time(1)*0.95            ! if only one real particle
       max=results(index1)%part_time(1)*1.05            ! assign times based on single particle
       binwidth=(max-min)/num_prob
       begining=min
     end if
   else                                 ! if no real particles
     min=0.0                            ! assign default time values between 0 and 10
     max=10.0
     binwidth=(max-min)/num_prob
     begining=min
   endif

    if(begining.lt.0)begining=0 
    results(index1)%prob(1,1)= begining
    results(index1)%prob(1,2)=0
    results(index1)%prob(1,3)=0

    do i=2,num_prob
      results(index1)%prob(i,1)=binwidth+results(index1)%prob(i-1,1)
      results(index1)%prob(i,2)=0
      results(index1)%prob(i,3)=0
    end do


    results(index1)%sensitivity=sens_parameter  ! V3.2 
    results(index1)%t_offset= t_offset !V3.2.2 

end do


else if(selection .eq. "U"  .or. selection .eq. "u") then

    call skipheader(funit)
    read(funit,FMT=*,end=113) num_prob
    call skipheader(funit)
113 continue
    if( num_prob .le. 0) num_prob=200 
   
print*,'Uniformly assigned Breakthrough curve, number of points= ', num_prob
    do index1=1,RN
      allocate(results(index1)%prob(num_prob,3))

   if(results(index1)%part_weight(1).gt. 0.00)then      ! ensure at least one real particle
     if(results(index1)%num_part .gt. 1) then           ! if more than one real particle exists
       min=results(index1)%part_time(1)*0.95            ! assign times based on min and max times
       max=results(index1)%part_time(results(index1)%num_part)*1.01
       binwidth=(max-min)/num_prob
       begining=min
     else
       min=results(index1)%part_time(1)*0.95           ! if only one real particle
       max=results(index1)%part_time(1)*1.05           ! assign times based on single particle
       binwidth=(max-min)/num_prob
       begining=min
     end if
   else                                                ! if no real particles
     min=0.0                                           ! assign default time values between 0 and 10
     max=10.0
     binwidth=(max-min)/num_prob
     begining=min
   endif

        if(begining.lt.0)begining=0.00   
        results(index1)%prob(1,1)= begining
        results(index1)%prob(1,2)=0.00
        results(index1)%prob(1,3)=0.00

        do i=2,num_prob
         results(index1)%prob(i,1)=binwidth+results(index1)%prob(i-1,1)
         results(index1)%prob(i,2)=0.00
         results(index1)%prob(i,3)=0.00
        end do

     results(index1)%sensitivity=sens_parameter  !set the sensitivity parameter V3.2 
     results(index1)%t_offset = t_offset ! V3.2.2 

    end do

else if(selection .eq. "L" .or. selection .eq. "l") then
    call skipheader(funit)
    read(funit,FMT=*,end=114) num_prob
    call skipheader(funit)
    114 continue
    if( num_prob .le. 1) num_prob=200 
 
    print*,'Logarithmically assigned Breakthrough curve, number of points= ', num_prob

do index1=1,RN
   allocate(results(index1)%prob(num_prob,3))

     if(results(index1)%part_weight(1).gt. 0.00)then      ! ensure at least one real particle
       if(results(index1)%num_part .gt. 1) then           ! if more than one real particle exists
         min=results(index1)%part_time(1)*0.95            ! assign times based on min and max times
         max=results(index1)%part_time(results(index1)%num_part)*1.01
         binwidth=(log(max)-log(min))/(num_prob-1)
         begining=min
       else
         min=results(index1)%part_time(1)*0.95          ! if only one real particle
         max=results(index1)%part_time(1)*1.05          ! assign times based on single particle
         binwidth=(log(max)-log(min))/(num_prob-1)
         begining=min
       end if
     else                                 ! if no real particles
       min=0.00001                        ! assign default time values between 0 and 10
       max=10.0
       binwidth=(log(max)-log(min))/(num_prob-1)
       begining=min
     endif

        if(begining.lt. 0.000001)begining=0.000001
        results(index1)%prob(1,1)= begining  
        results(index1)%prob(1,2)=0.00
        results(index1)%prob(1,3)=0.00

        do i=2,num_prob
         results(index1)%prob(i,1)=begining*exp( binwidth*(i-1))
         results(index1)%prob(i,2)=0.00
         results(index1)%prob(i,3)=0.00
        end do
 
     results(index1)%sensitivity=sens_parameter  ! set the sensitivity parameter V3.2 
     results(index1)%t_offset = t_offset ! V3.2.2 
end do

else if(selection .eq. "M" .or. selection .eq. "m")then 
  call skipheader(funit)
  read(funit,*,end=115) num_prob
  call skipheader(funit)
115  continue

print*,'Manually assigned Breakthrough curve, number of points= ', num_prob

  allocate(prob(num_prob,3))
  do i=1,num_prob
     read(funit,*) temp
     read(temp,*) prob(i,1)
     prob(i,2)=0.00
     prob(i,3)=0.00
  end do

  do i=1,RN
    allocate(results(i)%prob(num_prob,3))
    results(i)%prob=prob
    results(i)%sensitivity=sens_parameter  ! set the sensitivity parameter V3.2 
    results(i)%t_offset = t_offset ! V3.2.2 
  end do

  deallocate(prob) 


end if   ! selection M
!*************************************

! shift by time offset 
!  SP for V3.2.2 
do index1=1,RN
 
 atc => results(index1) 

 if( .not. associated( atc%part_time) ) then 
   print *, 'not allocated' 
 end if 

 do i=1,size( atc%part_time) 
 atc%part_time(i) = atc%part_time(i) - atc%t_offset 
 end do 
 atc%prob(:,1) = atc%prob(:,1) - atc%t_offset 

 nptime = any( atc%part_time .le. 0.0 .and. atc%part_weight .gt. 0.0)   
 if( nptime ) then 
  print *, 'particle arrives before specified start time' 
  print *,  ' check time offset value in postprocessor.dat' 
  print *, atc%t_offset, index1 
  print *, minval( atc%part_time ), minloc(atc%part_time)  
  stop 
 end if 
 nptime = any( atc%prob(:,1) .le. 0.0 ) 
 if( nptime .and. any( atc%part_weight .gt. 0.0) ) then 
  print *, 'requested breakthrough before specified start time' 
  print *,  ' check time offset value in postprocessor.dat' 
  print *, atc%prob(1,1)  
  stop 
 end if 

end do 
!  end V3.2.2 


if(file_exist) close(funit) 

   return

end SUBROUTINE get_usr_prob

!**********************************************************************

SUBROUTINE  get_selected_traj(selected_traj)
INTEGER, dimension(:,:), pointer   :: selected_traj
LOGICAL         :: ALL, file_exist 
INTEGER                                                :: funit,index1, num_points
real(kind=dp)                                          :: sp
CHARACTER(len=1)                                       :: mode
INTEGER                                                :: count

    inquire(file="postprocessor.dat",exist=file_exist)

    if(file_exist)then         ! collect and use user supplied values 
       All=.false.

       funit=openread('postprocessor.dat')

       call skipheader(funit)            ! skip comments and blank lines

       read(funit,*,end=9988) sp  
       read(funit,*,end=9988) mode

       if( mode .eq. 'A' .or. mode .eq. 'a')then
           print*, "Breakthrough calculations using all trajectories"
           ALL=.true.
       else if(mode .eq. 'P' .or. mode .eq. 'p')then
           print*, "Breakthrough calculations using partial trajectory list"
           ALL=.false.
           read(funit,*, end=9988) count

           if(count.lt.1)then
              ALL=.true.
           else
             allocate(selected_traj(count,2))

             do index1=1,count
               read(funit,*,end=9988) selected_traj(index1,1), selected_traj(index1,2)
             end do
           end if

        else  
           print*, "Format error in 'postprocessor.dat'"      ! use all trajectories
           print*, "Breakthrough calculations using all trajectories"
           ALL=.true.
        end if
    
      close(funit)

    else
       All=.true.
    end if

    if( ALL) then 
      count = 1 
      allocate(selected_traj(count,2))
      selected_traj(1,1) = 1 
      selected_traj(1,2) = 999999999
    end if 
      
    return

! if you have reached this point there is an input error
9988 continue

     print*, "Format error in 'postprocessor.dat'"     ! use all trajectories
     print*, "Breakthrough calculations using all trajectories"

     stop 

end subroutine get_selected_traj

!**********************************************************************

FUNCTION   use_traj(selected_traj,type_trj) result(vnv)
INTEGER, dimension(:,:), intent(in)              :: selected_traj
INTEGER, intent(in)                              :: type_trj
INTEGER                                          :: num_ranges, index1
LOGICAL                                          :: vnv               !(valid-not-valid)

num_ranges=size(selected_traj(:,1))

    do index1=1,num_ranges
      if(type_trj.ge.selected_traj(index1,1).and. type_trj .le. selected_traj(index1,2) )then
        vnv=.true.
        goto 3445
      end if
    end do

    vnv=.false. 
3445  continue
 

return

end function use_traj
!**********************************************************************
end module tally_class
