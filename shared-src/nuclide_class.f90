
!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland).
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

! *********************************************************************
! File Name        : nuclide_class.f95  
! Program Name     : marfa  
! Developed for    : SKB and POSIVA  
! Revision History : tdrw Version 1.0  March 2006   Scott Painter
!                  : marfa Version 3.1beta June 2006  Scott Painter
!                  : marfa Version 3.1 December 2006 Scott Painter 
!                  : marfa Version 3.2.2 November 2009 
!                  : marfa Version 3.2.3 June 2011 Scott Painter 
! SRD Section      : Section 3 item 7 
! *********************************************************************
!                  : V3.2.2  minor format change on print statement SP 
!                  : V3.2.3  read splitting parameter (optional) 

! module nuclide_class 
! data structure and constructor for nuclide 
! module originally developed for tdrw version 1 

! importance weighting added Dec 5, 2006

module nuclide_class 

 use precision 
 implicit none 

 type nuclide 
   real(kind=dp) :: lambda 
   integer :: next,ielem  
   character(len=10) :: name 
   real(kind=dp) :: importance  
   integer :: splitatbirth 
 end type 

 contains 

 function nuclide_(iunit, elementname, nuclidename,nn)  result(a) 

  character(len=10) :: ename,nname  
  character(len=10), dimension(:) :: elementname
  character(len=10), dimension(0:nn) :: nuclidename  
  character(len=100) :: buffer 
  integer :: iunit , i, nelem ,nn , sflag , i1, i2 
  integer :: ios , ios1 
  real(kind=dp) :: foo 
  type(nuclide) :: a 
  
  nelem = size(elementname) 

  !V3.2.3 added splitatbirth 
  read(unit=iunit,fmt='(a)') buffer 
  read(unit=buffer,fmt=*,iostat=ios) a%name, a%lambda, ename, nname 
  if(ios .ne. 0) then 
   print *, 'error in reading nuclide information'  , a%name 
  end if 

  i1=index(buffer,trim(adjustl(nname))) 
  i2=len_trim(nname) 
  read(unit=buffer(i1+i2+1:),fmt=*,iostat=ios) a%importance
  if( ios .eq. 0) then 
    read(unit=buffer(i1+i2+1:),fmt=*,iostat=ios1) foo, a%splitatbirth  
    if( ios1 .ne. 0) a%splitatbirth=0 
  else 
    a%importance=1.0 
    a%splitatbirth=0 
    print *, 'importance for ', nname, ' defaulting to 1' 
  end if 

  ! read(unit=iunit,fmt=*,iostat=ios) a%name, a%lambda,  ename,nname  , a%importance , a%splitatbirth  
  !V3.2.3 end 

  if( a%importance .le. 0.0) then 
   print *, 'importance for ', a%name , ' is '  , a%importance 
   print *, 'resetting to 1.0d-30' 
   a%importance=1.0d-30
  end if 

  do i=0,nn
   if( nname .eq. nuclidename(i)) then 
      a%next = i
      exit 
   end if 
  end do 
  if( i .gt. nn) then 
   print *, 'next nuclide not in list', nname 
   stop 
  end if 

  do i = 1, nelem 
   if( ename .eq. elementname(i)) then 
      a%ielem = i  
      exit  
   end if 
  end do 
  if( i .gt. nelem) then 
   print *, 'element not in list ', ename   !V3.2.2 
   stop 
  end if 

  end function nuclide_ 
   
end module nuclide_class 

