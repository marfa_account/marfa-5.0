!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland). 
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


! *********************************************************************
! File Name        : hydrofacies.f95  
! Program Name     : marfa  
! Developed for    :  SKB and POSIVA 
! Revision History : Version 3.1beta  June 2006   Scott Painter
!                  : Version 3.1      Dec  2006   Scott Painter 
!                  :         3.2      Nov  2007   James Mancillas
!                  : Version 3.2.1    April 2008  Scott Painter 
!                  :                  SCR716 Item 5 
!                  : 3.2.5 Scott Painter 
!                  :  removed allocatable components from derived types 
! SRD Section      : Section 3
! *********************************************************************

! module hydrofacies  
! This module defines the hydrofacies structure 
! A segmentpool data structure is used for stochastic facies 
! This module is basically just data structures and constructors 
! V3.2: updated to allow differing retention parameters
!   for individual flow epochs  
! V3.2.3 added tabular retention model 

 module hydrofacies 

   use precision 
   use io   
   use retention_md_class   ! equilibrium sorptin 
   use retention_es_class   ! matix diffusion  
   use retention_ld_class   ! limited matrix diffusion  
   use retention_tab_class  ! tabular 


   implicit none 

   private 

   public hf ,hfset ,init_hf, segmentpool

   ! V1.0 
   type segmentpool  
     integer :: nseg         ! number of segments 
     real(kind=dp), dimension(:), pointer :: tau 
     real(kind=dp), dimension(:), pointer :: beta 
     real(kind=dp), dimension(:), pointer :: ell      !length 
     real(kind=dp), dimension(:), pointer :: next   !next class 
   end type segmentpool 
   ! V1.0 


   type hf 

      type(retention_LD), pointer :: ptr_to_SW => null()

      type(retention_ES), pointer :: ptr_to_ES => null()  
      type(retention_MD), pointer :: ptr_to_MD => null()  
      type(retention_LD), pointer :: ptr_to_LD => null()  
      type(retention_TAB), pointer :: ptr_to_TAB => null()  
      character(len=15) :: faciesID   !sp v3.2.1 changed 5 to 15 
      real(kind=dp) :: alpha           ! dispersivity 
      real(kind=dp) :: W            ! flowpath width 
      real(kind=dp) :: delSW           ! stagnant zone width 
      real(kind=dp) :: dm !molecular diffusion coefficient  
      logical :: stoch = .false.         ! stochastic flag 
      logical :: rescalc = .false. 	! calculate resident concentration 

      !4amphos 
      character(len=10), dimension(:), pointer :: statelabel

      ! used only for stochastic facies  
      integer :: numdir  ! number directions  
      integer :: numvc   ! number velocity classes  

      ! the following should be dimensioned to numdir 
      real(kind=dp),dimension(:),pointer :: vcpm      ! reference velocity  
      real(kind=dp),dimension(:),pointer :: theta     ! directions 
      real(kind=dp),dimension(:),pointer :: phi       ! directions   
      real(kind=dp),dimension(:),pointer:: gradh     ! magnitude of applied gradient  

      ! dimension to numdir by numvc 
      type(segmentpool),dimension(:,:),pointer :: spset  

   end type hf  

   type(hf), dimension(:), allocatable, target :: hfset 

   contains 
!***********************************************************************
   subroutine hf_(ahf,iunit,kdunit,nelem,datadir) 
!***********************************************************************
! V3.2
     type(hf) :: ahf 
     type(retention_LD), pointer :: aLD
     type(retention_MD), pointer :: aMD
     integer :: iunit,kdunit,nelem ,junit ,stoflag
     character(len=132) :: buffer 
     character(len=16) :: keywd 
     character(len=3) :: retmod 
     character(len=*) :: datadir 
     character(len=132) :: filename ,rootname 
     character(len=1) :: slash 
     character(len=1) :: SorD 
     character(len=15) :: faciesID
     real(kind=dp) :: diffSW, dm, foo  

     integer :: idir,istart  ,ivc 
     integer :: ielem 
     integer :: numdir, numvc 
     integer :: ntemp 
     integer :: ios
     integer :: istate, nstates 

     
     read(iunit,*) ahf%faciesID 

     read(kdunit,*) faciesID 
     if( trim(adjustl((ahf%faciesID))) .ne. trim(adjustl(faciesID)) )  then 
       print *, 'inconsistent rocktype names' 
       print *, faciesID, ahf%faciesID 
     end if 

     
     ! set defaults 
     ahf%W=0.0
     ahf%delSW=0.0 
     diffSW = 0.0 
     
     stoflag = 0 

     ahf%alpha = 0
     ahf%dm=0.0

     do 
       read(iunit, '(a132)') buffer 
       buffer=adjustl(buffer) 

       if(buffer(1:3) .eq. 'TAB' .or. buffer(1:2) .eq. 'ES') exit 
       if(buffer(1:2) .eq. 'MD' .or. buffer(1:2) .eq. 'LD') exit 
   
       if( buffer(1:3) .eq. 'STO') then 
          read(buffer, *) keywd,  numdir, numvc
          stoflag = 1 
       else if(buffer(1:3) .eq. 'STA') then 
          read(buffer,*) keywd, ahf%W, ahf%delSW, diffSW
          print *, 'STAGNANT WATER on' 
       else if(buffer(1:4) .eq. 'DISP') then 
          read(buffer,*) keywd, ahf%alpha 
          read(buffer,*,iostat=ios) keywd, foo , dm 
          if(ios .eq. 0) ahf%dm=dm 
          print *, 'dispersivity=', ahf%alpha 
          print *, 'molecular diffusion coefficient = ', ahf%dm
       else 
         print *, 'error in rocktype.dat file' 
         stop 
       end if 

     end do 

     read(buffer,*) retmod 

     if(retmod .eq. 'ES' ) then 
       call retention_ES_( ahf%ptr_to_ES, iunit,kdunit, nelem) 
       allocate( ahf%statelabel( ahf%ptr_to_ES%nstates) ) 
       ahf%statelabel => ahf%ptr_to_ES%statelabel 
     else if(retmod .eq. 'MD') then 
       call retention_MD_( ahf%ptr_to_MD, iunit,kdunit, nelem) 
       allocate( ahf%statelabel( ahf%ptr_to_MD%nstates) ) 
       ahf%statelabel => ahf%ptr_to_MD%statelabel 
     else if(retmod .eq. 'LD') then 
       call retention_LD_( ahf%ptr_to_LD, iunit,kdunit, nelem) 
       allocate( ahf%statelabel( ahf%ptr_to_LD%nstates) ) 
       ahf%statelabel => ahf%ptr_to_LD%statelabel 
     else if(retmod .eq. 'TAB') then 
       call retention_TAB_( ahf%ptr_to_TAB, iunit,kdunit, nelem) 
       allocate( ahf%statelabel( ahf%ptr_to_TAB%nstates) ) 
       ahf%statelabel => ahf%ptr_to_TAB%statelabel 
     else 
       print *, "invalid retention model ", retmod 
       stop 
     end if 

! now the water diffusion object
     allocate(ahf%ptr_to_SW)
     nullify(aLD)
     aLD => ahf%ptr_to_SW 

     nstates = 1 
     allocate(aLD%kappa(nelem,nstates))
     allocate(aLD%eta(nelem))
     allocate(aLD%ka(nelem,nstates))
     allocate(aLD%kcprime(nelem,nstates))
     allocate(aLD%statelabel(nstates))
     allocate(aLD%kca(nstates))

     if( diffSW .gt. 0.0d0) then  
      aLD%eta(:) = ahf%delSW/diffSW 
      do istate=1,nstates
         aLD%kappa(:,istate) = sqrt(diffSW) 
      end do
     else 
      aLD%eta(:) = 0.0d0 
      aLD%kappa(:,:) = 0.0d0 
     end if 
     aLD%kcprime(:,:) = 0.0
     aLD%ka= 0.0
     aLD%statelabel=ahf%statelabel
     aLD%nstates = nstates
     aLD%kca=0.0

     ! Allocate and populate the MD object for the stagnant water
     allocate(aLD%ptr_to_MD)
     nullify(aMD)
     aMD => aLD%ptr_to_MD
     allocate(aMD%kappa(nelem,nstates))
     allocate(aMD%ka(nelem,nstates))
     allocate(aMD%kcprime(nelem,nstates))
     allocate(aMD%statelabel(nstates))
     allocate(aMD%kca(nstates))
     aMD%kappa = aLD%kappa
     aMD%ka = aLD%ka
     aMD%kcprime = aLD%kcprime
     aMD%statelabel = aLD%statelabel
     aMD%kca = aLD%kca

 


     ! return if not a stochastic hydrofacies 
     if( stoflag .eq. 0) return  

     ahf%stoch = .true. 

     ahf%numvc = numvc 
     ahf%numdir = numdir 

     allocate( ahf%phi(numdir) ) 
     allocate( ahf%theta(numdir) ) 
     allocate( ahf%vcpm(numdir) ) 
     allocate( ahf%gradh(numdir) ) 

     
     ! determine if windows or unix 
     if( scan(datadir,'/') .gt. 0) then 
       slash='/'    !unix 
     else   
       slash='\'    ! windows  
     end if 

     rootname = trim(datadir)//trim(ahf%faciesID)//slash//'_' 
     istart=len_trim(rootname)

     ! load the segment pools 
     allocate( ahf%spset(numdir,numvc) ) 

     do idir=1,numdir 

       filename = rootname 
       write(filename(istart+1:), '(i0)' ) idir  

       junit=openread( trim(filename) ) 

       read(junit,fmt=*) ntemp 
       if( ntemp .ne. numvc) then 
         print *, 'error: inconsistency in the segment pool data' 
         print *, ntemp, ' velocity groups in ', trim(filename) 
         print *, numvc, ' groups in rocktypes.dat' 
         stop 
       end if 
       read(junit,fmt=*) ahf%gradh(idir),ahf%phi(idir),ahf%theta(idir),ahf%vcpm(idir) 

       do ivc=1,numvc 
         call segmentpool_( ahf%spset(idir,ivc), junit ) 
       end do 

       close(junit) 
     end do 

   end subroutine hf_

!***********************************************************************
   subroutine init_hf(nelem ) 
!***********************************************************************
! V3.2
     integer :: i,j,nelem ,numfacies , numfacies1 
     integer :: iunit, kdunit  
     integer :: nrc 
     character(len=80) :: datadir  
     character(len=15) :: faciesID 
     logical :: ex 


     iunit = openread("rocktypes.dat") 

     read(iunit,'(a80)') datadir ! already read and used. skip 

     ! get the location of the sub-grid trajectories 
     !  and add final slashes if not present already 

     read(iunit,'(a80)') datadir 
     i=len_trim(datadir) 
     if( scan(datadir,'/') .gt. 0) then
       if(datadir(i:i) .ne. '/') datadir(i+1:i+1)='/'    ! unix
     else
       if(datadir(i:i) .ne. '\') datadir(i+1:i+1)='\'    ! windows
     endif

     ! nepoch=num_flow_periods("trajectories.dat")

     read(iunit,*) numfacies 
     kdunit = openread("kdbins.dat") 
     read(kdunit,*) numfacies1 
     if(numfacies .ne. numfacies1) then 
       print *, 'inconsistency between rocktypes and kd file' 
       stop 
     end if 
     allocate(hfset(numfacies)) 

     do i=1,numfacies 
       call hf_( hfset(i),iunit,kdunit,nelem,datadir) 
     end do 

     close(iunit) 
     close(kdunit) 

   ! now check for rocktypes that need resident concentration 
     inquire(file="rc_control.dat", exist=ex) 
     if( .not.ex ) return 
     iunit = openread("rc_control.dat") 
     read( unit=iunit, fmt=*) nrc 
     do i=1,nrc 
      read(unit=iunit, fmt=*) faciesID  
      do j=1,numfacies 
        if( hfset(j)%faciesID .eq. faciesID ) exit 
      end do 
      if( j .gt. numfacies) then 
        print *, 'error in rc_control.dat file' 
        stop 
      end if 
      hfset(j)%rescalc = .true. 
     end do 
     close(iunit) 
     
   return 
   

   end subroutine init_hf 
!***********************************************************************
    subroutine segmentpool_(asp,iunit) 
!***********************************************************************

     integer :: iunit ,nseg , i 
     type(segmentpool) :: asp 

     read(iunit,*) nseg 
     asp%nseg = nseg  
     allocate(asp%tau(nseg) ) 
     allocate(asp%beta(nseg) ) 
     allocate(asp%ell(nseg) ) 
     allocate(asp%next(nseg) ) 

 
     do i=1,nseg 
      read(iunit,*) asp%tau(i),asp%beta(i),asp%ell(i),asp%next(i) 
     end do 

     return 
      
   end subroutine segmentpool_ 
!**********************************************************************

 end module hydrofacies  
