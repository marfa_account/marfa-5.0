!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland). 
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

module postprocess
!*********************************************************************
!quick program which opens the results data file to separate and organize
!the RNs by type and release time.
! 
! James Mancillas 04-01-06  (dd-mm-yy)
! 
!*********************************************************************
!**********************************************************************


private 

public post_process

contains

!**********************************************************************

subroutine  post_process(asciiflag)

use precision
use tally_class
use calculate_resident 

implicit none

logical :: asciiflag
integer                                           :: i       ! generic loop counters       
type(tally), dimension(:), pointer            :: results
integer                                           :: RN      ! number of radionuclides
integer(kind=npk) :: npart 
!**********************************************************************
!  get nuclide data from 'results.dat' and stores information into
! tally class 'results

   npart = get_results_metadata() 
   results=>get_results_data(asciiflag)
   RN=size(results) 

!**********************************************************************
! get usr defined calculation points, if 'brktimes.dat' not present  use default values

   call get_usr_prob(results)

!**********************************************************************
! calculates instantaneous breakthrough curves
! calculates the cummulative breakthrough curves

    do i=1,RN
       call  calculate_breakthru(results(i))
    end do

!**********************************************************************
! stores results in results_pp.dat

   call  save_results(results,npart)

!**********************************************************************

   call calc_and_print_cres(RN) 

end subroutine post_process
!**********************************************************************
end module postprocess
!**********************************************************************
! EOF



