
!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland). 
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



! module longdisp 
! provides a single public routine to sample  
!   CDF for travel time based on longitudinal dispersion 
! solves CDF(tau; taubar,la) = R for tau 
!    where R is random number (0,1]
!    la is length/dispersivity 
!    taubar is average travel time for segment  
! method for inverting the CDF is newton raphson 
!    with analytical representation for derivative 
! calculation starts at point of largest gradient to improve stability   
! module originally developed for tdrw version 1.0 
!  Scott Painter
!  Feb 2006 

 module longdisp 
 use precision 

 implicit none 

 contains 

    function sample_ld(la,taubar) result(rtsec)

!sp SCR 716 Item 2 v3.2.1  
!    integer, parameter :: maxit=30 
    integer, parameter :: maxit=50 
!end SCR 716 Item 2 v3.2.1   
    integer :: j 
!sp SCR 716 Item 2 v3.2.1  
!    real(kind=dp), parameter :: xac = 1.0d-7
    real(kind=dp), parameter :: xac = 1.0d-7    
!end SCR 716 Item 2 v3.2.1  
    real(kind=dp) :: fl,f ,rtsec ,R,la,taubar
    real(kind=dp) :: x1,x2,swap,dx ,xl ,df ,rtnewt 


    ! get a random number 
    call random_number(R)

    ! initial guess is at point of largest gradient 
    rtnewt=(sqrt(9.0d0+la*la)-3.0d0)/la 

    ! secent method 
    do j=1,maxit 
      f=func(rtnewt) 
      df=d_ogatabanks(rtnewt) 
      if( df .eq. 0.0d0) then 
        print *, 'Failed in sample_ld' 
        print *, f, df, rtnewt
        stop 
      end if 
      dx=f/df 
      rtnewt=rtnewt-dx 
      rtnewt = max(rtnewt,1.0d-9) 
      if(abs(dx) .lt. xac) then 
        rtsec = rtnewt*taubar 
        return 
      end if 
    end do  
    print *, 'maxit exceeded in sample_ld'
    rtsec = rtnewt*taubar

    contains 

    function func(tt) result(f) 
    ! this is the function whose root is sought 
    !  CDF(tt) = R 
    
     real(kind=dp) :: f ,tt 
 
      if( tt .le. 0.0d0) then 
        f = - R 
      else 
        f = ogatabanks(tt)-R 
      end if 

    end function func 

    function ogatabanks(tt) result(ob) 
    ! travel time CDF 
    ! is ogata-banks solution for cumulative breakthrough 
    !   with dirac-delta input 
    ! equivalent to ogata-banks solution for instantaneous  
    !   breakthrough and heaviside func as input 
 
     real(kind=dp) :: u1,u2,u3, ob ,tt 

     u1 = erfc( sqrt(la)/2.0d0 * (1.0d0-tt)/sqrt(tt) ) 
     u2 = erfc( sqrt(la)/2.0d0 * (1.0d0+tt)/sqrt(tt) )  
 
     u3 = min(la,500.0d0) 
     u3 = exp(u3) 
 
     ob = (u1 + u3*u2)/2.0d0 

    end function ogatabanks 

    function d_ogatabanks(tt) result(dob) 
    ! derivative of ogata-banks 
    
    real(kind=dp) :: foo, tt, dob ,eta 
    real(kind=dp), parameter :: pi=3.14159

    eta = la 

    foo =  eta*(tt-1.0d0)*(tt-1.0d0)/(4.0d0*tt) 
    dob = exp(-foo)*sqrt(eta)/(2.0d0 * sqrt(pi) * tt*sqrt(tt))  

    end function d_ogatabanks 

    end function sample_ld

end module longdisp 

