# MARFA #

This is the landing page for MARFA.  Please go to the [wiki][wikilink] to see a summary of the capabilities and different versions available. A version of the User's Manual is available [here][docslink].

[wikilink]: https://bitbucket.org/marfa_account/marfa-5.0/wiki/Home
[docslink]: https://bitbucket.org/marfa_account/marfa-docs_git/downloads/marfa_user_manual_v5.0_rn1.pdf

Please contact Amphos 21 (marfa@amphos21.com) if you have any requests.

## LICENSE

MARFA is distributed under the terms of the BSD 3-clause license.