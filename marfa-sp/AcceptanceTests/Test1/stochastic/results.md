!results.md
!file created on: 20200201 at 151514.279
!MARFA Developer Version
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 1000000
!aseed=          844      270080                                                        
     1000000
           4
 Am241        1.6000000000000001E-003     1000000
 Np237        3.2000000000000001E-007           0
 U233         4.4000000000000002E-006           0
 Th229        9.5000000000000005E-005           0
