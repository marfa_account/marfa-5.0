!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland)
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

module preprocess

! *********************************************************************
! File Name        : preprocess.f95
! Program Name     : marfa
! Developed for    : SKB and POSIVA
! Revision History : Version 3.1  December 2006   James Mancillas  
!                  :  Version 3.1 September 2007 Scott Painter  
!                  :    minor bug fix in read/write statements 
!                  :  Version     3.2 Oct 2007 James Mancillas
!                  :              update for epoch dependent ret parameters
!                  :  Version     3.2.1 Apr 2008 James Mancillas
!                  :              update for file format change "rocktypes.dat"
! SRD Section      : Section 3
! *********************************************************************

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!   NAME: PREPRO
!
!   PURPOSE:  collect and sort trajectory data into catagorized segment
!             data
!             1) open trajectory data files
!             2) determine number of segments
!             3) sort into segment catagories and save to file
!                 a. segment_cat_1 ... segment_cat_N
!
!   INPUT:    Number of catagories
!             Location of trajectory files
!            
!   SUBROUTINES:  set_filenames
!                 open_files
!                 get_data
!                 init_data             
!                     scan_file
!                 sort_data
!                     qsort_2d
!                 save_data
!                     cat_vel
!                 segment_prepro
!                 each_hydrofacies
!                 each_direction
!                 catstrunderscore
!                 catstr
!                 find_free_funit
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

USE qsort_module
USE precision
USE io
USE hydrofacies

public segment_prepro

contains

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! jm 3.2.1 change
! SUBROUTINE segment_prepro()        
SUBROUTINE segment_prepro(nelem)        
! end 3.2.1 change
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

IMPLICIT NONE

CHARACTER(len=132)                                  :: root_path
CHARACTER(len=132)                                  :: path
INTEGER                                             :: num_hydrofacies
! jm 3.2.1 change
INTEGER                                             :: nelem
! end 3.2.1 change
CHARACTER(len=132),dimension(:),pointer            :: hydrofacies_names
INTEGER,dimension(:),pointer                    :: num_directions
INTEGER,dimension(:),pointer                    :: num_vel_groups
LOGICAL,dimension(:),pointer                    :: ANALYZE_DATA        ! data analysis flag

CHARACTER(len=1)                                   :: slash
INTEGER                                            :: index1

! opens data files rocktypes.dat 
! collects hydrofacies names,file paths, num_hydrofacies, num_vel_groups and num_directions
! and attempts to determine which file system is in use
! unix slash ='/' or  windows slash = '\' 

! jm 3.2.1 change
!    call init_data(hydrofacies_names,root_path,num_hydrofacies, &
!              num_vel_groups,num_directions, ANALYZE_DATA, slash)
    call init_data(nelem,hydrofacies_names,root_path,num_hydrofacies, &
              num_vel_groups,num_directions, ANALYZE_DATA, slash)
!  end 3.2.1 change  
! scans through hydrofacies which should have data analyized


  do index1=1,num_hydrofacies
    if(ANALYZE_DATA(index1))then                       

             path=root_path                                !  creates path using proper 
             call catstr(path,hydrofacies_names(index1))   !  slash '/' or '\'
             call catstr(path,slash)                       !
             call each_hydrofacies(path,hydrofacies_names(index1),num_vel_groups(index1),num_directions(index1))
           
    end if
  end do

! deallocates arrays   

  deallocate(hydrofacies_names)
  deallocate(ANALYZE_DATA)
  deallocate(num_vel_groups)

  return
  end subroutine segment_prepro
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
SUBROUTINE each_hydrofacies(path,rootname,num_vel_groups,num_directions)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

   IMPLICIT NONE

   CHARACTER(len=132),intent(in)                    :: path
   CHARACTER(len=132),intent(in)                    :: rootname
   INTEGER,intent(in)                               :: num_vel_groups
   INTEGER,intent(in)                               :: num_directions

   CHARACTER(len=132)                               :: tempname
   CHARACTER(len=132)                               :: out_filename
   CHARACTER(len=2)                                 :: dir
   CHARACTER(len=2)                                 :: itr  

   LOGICAL                                         :: file_exists
   LOGICAL                                         :: ANALYZE
   INTEGER                                         :: index1,index2
   INTEGER                                         :: file_num_VG    
   INTEGER                                         :: direction
   INTEGER                                         :: funit

   file_exists=.true.

  do index1=1,num_directions
     ANALYZE=.true.


! create filename of output for this direction   path/_direction
     tempname=path
     write(dir,'(i2)') index1                          ! v3.1.1 sp  
     dir= adjustl(dir)                            !  
     call  catstrunderscore(tempname,dir)         !   path/_1


! inquire if output file exists  
! and if it does varify its contents

     inquire(file=tempname, exist=file_exists)

      if(file_exists)then
         funit=openread(tempname)
         read(funit,*) file_num_VG
         close(funit)                   
         if(file_num_VG .eq. num_vel_groups) ANALYZE=.false.
      else
         ANALYZE=.true.
      end if

! if analize=.true. check to see if at least one input file for that direction exists 
         
if(ANALYZE)then                       

     index2=1
     tempname=path
     call catstr(tempname,rootname)          !  constructs file names
     write(dir,'(i2)') index1                !  root_dir_itr V3.1.1 sp 
     dir= adjustl(dir)                       !  only checks to see if
     write(itr,'(i2)') index2                !  at least one data set V3.1.1 sp 
     itr= adjustl(itr)                       !  exists ::
                                             !
     call  catstrunderscore(tempname,dir)    !   path/root_dir_1 
     call  catstrunderscore(tempname,itr)    ! 

     inquire(file=tempname, exist=file_exists)

     if(file_exists)then


       call peek_and_preprocess(tempname)            ! examine format if in connectflow .ptv format
                                                     ! preprocess file into local format and overwrite
                                                     ! initial file
                                                     ! the data grad,phi, theta and vcpm need to be entered

       out_filename= path                            ! if a single file in a direction
                                                     ! exists then the function 'each_direction'
                                                     ! is called

       call each_direction(path,rootname,out_filename,num_vel_groups,index1)
     else 
       print*,"File  '",trim(tempname),"' was not found "
       stop
     end if

end if 

  end do

      return
      end subroutine each_hydrofacies
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
SUBROUTINE each_direction(path,in_rootname,out_filename,num_vel_groups,direction)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

IMPLICIT NONE
CHARACTER(len=132),intent(in)                 :: path
CHARACTER(len=132),intent(in)                 :: in_rootname
CHARACTER(len=132),intent(inout)              :: out_filename
INTEGER, intent(in)                          :: num_vel_groups
INTEGER, intent(in)                          :: direction

INTEGER                                      :: num_datasets
CHARACTER(len=132),dimension(:),pointer      :: in_filenames
INTEGER,dimension(:),pointer                 :: in_fileunits
INTEGER                                      :: out_fileunit
real(kind=dp),dimension(:,:),pointer         :: seg_data
real(kind=dp),dimension(4)                   :: h_data
INTEGER                                      :: num_seg
INTEGER                                      :: index

!  set_filenames => creates the namestrings for the input and output datafiles
!  and sets the ANALYZE_DATA flag, also returns number of data sets for this direction
    
      CALL set_filenames(num_datasets,in_rootname, direction,                 &
                  in_filenames,out_filename,path)   


!  open_files => opens hydofacies (input) files and vel_group (output) files
!  returns: file units for the input and output files -- in_fileunits ,out_fileunits 
     
      CALL open_files(num_datasets, in_filenames, out_filename, in_fileunits, &
                  out_fileunit)

! collect segment data from input files into a single array called data
! returns the segment data from the trajectory files in the array 'data'
! 'num_seg' is the number of segments recovered from trajectory files

      CALL  get_data(in_fileunits,seg_data,num_seg, h_data)

! orders the segment data using a simple qsort (mapping) of the segment velocities

      CALL sort_data(seg_data,num_seg)

! split data into velocity groups and save to file

      CALL save_data(seg_data,num_seg,num_vel_groups,out_fileunit,h_data)

! close files deallocate arrays

            close(out_fileunit)

      do index=1,num_datasets
            close(in_fileunits(index))
      end do

      deallocate(seg_data)
      deallocate(in_filenames)
      deallocate(in_fileunits)  
      return
      end subroutine each_direction          
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
  subroutine  set_filenames( num_datasets, in_rootname,    &
                  direction, in_filenames, out_filename,path)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
IMPLICIT   NONE

INTEGER, intent(inout)                                       :: num_datasets
INTEGER, intent(in)                                          :: direction
CHARACTER(len=132), intent(in)                               :: in_rootname
CHARACTER(len=132), intent(inout)                            :: out_filename
CHARACTER(len=132), intent(in)                               :: path
CHARACTER(len=132), dimension(:), pointer :: in_filenames


INTEGER                                        :: index1
CHARACTER(len=132)                             :: sub1,sub2
INTEGER                                        :: lengthsub1
CHARACTER(len=132)                             :: tempname
LOGICAL                                        :: file_exists

     file_exists=.true.

! construct a string   => path/in_rootname_direction_realizations
! and check to see if the file exists and the number of data sets
! 'num_datasets'

        index1=0
     do while(file_exists)      
        index1=index1+1
        sub1=path
        call catstr(sub1,in_rootname)
        write(sub2,*) direction
        sub2= adjustl(sub2)
        call catstrunderscore(sub1,sub2(:2))
        write(sub2,*) index1
        sub2= adjustl(sub2)
        call catstrunderscore(sub1,sub2(:2))
        inquire(file=sub1,exist=file_exists)
     end do

       num_datasets=index1-1


! create array of input filenames of the size 'num_datasets'

       allocate(in_filenames(num_datasets))

! populate the array with the filenames

      do index1=1,num_datasets
        sub1=path
        call catstr(sub1,in_rootname)
        write(sub2,*) direction
        sub2=adjustl(sub2)
        call catstrunderscore(sub1,sub2(:2))
        write(sub2,*) index1
        sub2=adjustl(sub2)
        call catstrunderscore(sub1,sub2(:2))
        in_filenames(index1)=sub1
      end do

! create output filename
  
       write(sub2,*) direction
       sub2=adjustl(sub2)
       lengthsub1=len_trim(out_filename)
       call  catstrunderscore(out_filename,sub2(:2))

! print to screen files to be accessed

       do index1=1,num_datasets
          print*,'Processing : ', trim( in_filenames(index1))
       end do

     return
     end subroutine set_filenames
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
SUBROUTINE open_files(num_datasets, in_filenames, out_filename,    &
                  in_fileunits, out_fileunit)         
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
IMPLICIT NONE

INTEGER, intent(in)                                          :: num_datasets
CHARACTER(len=132), dimension(:), intent(in)                 :: in_filenames
CHARACTER(len=132),intent(in)                                :: out_filename
INTEGER, dimension(:), pointer                               :: in_fileunits
INTEGER, intent(inout)                                       :: out_fileunit

LOGICAL                                                      :: file_exist
INTEGER                                                      :: index1
  file_exist=.true.

   allocate(in_fileunits(num_datasets))

! open trajectory data files
   
         do index1=1,num_datasets
             inquire(file=in_filenames(index1),exist=file_exist)
             if(file_exist) then
                in_fileunits(index1)=findunit()
                open(unit=in_fileunits(index1),file=in_filenames(index1))
             else
                write(6,*) "Trajectory file ", in_filenames(index1), "not found."
                write(6,*) "Exiting ...."
                STOP
             end if
        end do

! open velocity group data files

             out_fileunit=findunit()
             open(unit=out_fileunit,file=out_filename)

  return
  end subroutine open_files
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
SUBROUTINE find_free_unit(funit)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      IMPLICIT NONE

      INTEGER, intent(inout)         :: funit
      LOGICAL                        :: used

      used=.true.     
      funit=15
      do
         inquire(funit, opened=used)
         if(.not.used) return
         funit=funit+1
      end do

      return
      end subroutine find_free_unit
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
SUBROUTINE   get_data(in_fileunits,seg_data,num_seg,h_data)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

IMPLICIT   NONE

INTEGER, dimension(:),intent(in)                         :: in_fileunits
real(kind=dp),dimension(:,:), pointer                    :: seg_data   ! segment data
real(kind=dp),dimension(4),intent(inout)                 :: h_data

real(kind=dp),dimension(:,:),allocatable                 :: seg_data1,seg_data2  ! temp arrays
real(kind=dp)                                            :: count
INTEGER                                                  :: index1,index2
INTEGER                                                  :: num_files
INTEGER                                                  :: num_seg,last_num
INTEGER                                                  :: num_trajectories
LOGICAL                                                  :: FIRST_FILE


! initialize parameters

     num_files=size(in_fileunits)
     num_seg=0
     last_num=0
     count=0.0d0
     FIRST_FILE=.true.

! collect information from data files

    do index1=1,num_files                   ! loop through the trajectory data files 

      if(associated(seg_data))deallocate(seg_data)
      last_num=num_seg

      CALL scan_file(in_fileunits(index1),num_trajectories,num_seg,seg_data,h_data)
      count=count+1.0d0

      if(FIRST_FILE)then                    ! first set of data returned 
         num_seg=size(seg_data(:,1))

         allocate(seg_data1(num_seg,4))
         seg_data1(:,:)=seg_data 
         deallocate(seg_data)
         FIRST_FILE=.false.         
      else
         if (int(count/2.0d0).eq. count/2.0d0 ) then     ! accumulates the total data set into
            num_seg=last_num+size(seg_data(:,1))         ! a single dynamic array which is 
            allocate(seg_data2(num_seg,4))               ! resized after each new data set is  
            seg_data2(:last_num,:)=seg_data1(:,:)        ! returned.  this is a temp array 
            seg_data2(last_num+1:,:)=seg_data            ! named either seg_data1 or seg_data2
            deallocate(seg_data1)                        !
            deallocate(seg_data)                         !
                                                         !
          else                                           !
                                                         !
            num_seg=last_num+size(seg_data(:,1))         !
            allocate(seg_data1(num_seg,4))               !
            seg_data1(:last_num,:)=seg_data2(:,:)        !
            seg_data1(last_num+1:,:)=seg_data            !
            deallocate(seg_data2)                        !
            deallocate(seg_data)                         !
 
          end if
      end if
 
   end do

! copy data in temp arrays into output array 'data'

       if(allocated(seg_data2))then                      !  Takes the data saved in the temp
         num_seg=size(seg_data2(:,1))                    !  array and copies it into the returning
         allocate(seg_data(num_seg,4))                   !  array
         seg_data(:,:)=seg_data2(:,:)                    !     seg_data(:,:)
         deallocate(seg_data2)                           !
       else                                              !
         num_seg=size(seg_data1(:,1))                    !
         allocate(seg_data(num_seg,4))                   !
         seg_data(:,:)=seg_data1(:,:)                    !
         deallocate(seg_data1)                           ! 
       end if
      
     return
     end subroutine get_data
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
SUBROUTINE scan_file(funit,num_trajectories,num_segments,seg_data,h_data)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

IMPLICIT   NONE

INTEGER, intent(in)                                    :: funit
INTEGER,intent(inout)                                  :: num_trajectories
real(kind=dp),dimension(:,:), pointer                  :: seg_data
real(kind=dp),dimension(4), intent(inout)            :: h_data

INTEGER, dimension(:), allocatable                     :: array
INTEGER                                                :: counter1
INTEGER                                                :: counter2
INTEGER                                                :: int_val
INTEGER                                                :: index1,index2
INTEGER                                                :: num_segments
INTEGER                                                :: seg_num

real(kind=dp)                                        :: new_tau,new_beta,new_len
real(kind=dp)                                        :: old_tau,old_beta,old_len
CHARACTER(len=132)                                   :: string
CHARACTER(len=80)                                    :: aline,foo
CHARACTER(len=132)                                   :: filename
LOGICAL                                              :: NEW_TRAJECTORY

! scan through file first time to determine data locations
! and the number of trajectories and segments in data file

! initialize parameters

counter1=1
num_trajectories=0

! skim through file
! skip header lines 

    counter2=0
    do
     read(unit=funit,fmt='(a80)',end=120) aline
     foo=adjustl(aline)
     counter2=counter2+1
     if( foo(1:1) .ne. '!') exit
   end do
     

   backspace(unit=funit)
   counter2=counter2-1

      read(funit,*) h_data(1),h_data(2),h_data(3),h_data(4)  ! read hydro data grad,phi,theta,vcpm

      read(funit,*,END=101) num_trajectories                 ! Read number of trajectories 

  counter2=counter2+2

      allocate(array(num_trajectories+100))
      array=0

      array(counter1)=counter2

      99  continue
          counter1=counter1+1                                  
      100 continue
          counter2=counter2+1       
     
      read(funit,*,END=101) string             !   Skim through file finding 'END'              
                                               !   locations
      string=  adjustl(string)

        ! v3.1.1 sp fixed error below 
        int_val=ichar(string(1:1))                 !   ASCII value of first non space character

! examine first character : two possibilities :: number , non-number
! may need to expand this to the first non space character

       if( int_val .gt. 57 .or. int_val .lt. 48 ) then           ! not a number
          array(counter1)=counter2
          goto 99
       else
          goto 100                                               !  a number
       end if

      101 continue

! ARRAY contains the locations of non numeric data starting with line num_header_lines+1
! ARRAY(n+1)-ARRAY(n)-1=number of segments in the trajectory n

! determine number of segments

      num_segments=0
      do index1=1,num_trajectories
        num_segments=num_segments + ( array(index1+1)-array(index1)-1 )
      end do

! second pass through the data file => populates the array data
! this array contains tau,beta,len, vel_next

! initialize parameters
     allocate(seg_data(num_segments-num_trajectories,4))  ! this size is set to account for 
     rewind(funit)                                    ! neglecting the last segment in each trajectory
     index1=1
     index2=1
     seg_num=1
     NEW_TRAJECTORY=.true.

!  scan through file using known locations of trajectory terminations to direct the
!  collection of individual segment data

     do 
        if((index2 .gt. array(index1)).and.(index2 .lt. array(index1+1)))then
           read(funit,*,END=102) new_tau, new_beta, new_len
             if(NEW_TRAJECTORY)then
                NEW_TRAJECTORY=.false.
                old_tau=new_tau
                old_beta=new_beta
                old_len=new_len
             else
                seg_data(seg_num,1)=old_tau
                seg_data(seg_num,2)=old_beta
                seg_data(seg_num,3)=old_len
                seg_data(seg_num,4)=new_len/new_tau

                old_tau=new_tau
                old_beta=new_beta
                old_len=new_len

                seg_num=seg_num+1
             end if
           index2=index2+1
        else 
             if(index2.eq.array(index1+1))then
               read(funit,*,END=102) string
               index2=index2+1
               index1=index1+1
               NEW_TRAJECTORY=.true.
             else
               read(funit,*,END=102) string
               index2=index2+1
               NEW_TRAJECTORY=.true.
             end if
        end if
     end do

     102 continue

      deallocate(array)
     
      return

   120 inquire(funit,name=filename)
       print*, 'End of File ',trim(filename)
       stop

     end subroutine scan_file
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
SUBROUTINE sort_data(seg_data,num_seg)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

IMPLICIT NONE

real(kind=dp), dimension(:,:), intent(inout)     :: seg_data
INTEGER,intent(in)                               :: num_seg

real(kind=dp), dimension(:,:), allocatable       :: vel_array
real(kind=dp), dimension(:,:), allocatable       :: data_sort
INTEGER                                          :: index1,index2
INTEGER                                          :: sort_column

! initialize parameters

     sort_column=1                   ! velocity column
     allocate(vel_array(num_seg,2))  ! parrallel array
     allocate(data_sort(num_seg,4))  ! temp array

! set up a parallel array containing velocities and positions
 
     do index1=1,num_seg
        vel_array(index1,1)=seg_data(index1,3)/seg_data(index1,1)
        vel_array(index1,2)=index1
     end do

! sort parrallel array using velocities

     CALL qsort_2d(vel_array,sort_column)

! map the order of the data set to the order of the parrallel array
! using a temp array
                          
     do index1=1,num_seg
           index2=int(vel_array(index1,2))
        data_sort(index1,1)= seg_data(index2,1 )
        data_sort(index1,2)= seg_data(index2,2 )
        data_sort(index1,3)= seg_data(index2,3 )
        data_sort(index1,4)= seg_data(index2,4 )
     end do

! overwrite the original data set with the sorted data set
 
     seg_data(:,:)=data_sort(:,:) 

     deallocate(vel_array)
     deallocate(data_sort)
     
     return
     end subroutine sort_data
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
SUBROUTINE save_data(seg_data,num_seg,num_vel_groups,out_fileunit,h_data)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

IMPLICIT  NONE

real(kind=dp),dimension(:,:),intent(inout)           :: seg_data
INTEGER, intent(in)                                  :: num_seg
INTEGER, intent(in)                                  :: num_vel_groups
INTEGER, intent(in)                                  :: out_fileunit
real(kind=dp),dimension(4),intent(in)                :: h_data

CHARACTER(len=132)                                   :: file_name
real(kind=dp),dimension(:),allocatable               :: vgroup_values
INTEGER                                              :: num_each
INTEGER                                              :: index1,index2

! initializes parameters

   allocate(vgroup_values(num_vel_groups))
   num_each=num_seg/num_vel_groups            ! determines how many segments will be in each group

! sets the velocity class discrimination values for each group

     do index1=1,num_vel_groups
         index2=index1*num_each
         vgroup_values(index1)=seg_data(index2,3)/seg_data(index2,1)
     end do
         vgroup_values(num_vel_groups)=seg_data(num_seg,3)/seg_data(num_seg,1)  ! <= this ensures that the 
                                                                                !    last entry is captured
                                                                                !    which might not always occur
! quick print for user information                                              !    because of integer math
    inquire(unit=out_fileunit,name=file_name)
    print*, 'Creating New file ',trim(file_name)

! stores  each vel group into a separate file
!    file=> header info, number of segments, more info, data ....

         call writeheader(out_fileunit)

         write(out_fileunit,*) num_vel_groups
         write(out_fileunit,*) h_data(1),h_data(2),h_data(3),h_data(4)

     do index2=0,num_vel_groups-1
         write(out_fileunit,*) num_each 
       do index1=(num_each*(index2)+ 1) ,num_each*(index2+1)

         write(out_fileunit,FMT=10001) seg_data(index1,1),seg_data(index1,2), &
                          seg_data(index1,3),cat_vel(vgroup_values,seg_data(index1,4))

       end do
     end do

10001 FORMAT( E12.4, E12.4 ,E12.4, I4)

    deallocate(vgroup_values)

    return
    end subroutine save_data
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
FUNCTION cat_vel(vgroup_values,observed) result(cat)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
IMPLICIT NONE

real(kind=dp),dimension(:),intent(in)            :: vgroup_values
real(kind=dp),intent(in)                         :: observed

INTEGER                                          :: index1, num_cat
INTEGER                                          :: cat

num_cat=size(vgroup_values)

         do index1=1,num_cat
             if(observed.le.vgroup_values(index1))then
                cat=index1
                return
             end if
         end do
  
    print*,'Warning error in categorizing function'
    cat=num_cat 
  
    return
    end function cat_vel
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
SUBROUTINE catstrunderscore(string1,string2)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
IMPLICIT  NONE
CHARACTER(len=*),intent(inout)                       :: string1
CHARACTER(len=*),intent(in)                          :: string2

CHARACTER(len=2)                                     :: underscore
INTEGER                                              :: len1

   write(underscore,*)'_'
   underscore=adjustl(underscore)
  
        len1=len_trim(string1)
        string1=string1(:len1)//underscore
        len1=len_trim(string1)
        string1=string1(:len1)//string2

   return
   end subroutine catstrunderscore
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
SUBROUTINE catstr(string1,string2)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
IMPLICIT  NONE
CHARACTER(len=*),intent(inout)                       :: string1
CHARACTER(len=*),intent(in)                          :: string2

INTEGER                                              :: len1
INTEGER                                              :: len2

    len1=len_trim(string1)
    string1=string1(:len1)//string2

    return
    end subroutine catstr
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!  jm 3.2.1 change
!SUBROUTINE init_data(hydrofacies_names,root_path,num_hydrofacies, &
!              num_vel_groups, num_directions,ANALYZE_DATA,slash)
SUBROUTINE init_data(nelem,hydrofacies_names,root_path,num_hydrofacies, &
              num_vel_groups, num_directions,ANALYZE_DATA,slash)
! end 3.2.1 change
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
IMPLICIT   NONE

CHARACTER(len=132),dimension(:),pointer  :: hydrofacies_names
CHARACTER(len=132),intent(inout)                            :: root_path
INTEGER, intent(inout)                                     :: num_hydrofacies
INTEGER, dimension(:),pointer          :: num_vel_groups
INTEGER, dimension(:),pointer          :: num_directions
LOGICAL, dimension(:),pointer           :: ANALYZE_DATA
CHARACTER(len=1), intent(inout)                            :: slash


INTEGER                                                    :: funit,index1,i
CHARACTER(len=132)                                         :: aline,foo
CHARACTER(len=16)                                          :: keywd 

INTEGER                                                    :: nelem


! read data contained in rocktypes.dat  


       funit=openread('rocktypes.dat')

       read(funit,'(A)') root_path              ! location of hydrofacies data directories - not needed
       read(funit,'(A)') root_path              ! location of subgrid data

                                                ! determine if windows or unix

       i=len_trim(root_path)
     if( scan(root_path,'/') .gt. 0) then
       if(root_path(i:i) .ne. '/') root_path(i+1:i+1)='/'    ! unix
       slash='/'    !unix                       !
     else
       if(root_path(i:i) .ne. '\') root_path(i+1:i+1)='\'    ! windows
       slash='\'    ! windows                   !
     endif

       read(funit,*) num_hydrofacies            ! number of hydrofacies

       if(num_hydrofacies.lt.1)then             ! skips rest of program if
         ANALYZE_DATA=.false.                       ! number of hydrofacies =0
         return                                 !
       end if                                   !

       allocate(hydrofacies_names(num_hydrofacies))        ! array holding hydrofacies names
       allocate(num_vel_groups(num_hydrofacies))           ! array holding number of velocity groups for each hydrofacies
       allocate(ANALYZE_DATA(num_hydrofacies))             ! array holding flag for new data analysis
       allocate(num_directions(num_hydrofacies))           ! array with values of the number of directions in each hydrofacites

       do index1=1,num_hydrofacies
          read(funit,*,END=120) hydrofacies_names(index1)                              ! gets hydrofacies name

          num_directions(index1)=0
          num_vel_groups(index1)=0
          ANALYZE_DATA(index1)=.false.


          do 
            read(funit,'(A)',End=120) aline
            foo=adjustl(aline)
            if(foo(1:3) .eq. 'TAB' .or. foo(1:2) .eq. 'MD') exit 
            if(foo(1:2) .eq. 'LD'  .or. foo(1:2) .eq. 'ES') exit 
            
            if( foo(1:3) .eq. 'STO') then 
               read(aline,*,END=120)keywd,num_directions(index1),num_vel_groups(index1)
               ANALYZE_DATA(index1)=.true.
            else if ( foo(1:4) .ne. 'DISP' .and. foo(1:3) .ne. 'STA') then 
              print *, 'error in prepro ', foo 
            end if 

          end do 

          read(funit, '(A)', end=120) aline 
          if( foo(1:3) .ne. 'TAB') then 
           do i=1,nelem 
            read(funit, '(A)', end=120) aline 
           end do 
          end if 

      end do

       close(funit)
       
      return
      120 print *, 'error in prepro.f' 
      stop 
    end  subroutine init_data
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
SUBROUTINE  peek_and_preprocess(tempname)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
CHARACTER(len=132), intent(in)                   :: tempname         ! examines the input file and looks at
INTEGER                                          :: funit            ! the first charater of the data file
                                                                     ! if it is non-numeric it is assumed the file is in a
                                                                     ! connect flow .ptv format.
Integer                                          :: value
CHARACTER(len=1)                                 :: aline

     funit=openread(tempname)                           ! open file and skip header
     read(funit,'(A1)') aline                           ! read first character of first line
     close(funit)   

     value=iachar(aline)                                ! integer ASCII value of letter
     
     if(value .lt. 48 .or. value .gt. 57)then           ! if non-numeric
          call fileconvert(tempname)                    ! creates 'temp' file 
     end if  

return 

end subroutine peek_and_preprocess
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
SUBROUTINE  fileconvert(tempname) 
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
IMPLICIT NONE
CHARACTER(len=132)                                  :: tempname
CHARACTER(len=132)                                  :: tempname2
CHARACTER(len=132)                                  :: tempname3
CHARACTER(len=4)                                    :: string1,string2
CHARACTER(len=4)                                    :: STEPS, PART 
CHARACTER(len=180)                                  :: aline
INTEGER                                             :: funit1,funit2,funit3  
INTEGER                                             :: num_traj
INTEGER                                             :: index1,index2,index3
REAL(kind=dp),dimension(3)                          :: start, ecoord
REAL(kind=dp)                                       :: tau_sum, beta_sum, len_sum
REAL(kind=dp)                                       :: tau, beta, len
REAL(kind=dp)                                       :: aperture, aperture_old
INTEGER, dimension(:), allocatable                  :: num_segments
REAL(kind=dp)                                       :: temp_var,dtemp
INTEGER                                             :: ivalid,itemp, elements

    string1='.tmp'
    string2='.org'

    tempname2= trim(tempname)//string1
    tempname3= trim(tempname)//string2

    funit1=openread(tempname)

    funit2=findunit()
    open(unit=funit2, STATUS='scratch')

    funit3=findunit()
    open(unit=funit3,file=tempname3)
   
! determine number of trajectories

     do index1=1,2              ! skips connect flow intro- data
       read(funit1,*) aline     !
     end do                     !

     do 
       read(funit1,'(a)',end=88) aline
       read(aline,*) PART 
       if(PART .eq. 'PART')then
         read(aline(47:47),*) ivalid
         if(ivalid .eq. 0) num_traj = num_traj+1
       end if        
     end do

 88  continue

     allocate(num_segments(num_traj))

     rewind(funit1)


! determine the tau, beta, len for each segment of each trajectory

     do index1=1,2              ! skips connect flow intro- data
       read(funit1,*) aline     !
     end do                     !

   do index1=1,num_traj

       read(funit1,'(a)',end=5555) aline
7788 continue
       read(aline,*) PART

    if(PART .eq. 'PART')then ! begin new trajectory

        if(aline(47:47) .ne. '0')then
          do 
            read(funit1,'(a)',end=5555) aline
            if(aline(1:1) .eq. 'P') goto 7788
          end do
        end if
            
           read(funit1,'(a)') aline
           read(aline,*) start
           read(funit1,'(a)') aline
           read(aline,*) STEPS, elements 
  
           num_segments(index1)=0
           beta_sum=0.0
           len_sum=0.0
           tau_sum=0.0

             read(funit1,*,end=5555) tau, ecoord, aperture,temp_var,temp_var,beta,temp_var,temp_var
             backspace(funit1)
             aperture_old=aperture 

          do index2=1,elements 
             read(funit1,*,end=5555) tau, ecoord, aperture,temp_var,temp_var,beta,temp_var,temp_var

               if(aperture.eq.aperture_old)then
                 len_sum  = sqrt(sum((start - ecoord)**2)) + len_sum
                 beta_sum = beta + beta_sum
                 tau_sum  = tau + tau_sum
                 start=ecoord

               else
                 num_segments(index1) = num_segments(index1) + 1
                 write(funit2,'(E12.6, 4x, E12.6,4x ,E12.6)') tau_sum, beta_sum, len_sum

                   aperture_old = aperture
                   beta_sum = beta
                   len_sum = sqrt(sum((start-ecoord)**2))
                   tau_sum = tau 
                   start = ecoord 
               end if

          end do

         write(funit2,'(E12.6, 4x, E12.6,4x ,E12.6)') tau_sum, beta_sum, len_sum 

       else
       goto 7788
       end if

   end do

5555 continue

   rewind(funit1)
   rewind(funit2)

! copy original file to file.org

      do
         read(funit1,'(a)',end=6666) aline
         write(funit3,'(a)') trim(aline)
      end do

6666 continue
     close(funit3)
     close(funit1)

! overwrite initial file with new data format

      funit1=findunit()
      open(unit=funit1,file=tempname)
      call skipheader(funit2)     

      call writeheader(funit1)
      write(funit1,'(a)') "! imposed gradient and resulting vcpm are placeholders"
      write(funit1,'(a)') "0.001  0.0 0.0 1.0"
      write(funit1,*) num_traj

      do index1=1,num_traj
        do index2=1,num_segments(index1)+1
          read(funit2,'(a)',end=7777) aline
          write(funit1,'(a)') trim(aline)
        end do
        write(funit1,'(a)') "END"
      end do

7777 continue

     close(funit1)
     close(funit2)

! remove temp file

 
return

end subroutine fileconvert
!**********************************************************************
function num_flow_periods(filename) result(nepoch)
! V3.2 function added
CHARACTER(len=180)                                    :: aline
CHARACTER(len=12)                                     :: string2="FLOW CHANGES"
CHARACTER(len=*)                                      :: filename
INTEGER                                               :: nm1, i1
INTEGER                                               :: nepoch,iunit

     iunit= openread(filename)

     nm1=0

     do
     read(iunit,'(a80)',end=150) aline
      i1=index(aline,string2)
      if( i1 .ne. 0) then ! usr has supplied a list of flow changes
        read(aline(i1+12:),*,end= 150)  nm1
        exit
      end if
     end do

  150 continue

     nepoch =nm1+1     ! number of constant velocity periods

     close(iunit)

     return

end function num_flow_periods
end module preprocess
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! EOF

