!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland). 
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


! *********************************************************************
! File Name        : retention_ES.f95
! Program Name     : marfa  
! Developed for    : SKB and POSIVA  
! Revision History : tdrw version 1  March 2006  
!                  :  marfa 3.1beta June 2006 
!                  :  marfa 3.1     Dec  2006
!                  :  marfa 3.2     Oct  2007
!                  :         Updated to include change in ret parameters during flow epochs
!                  :  marfa 3.2.1  Apr 2008 James Mancillas
!                  :         Update for format change to file "rocktypes.dat" 
!                  :  marfa 3.2.5 Oct 2013 Scott Painter 
!                  :         Removed allocatable components from derived types 
!                  :         changed rocktypes input file to read physical parameters 
! SRD Section      : Section 3 item 4 
! *********************************************************************


! module retention_ES_class 
! This module is the equilibrium sorption retention model  
! Three public routines and one public structure are provided. 
! Public routines are: 
!    init_retention_ES  a global initialization routine 
!    retention_ES_   a constructor for the class 
!    sample_rt_ES    returns a sample from the retention time distribution 

module retention_ES_class 

  use precision 
  implicit none 
  private 

  public sample_rt_ES
  public init_retention_ES 
  public retention_ES_  
  public retention_ES 

  type retention_ES
     integer :: nstates 
     real(kind=dp), dimension(:,:), pointer :: kd
     real(kind=dp), dimension(:,:), pointer :: kcprime
     real(kind=dp), dimension(:), pointer :: kdc
     character(len=10), dimension(:), pointer  :: statelabel
  end type retention_ES 

  contains 

  subroutine init_retention_ES (datadir) 
     character(len=*) :: datadir 
  end subroutine init_retention_ES 

!***********************************************************************
    subroutine retention_ES_(aES, iunit,kdunit, nelem ) 
!  **********************************************************************

     type(retention_ES), pointer :: aES  
     integer :: nelem,  iunit , kdunit
     integer :: ielem, istate  
     integer :: nstates 
     integer :: e 
     real(kind=dp) :: rhob !bulk density, kg/m3  
     real(kind=dp) :: mc ! colloid mass density kg/m3 
     real(kind=dp), dimension(:), allocatable :: por !porosity 
     character(len=15) :: delimiter
     character(len=80) :: aline 


     allocate( por(nelem) ) 
     allocate(aES) 
    
     !read physical parameters and calculate kd 
     read(iunit,*) rhob, mc ! element-independent parameters 
      do ielem=1,nelem 
        read(iunit,'(a80)') aline 
        read(aline, *, IOSTAT=e) por(ielem)  !time indepenent  
      end do  

     ! check for delimiter 
!     read(kdunit,*) delimiter  
!     if( trim(adjustl(delimiter)) .ne. 'kdbins' ) then 
!       print *, ' error in rocktypes.dat' 
!       print *, ' kdbins delimiter string not found' 
!     end if 
         
     ! get kd and time of kd change 
     read(kdunit,*) nstates  
     aES%nstates=nstates 
     if(nstates .lt. 1) then 
       print *, 'problem in kdfile. nstates=' , nstates 
       stop 
     end if 

     allocate(aES%kd(nelem,nstates))  
     allocate(aES%kcprime(nelem,nstates))  
     allocate(aES%statelabel(nstates))  
     allocate(aES%kdc(nstates))  
     do istate=1,nstates 
      read(kdunit,*) aES%statelabel(istate) 
      read(kdunit,*) aES%kdc(istate) ! this is entered as Retardation 
      do ielem=1,nelem 
      read(kdunit,*) aES%kd(ielem, istate), aES%kcprime(ielem, istate) 
      aES%kd(ielem,istate)=rhob*aES%kd(ielem,istate)/por(ielem) ! dimensionless Kd  
      aES%kcprime(ielem,istate) = aES%kcprime(ielem,istate)*mc !dimensionless Kc
      end do 
     end do 

     if( any(aES%kd .lt. 0.0d0) ) then 
       print *, 'dimensionless Kd cannot be negative ' 
       stop 
     end if 

     deallocate(por) 



    end subroutine retention_ES_
!***********************************************************************
    function sample_rt_ES(aES,elem,beta,r, istate)  result(rt) 
!***********************************************************************

      type(retention_ES) :: aES  
      integer :: elem,i,istate
      real(kind=dp) :: rt , r ,beta 

! need to sampel kd randomly if this option is desired 
! currently set for deterministic kd 
!  the variable r allows for sampling, if desired 



      !rt=beta * aES%kd(elem,istate)/(1.0d0 + aES%kcprime(elem,istate) ) 
      ! kdc is really Rc 
      rt=beta * (aES%kd(elem,istate) + aES%kdc(istate)*aES%kcprime(elem,istate)) & 
                 /(1.0d0 + aES%kcprime(elem,istate) ) 
      
    end function sample_rt_ES
!***********************************************************************
end module retention_ES_class 
