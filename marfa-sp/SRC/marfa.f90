!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (FINLAND).
!All rights reserved. 

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

! *********************************************************************************
! Program Name   :      marfa  
! Developer      :      Scott Painter 
! Developer Phone:      +1 210 722 9739
! Version Number :      developer 
! Client         :      SKB and POSIVA  
! Revision History:     Version 3.1 December 2006 
!                       Version 3.2 November 2007  
!                       Version 3.2.1 April 2008 
!                       Version 3.2.2 November 2009  
!                       Version 3.2.3 August 2011 
!                       Version 3.2.4 March 2013 
!                       Amphos branch October 2014 
!                       Version 3.2.5 - merge previous two versions  
! *********************************************************************************

!  this code solves for radionuclide transport along a one-dimensional pathway 
!  advection, longitudinal dispersion, retention processes, 
!  decay, ingrowth, and time-dependent flow are accounted for 
!  method is by an extended time-domain random walk algorithm 
!  Painter et al., International High Level Radioactive Waste Management Conference 2006 

!  Version 3.2 allows for changes in flow velocity including flow reversal 
!  also allows for changes in sorption properties 

! Version 3.2.2 is a minor upgrade that allows user to specify a channeling factor 
!   to reduce beta due to in-plane channeling 

! Version 3.2.3 removed SwRI code and added new capability to read retention time distributon from a file 
!               also minor bug fixes and capabilty to split after decay 

! Version 3.2.4 minor revisions: 
!       capability to start particles anywhere on trajectory 
!       option to specify number of particles on command line - overrides source.dat file 
!       more digits in output 
!       change handling of flow change to be different for ES and other retention models


!Developed by: Scott Painter and James Mancillas   


program marfa 
 

  use precision 
  use io 
  use nuclide_class 
  use longdisp 
  use hydrofacies
  use streamline 
  use source 
  use particle_class 
  use stack 
  use postprocess
  use preprocess  

  implicit none  
 
  logical :: active , last, runtransport, rescalc , asciiflag
  integer :: eflag 
 

  character(len=10) :: srctag 
  character(len=10) :: command
  character(len=80) :: dumstring  

  integer :: trjtag

  integer :: i ,nn ,nelem ,next ,iunit, funit, ival ,cunit
  integer, dimension(1) :: ipos 
  integer, dimension(8) :: idate ! V3.2.3 
  integer, dimension(:), allocatable :: aseed !4amphos 
  integer :: isplits,nsplits !V3.2.3 
  integer :: seedsize !V3.2.3 
  integer(kind=npk) :: ipart, npart !V3.2.5 

  integer, dimension(:), allocatable :: actualsource 

  real(kind=dp) :: tau ,beta, dt,rt ,ret_time, r, r1, deltaT 
  real(kind=dp) :: seglen , s
  real(kind=dp) :: ogparm, taubar, betabar , taunew
  real(kind=dp) :: frac , disp 
  real(kind=dp) :: wfac 

  type(particle), target :: crnt_particle 
  
  ! aliases for particle components 
  integer, pointer :: crnt_nuclide , crnt_elem
  real(kind=dp), pointer  :: time  , weight 
  real(kind=dp), pointer :: x

  ! list of nuclides, all chains  
  type(nuclide), dimension(:), allocatable, target :: nuclidelist 

  logical :: file_exists 

#ifdef ASCII_FILE 
    asciiflag = .true.  
#else 
    asciiflag = .false. 
#endif 

! *********************************************************************************

  ! print some information to the screen QA requirement 
  call writeheader(6)

  
!  check command line arguments
!  sp changed below 12/20/2006

  runtransport=.true.
  call getarg(1,command) 
  if( len_trim(command) .gt. 0)  then 
    if(command(1:3) .eq. 'ppo' .or. command(1:3) .eq. 'PPO') then 
        runtransport=.false.
    else 
        read(command,*,IOSTAT=eflag) npart  
        if(eflag .ne. 0) then 
           print *, 'please enter number of particles on command line' 
           stop 
        end if  
    end if 
  else 
    print *, 'must have something on the command line' 
    print *, ' number of particles or the PPO option' 
    print *, ' see users manual' 
    print *, 'stopping' 
    stop 
  end if
   
  if(runtransport)then

  ! load the nuclide and element list (local procedure) 
  call get_nuclides
  if( nelem .gt. 0 .and. nn .gt. 0 ) then 
    print *, "element and nuclide lists OK"
    print *, nn, " nuclides " 
    print *, nelem, " elements "  
  end if 
  allocate(actualsource(nn)) 
  actualsource = 0 


! jm 3.2.1 change
!   call segment_prepro()                    ! preprocess the stochastic Hydrofacies data !
   call segment_prepro(nelem)                    ! preprocess the stochastic Hydrofacies data !
! end 3.2.1 change
  
  call init_hf(nelem)                             ! load the hydrofacies 
  print *, "initialized the rocktypes module" 
  print *, size(hfset), " rock types found" 
 

  call init_streamline(nelem)               ! load velocity fields 
                                            !  and initialize the retention models 

  call init_source(nuclidelist)             ! load any source information 
                                            !  also sets the public variable npart 


  !v3.2.5 option to use seed from file
  !and update yet again the use of time and date to get seed
  call random_seed(SIZE=seedsize)
  allocate(aseed(seedsize))
  call random_seed(GET=aseed)
  inquire(file="seedfile.dat", exist=file_exists)
  if( file_exists ) then
    iunit=openread("seedfile.dat")
    idate=0
    aseed=0
     read(iunit, *) idate(1:2)  
     aseed(1:seedsize:2)=idate(1) 
     aseed(2:seedsize:2)=idate(2) 
    close(iunit)
  else
    call date_and_time(VALUES=idate)          ! get the time of day
    aseed(2:seedsize:2)=idate(8)*idate(7)*idate(6)
    aseed(1:seedsize:2)=idate(8)
!    funit=findunit()
!    open(unit=funit, file="seedfile.dat")
!    write(funit, *) aseed
!    close(funit) 
  end if
  call random_seed(PUT=aseed)
  call random_number(r)


  ! now open file for the actual results 
  funit=findunit()
  if(asciiflag) then 
   open(unit=funit, file = "results.dat", status="replace")
  else 
   open(unit=funit, form='unformatted', file='results.dat', status="replace") 
  end if 

  ! and for the concentration data 
  cunit=findunit()                          
  open(unit=cunit, form='unformatted', file = "concresults.dat", status="replace")   
  
  print *, "launching ", npart, " particles " 

! *********************************************************************************
! loop over all particles 
! *********************************************************************************
  do ipart = 1,npart 

  
   call sample_source(crnt_particle,srctag,trjtag,nn,npart) 
   call activate_trajectory(trjtag)  ! establish the active trajectory  
   
   crnt_nuclide => crnt_particle%nuclide 
   actualsource(crnt_nuclide)=actualsource(crnt_nuclide)+1 
   crnt_particle%elem = nuclidelist(crnt_nuclide)%ielem 

   crnt_particle%splitable = .FALSE. ! only split on ingrowth 

   ! push on the subparticle stack 
   call add2queue(crnt_particle) 

   do while ( .not.emptyqueue )   ! while sub-particle stack is not empty 

   ! pop from the subparticle stack 
   crnt_particle = fromqueue()  
   time         => crnt_particle%time 
   weight       => crnt_particle%weight 
   x            => crnt_particle%x 
   crnt_nuclide => crnt_particle%nuclide 
   crnt_elem    => crnt_particle%elem  
   
  ! keep sampling segments until monitoring boundary is reached  
  ! this loop was extensively rewritten for V3.2 
  do 

   call advance_segment(crnt_particle, taubar, betabar, seglen, ogparm)  

    ! longitudional dispersion 
    if( ogparm .lt. 1000.) then      !  dispersion  
     tau = sample_ld(ogparm, taubar) 
    else                                  ! almost no dispersion  
     tau = taubar
    end if 
    beta = betabar * tau/taubar 

    ! adjust tau and beta due to diffusion into stagnant water 
    taunew = sample_sw(crnt_particle,tau) 
    beta = beta * taunew/tau 
    tau = taunew 

     call random_number(r) 

     ret_time = sample_rt(crnt_particle,beta,r,tau)  ! retention time   

     rt = tau + ret_time                         ! total residence time 


     dt = sample_dt(crnt_nuclide)                ! decay time  

     deltaT =epochbndr-time                        ! time to next change in flow

     frac = 1.0d0 

     do               ! loop here until segment is completed 


       if( rt .lt. dt .and. rt .lt. deltaT) exit    ! reaches end of segment 

        if( deltaT .lt. rt .and. deltaT .lt. dt) then  !flow change 

            rescalc = check_for_rescalc(crnt_particle) 
            if( rescalc ) then 
              write(unit=cunit) & 
               crnt_nuclide, trjtag, crnt_particle%iseg, time, time+deltaT, & 
                 weight*tau/rt , weight*(1.0-tau/rt), frac 
            end if 


            call random_number(r) 
            ret_time = sample_rt2(crnt_particle, deltaT,r)  ! time required to return to the fracture 
            


            dt = sample_dt(crnt_nuclide) 
            do                                     ! check for decay while in matrix and after flow change 
              if( dt .gt. ret_time) exit           ! advance along the chain if so  
              crnt_nuclide =  nuclidelist(crnt_nuclide)%next  
              crnt_particle%splitable = .TRUE. 
              if( crnt_nuclide .eq. 0) exit                     ! end of decay chain   
              crnt_elem = nuclidelist(crnt_nuclide)%ielem 
              dt = dt + sample_dt(crnt_nuclide)                
            end do  
            if( crnt_nuclide .eq. 0) exit                     ! end of decay chain exit do loop   

            call scale_segment(crnt_particle, time+deltaT+ret_time+1.0d-7,tau,beta,deltaT,rt)  

            time = time + deltaT+1.0d-7          ! update clock for time elapsed to flow change
            time = time + ret_time               ! update clock for time to return to fracture 
             
            call random_number(r) 
            rt =  tau + sample_rt(crnt_particle,beta,r,tau)  ! determine residence time for remainder of segment

         else   ! decay 

            rescalc = check_for_rescalc(crnt_particle) 
            if( rescalc ) then 
              write(unit=cunit) & 
               crnt_nuclide, trjtag, crnt_particle%iseg, time, time+dt, & 
                weight*tau/rt , weight*(1.0-tau/rt), frac 
            end if 

            time = time + dt  

            crnt_nuclide =  nuclidelist(crnt_nuclide)%next  
            crnt_particle%splitable = .TRUE. 
            if( crnt_nuclide .eq. 0) exit                     ! end of decay chain   
            crnt_elem = nuclidelist(crnt_nuclide)%ielem 

            frac=1.0d0-dt/rt                  ! fraction of full segment remaining  

            ret_time = sample_rt(crnt_particle,beta,r,tau) 
            rt = tau + ret_time                       ! res time for full segment 
            rt = rt*frac 
            
            tau=tau*frac 
            beta=beta*frac 


         end if  

         dt = sample_dt(crnt_nuclide)   ! new decay time  

         deltaT = epochbndr-time   
           
     end do             ! end of segment 

     ! if here then survives the segment 

     x = x + seglen  
     time = time + rt 


   ! exit if crossing monitoring location  or if end of chain 
   active = .true.

   rescalc = check_for_rescalc(crnt_particle) 
   if( rescalc .and. crnt_nuclide .ne. 0) then 
     write(unit=cunit) & 
      crnt_nuclide, trjtag, crnt_particle%iseg, time-rt, time, weight*tau/rt , weight*(1.0-tau/rt), frac 
   end if 
 
   last= check_for_termination(crnt_particle) 
   if( last .or. crnt_nuclide .eq. 0) active = .false.  
   if( .not.active ) exit 
   if( time .gt. 1.0d9) exit 

   ! check for split 
!   split = check_forsplit(crnt_particle) 
   nsplits=nuclidelist(crnt_nuclide)%splitatbirth 
   if( nsplits .gt. 0 .and. crnt_particle%splitable) then 
     weight = weight/float(nsplits+1) 
     crnt_particle%splitable = .FALSE. 
     do isplits=1,nsplits 
     call add2queue(crnt_particle) 
     end do 
   end if 


  end do  ! end of all segments 

    if( crnt_nuclide .gt. 0) then 
    if( nuclidelist(crnt_nuclide)%splitatbirth .lt. 0 ) then !russian roulette 
      call random_number(r)
      wfac=abs( nuclidelist(crnt_nuclide)%splitatbirth) 
      r=r*wfac 
      if( r .lt. 1.0d0) then 
       weight=weight*wfac 
      else 
       crnt_nuclide = 0
      end if 
    end if 
    end if 

  if( crnt_nuclide .ne. 0 .and. time .lt. 1.0d9) then 
      
    if( asciiflag) then  
      write(funit,'(e16.9,i3,2x,e12.5,2x,a10,i5)') & 
        time,crnt_nuclide, weight , srctag ,trjtag 
    else 
      write(funit) & 
        time,crnt_nuclide, weight , srctag ,trjtag 
    endif 
  end if 

  end do  ! end  of subparticle stack 
 

  end do   ! end of all particles 
  close(funit) 
  close(cunit) 

  ! write metadata 
  funit=findunit()                          ! open file for results metadata 
  open(unit=funit, file = "results.md")    ! and print some header information 
  write(dumstring,*) aseed(1:2) 
  call writeheader(funit,npart,dumstring) 
  write(funit,*) npart 
  write(funit,*) nn 
  do i=1,nn 
   write(funit,*) nuclidelist(i)%name,nuclidelist(i)%lambda,actualsource(i) 
  end do 
  close(funit) 

  else    ! particle transport calculations not performed
    print*, "Command line argument ", command
    print*, "Performing breakthrough calculations only"
    print*, "Breakthrough calculations using previous results in 'results.dat' "
  end if   ! run transport calculation
  
! *********************************************************************
! *****                    POST PROCESSOR                     *********
! *********************************************************************

  print*, "begin post processing"
  call post_process(asciiflag)
  print*, "completed post processing"

  print *, "normal termination of marfa" 
  
  stop 

! *********************************************************************

  contains 

! *********************************************************************
! *****               SUBROUTINES and FUNCTIONS                  ******
! *********************************************************************
    subroutine get_nuclides
 
       ! list of element names  
       character(len=10),  dimension(:), allocatable :: elementnames 
       character(len=10),  dimension(:), allocatable :: nucnames  
 
       logical :: ex 

       ! get list of elements  
       iunit=openread("nuclides.dat") 
       read(iunit,*) nelem
       allocate(elementnames(nelem)) 
       do i=1,nelem 
          read(iunit,*) elementnames(i) 
       end do 

       ! get a temporary list of nuclides
       read(iunit,*) nn
       allocate(nucnames(0:nn))
       do i=1,nn
          read(iunit,*) nucnames(i)
       end do
       nucnames(0)='NULL'  

       ! got the names, now rewind and reposition
       rewind(iunit)
       call skipheader(iunit) 
       do i=1,nelem+1
        read(iunit,*)
       end do

       ! get nuclide list
       read(iunit,*) nn
       allocate(nuclidelist(nn))
       do i=1,nn
         nuclidelist(i)=nuclide_(iunit, elementnames,nucnames,nn)
      end do

      close(unit=iunit)
      deallocate(nucnames)
      deallocate(elementnames) 


    end subroutine get_nuclides
!*********************************************************************
FUNCTION sample_dt(oldnuclide) result(dt) 
!*********************************************************************
!  this functions samples the decay time 
!  list of nuclide objects available globally 
!  input is index in that list 

 integer :: oldnuclide

 real(kind=dp) :: r , dt  

 call random_number(r)  

 dt = -log(r)/nuclidelist(oldnuclide)%lambda 

 end function sample_dt 
!**********************************************************************
end program marfa  



