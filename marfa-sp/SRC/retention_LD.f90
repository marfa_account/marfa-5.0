!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland). 
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


! *********************************************************************
! File Name        : retention_LD.f95
! Program Name     : marfa  
! Developed for    : SKB and POSIVA  
! Revision History : Version 3.1 Dec 2006  Scott Painter 
!                  :    marfa 3.2     Oct  2007
!                  :         Updated to include change in ret parameters during flow epochs
!                  :  marfa 3.2.1  Apr 2008 James Mancillas
!                  :         Update for format change to file "rocktypes.dat"
!                  :  marfa 3.2.3 May 2011 Scott Painter 
!                  :         Changes to accommodate new interpolation routine 
!                  :  marfa 3.2.5 Oct 2013 Scott Painter 
!                  :         Remove allocatable components from derived types  
! SRD Section      : Section 3 item 4  
! *********************************************************************


! module retention_LD_class 
! This module is the limited-diffusion model  
! Three public routines and one public structure are provided.
! Public routines are: 
!    init_retention_LD  a global initialization routine 
!    retention_LD_   a constructor for the class 
!    sample_rt_LD returns a sample from the retention time distribution 

module retention_LD_class

  use precision 
  use interpolation_class
  use retention_MD_class
  implicit none 

  private 
 
  public sample_rt_LD, sample_rt2_LD
  public init_retention_LD
  public retention_LD_ 
  public retention_LD 

   type retention_LD
     integer :: nstates 
     type(retention_MD), pointer :: ptr_to_MD 
     real(kind=dp), dimension(:,:), pointer :: kappa ! kappa variable  
     real(kind=dp), dimension(:,:), pointer :: ka    ! dist coef for surface  sorption [L]
     real(kind=dp), dimension(:), pointer :: kca !disp coef for colloids on surface [L] 
     real(kind=dp), dimension(:,:), pointer :: kcprime  ! dimensionless kd for sorp to colloids  
     real(kind=dp), dimension(:), pointer :: eta   ! normalized distance 
     character(len=10), dimension(:), pointer :: statelabel
   end type retention_LD


  type(interpolation2d) :: ffunc, fpntrt, frtrn  

  contains 
!**********************************************************************
    subroutine init_retention_LD(datadir) 
!**********************************************************************
     character(len=80) :: datadir 
     character(len=132) :: filename 
     integer :: il 

     write(filename, '(2a)') trim(datadir),"limdiff.dat" 
     il = len_trim(filename) 
     print *, 'opening ',filename(1:il) 
     ffunc = interpolation2d_(filename(1:il)) 

     write(filename,'(2a)' ) trim(datadir),"LD_table1.dat" 
     il = len_trim(filename) 
     print *,'opening ', filename(1:il) 
     fpntrt = interpolation2d_(filename(1:il))   ! penetration depth

     write(filename,'(2a)' ) trim(datadir),"LD_table2.dat" ! return time
     il = len_trim(filename) 
     print *,'opening ', filename(1:il) 
     frtrn = interpolation2d_(filename(1:il)) 
   
    end subroutine init_retention_LD
!**********************************************************************
    subroutine retention_LD_(aLD, iunit, kdunit, nelem) 

      type(retention_LD), pointer :: aLD
      type(retention_MD), pointer :: aMD
      integer :: iunit, kdunit, nelem 
      integer :: nstates, istate, ielem 
      integer :: e 
      real(kind=dp) :: rhob, delta,  mc 
      real(kind=dp), dimension(:), allocatable ::  Rm , kca , por, deff 
      real(kind=dp), dimension(:,:), allocatable ::  kd  , ka , kc 
      character(len=15) :: delimiter
      character(len=80) :: aline 

     allocate(aLD) 

     allocate(aLD%eta(nelem)) 
     allocate(por(nelem)) 
     allocate(deff(nelem)) 
     allocate(Rm(nelem))

     read(iunit,*) rhob, mc,delta ! element-independent parameters
     do ielem=1,nelem
        read(iunit,'(a80)')  aline 
        read(aline,*,IOSTAT=e) por(ielem),deff(ielem) ! time independent, element-dependent
     end do 
     aLD%eta=delta/deff 


     ! check for delimiter
!     read(kdunit,*) delimiter
!     if( trim(adjustl(delimiter)) .ne. 'kdbins' ) then
!       print *, ' error in rocktypes.dat'
!       print *, ' kdbins delimiter string not found'
!     end if

     read(kdunit,*) nstates
     aLD%nstates=nstates
     if(nstates .lt. 1) then
       print *, 'problem in kdfile. nstates=' , nstates
       stop
     end if


     allocate(aLD%kappa(nelem,nstates))
     allocate(aLD%ka(nelem,nstates))
     allocate(aLD%kcprime(nelem,nstates))
     allocate(aLD%statelabel(nstates))
     allocate(aLD%kca(nstates))
     allocate(kd(nelem,nstates))
     allocate(kc(nelem,nstates))

     do istate=1,nstates
     read(kdunit,*) aLD%statelabel(istate)
     read(kdunit,*) aLD%kca(istate) 
     do ielem=1,nelem
      read(kdunit,*) kd(ielem,istate),kc(ielem,istate), & 
                     aLD%ka(ielem,istate)
     end do
     end do


     ! calculate Kd
     do istate=1,nstates
        Rm = 1.0d0+kd(:,istate)*rhob/por
        aLD%kappa(:,istate) = sqrt(deff*por*Rm)
        aLD%kcprime(:,istate) = kc(:,istate)*mc !v3.2.5
     end do

     deallocate(kd)
     deallocate(kc)
     deallocate(Rm)
     deallocate(deff)
     deallocate(por)

     ! allocate and populate the MD object. 
     ! used for asymptotics when matrix is large  
     allocate(aLD%ptr_to_MD) 
     aMD => aLD%ptr_to_MD 
     allocate(aMD%kappa(nelem,nstates))
     allocate(aMD%ka(nelem,nstates))
     allocate(aMD%kcprime(nelem,nstates))
     allocate(aMD%statelabel(nstates))
     allocate(aMD%kca(nstates))
     aMD%kappa=aLD%kappa
     aMD%ka=aLD%ka 
     aMD%kcprime=aLD%kcprime
     aMD%statelabel=aLD%statelabel
     aMD%kca=aLD%kca

     end subroutine retention_LD_

!*****************************************************************
    function sample_rt_LD(aLD,elem,beta,r,epoch)  result(rt) 
!*****************************************************************

      type(retention_LD) :: aLD 
      integer :: elem,i, epoch 
      real(kind=dp) :: rt , r ,beta , eta ,r1,kappa , rtsurf 
      real(kind=dp) :: kcprime !v3.2.5  
      real(kind=dp) :: kaeff !v3.2.5 
      real(kind=dp) :: kca, kc , ka !working vars 

      
      eta = aLD%eta(elem)/beta  ! dimensionless eta 
      kcprime = aLD%kcprime(elem,epoch) 
      eta = eta*(1.0d0+kcprime) !v3.2.5  
      kappa = aLD%kappa(elem,epoch)*beta  ! dimensionless kappa 
      kappa = kappa/(1.0d0+kcprime) !v3.2.5  

      ! make sure variables are in range 
      ! function is well-behaved - no need to extrapolate 
   
      !eta = min(eta,ffunc%xmax) !3.2.3 
      if( eta .le. ffunc%xmax) then 


       if( eta .lt. ffunc%xmin ) then  !approximates equilibrium   3.2.3 
        rt = eta                           
       else 
        rt = interpolate(ffunc,eta,r)  !3.2.3  
       end if 

       rt = rt*kappa*kappa  ! note beta is in there  


       !v3.2.5 group below. 
       ka = aLD%ka(elem,epoch) 
       kca = aLD%kca(epoch) 
       kaeff = (ka + kca*kcprime)/(1.0d0+kcprime) 
       rt = rt + kaeff*beta ! add time due to surface sorption  

      else 
       rt = sample_rt_MD(aLD%ptr_to_MD,elem,beta,r,epoch) 
      end if 

    end function sample_rt_LD
!*****************************************************************
    function sample_rt2_LD(aLD,elem,remt,r, istate,nxtstate)  result(rt) 

      type(retention_LD) :: aLD 
      integer :: elem, istate,nxtstate 
      real(kind=dp) :: rt , r , remt, r1 
      real(kind=dp) :: z, kappa, eta, t0, norm_time

  
      ! normalize time

      eta = aLD%eta(elem)           ! eta 
      kappa = aLD%kappa(elem,istate)       ! kappa 
      t0 = (eta*kappa)**2.0

      norm_time=remt/t0 
 
      ! sample a penetration depth given a normalized time and quantile(r)      

      if(norm_time.gt.fpntrt%xmax)then !3.2.3 
           call random_number(z)
      else
         z=interpolate(fpntrt,norm_time,r) !3.2.3 
      end if

      ! sample a return time given a depth and a quantile

        call random_number(r1)
        rt=interpolate(frtrn,z,r1)  !3.2.3 

        eta = aLD%eta(elem)           ! eta 
        kappa = aLD%kappa(elem,nxtstate)       ! kappa 
        t0 = (eta*kappa)**2.0
        rt = t0*rt  ! denormalize  


    end function sample_rt2_LD
!*****************************************************************

end module retention_LD_class

