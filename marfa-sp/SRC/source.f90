
!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy.
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

! *********************************************************************
! File Name        : source.f95
! Program Name     : marfa  
! Developed for    : SKB and POSIVA  
! Revision History : version 3.1beta  June 2006 Scott Painter 
!                  : version 3.1betaE Dec 2006 SP  
!                  : version 3.1      Dec 2006 Scott Painter 
!                  : version 3.2.2    November 2009 Scott Painter
!                    SCR722 items 1, 6 and 7 
!                  : modified for new interpolation routine July 2011 
! *********************************************************************

! module source  
! This module is the source 
!  two public routines : 
!  init_source reads the source information 
!  sample_source provides a starting location and starting weight   
!  one public variable: 
!  npart is number of particles  
!  removed npart from this module V3.2.5 now must be command line argument   

! added importance weighting december 2006 

module source 

!  input is from source.dat 

use precision 
use io 
use particle_class
use interpolation_class 
use nuclide_class 
use streamline

implicit none 

private 

integer, dimension(:,:), allocatable        :: srctrj   ! range of trajecties associated with src
type(interpolation1d), dimension(:), allocatable :: srcobj   ! sources 
character(len=10),dimension(:), allocatable      :: srcid 

real(kind=dp),dimension(:),allocatable           :: cmprob   ! cumulative prob for selecting  
real(kind=dp),dimension(:),allocatable           :: temparr 
real(kind=dp),dimension(:),allocatable           :: wfac     ! importance weight each nuclide  

logical :: uniform_in_time !V3.2.2 

public sample_source , init_source 

contains 
!***********************************************************************
 subroutine init_source(nuclidelist) 
  ! this subroutine reads source information 
  ! user must specify cumulative mass released for each nuclide in the chain 
  !  zero release is  allowed for some nuclides 

  ! conversion converts Mol to Bq given the lambda in yr^-1
  ! using 365 day/yr * 24 hr/day * 3600 sec/hr
  ! Mol  6.022E23   conversion=1.9095636732E16

  integer :: nnuc,i,j,ndp ,nsources ,m ,isrc ,inuc ,iunit, junit ,msrc 
  integer :: ios, numwarnings=0 !V3.2.2 
  integer                                           :: index1, index2,tindex,total 
  type(nuclide), dimension(:)                       :: nuclidelist
  character(len=6)                                  :: unitname 
  character(len=80)   :: buffer ! V3.2.2  
  character(len=15) :: samplehow  ! V3.2.2 
  integer :: firstletter ! V3.2.2  

  character(len=60), dimension(:), allocatable :: pagebuff  

  real(kind=dp)                                     :: norm,dt2 , afac
  real(kind=dp),dimension(:),allocatable            :: time 
  real(kind=dp),dimension(:,:),allocatable          :: cmass ! cumulative source  
  real(kind=dp),dimension(:,:),allocatable          :: massr ! rate at which mass is released  
  real(kind=dp)                                     :: conversion=1.9095636732E16
  real(kind=dp) :: wf 

  logical :: actflag   ! set to true if source is in Bq/yr 
  logical :: ex 

  nnuc=size(nuclidelist)

!****************************************
  iunit=openread("source.dat")
  print *, 'opening source.dat' 
  read(iunit,*) unitname  

  ! V3.2.2 
  read(iunit,*) buffer  
  buffer=adjustl(buffer) 
  
  firstletter = iachar( buffer(1:1) )   
  if(firstletter .ge. 65 .and. firstletter .lt. 122  ) then 
   read(buffer,*) samplehow 
   read(iunit,*) buffer 
  end if 
  uniform_in_time = .FALSE. 
  if( samplehow(1:7) .eq. 'UNIFORM') then 
    uniform_in_time = .TRUE. 
    print *, 'sources will be sampled uniformly in time' 
  end if 
  read(buffer,*) nsources 
  ! end V3.2.2 

!****************************************

  select case(unitname) 
  case('bq','Bq', 'BQ') 
    actflag = .true. 
  case('mol', 'MOL', 'Mol') 
    actflag = .false. 
  case default 
    print *, 'source specified in invalid units', unitname 
    stop 
  end select 

!****************************************


  allocate( srcobj(nnuc*nsources) ) 
  allocate( cmprob(nnuc*nsources) ) 
  allocate( srcid(nsources) ) 
  allocate( srctrj(nsources,2) )

  m=0 
  
   isrc = 1 
   do msrc=1,nsources 
  
   read(iunit,*) srcid(isrc)                   ! read source name
   read(iunit,*) srctrj(isrc,1),srctrj(isrc,2) ! read trajectories associated with this source
   read(iunit,*) ndp                           ! read number of time steps of source

     allocate( time(ndp) )                
     allocate( massr(ndp,nnuc) ) 
     allocate( cmass(ndp,nnuc) ) 

     allocate( pagebuff(ndp+1)) 

   
       do i=1,ndp                                ! read in the source inventory
         read(iunit,*) time(i), massr(i,:)       !
       end do                                    !

!**************************
! ensure that the source has at least 1 valid trajectory
! if it does not the source is zeroed out so it doesn't contribute to
! the total source release

    if(srctrj(isrc,1).gt.srctrj(isrc,2))then
       print*,'Source ',isrc, 'has invalid trajectories assigned.'
       print*,'Trajectory range should be entered from low to high'
       stop
    end if

    if(srctrj(isrc,2).gt.num_trajectories())then
       print*,'Source ',isrc, 'has invalid trajectories assigned.'
       print*,'Number of trajectories are out of bounds'
       stop
    end if
 
  
     total=0 
     do tindex=srctrj(isrc,1),srctrj(isrc,2)
        if(tindex.gt.0)total=total+validate_trj(tindex)
     end do
     
    if(total.lt.1 .or. srctrj(isrc,1).le.0)then
      ! V3.2.2  
      if( numwarnings .lt. 3) then 
        print*, 'Warning Source ',srcid(isrc),' has no valid release path'
        numwarnings = numwarnings + 1 
        if( numwarnings .eq. 3) then 
           print *, ' 3 warnings about no release path have been issued ' 
           print *, ' further warnings will be suppressed' 
        end if   
      end if  

      deallocate(pagebuff) 
      deallocate(cmass) 
      deallocate(massr) 
      deallocate(time) 
      cycle !Vfp

      ! end V3.2.2 
      !do i=1,ndp
      !    massr(i,:)=0.0
      !end do
      !valid_source(isrc)=0
      !srctrj(isrc,1)=1
      !srctrj(isrc,2)=1

    end if


!***************************

   if( actflag ) then                              !  convert source data   
     do inuc=1,nnuc                                !  from Bq to mol if data
        afac = conversion*nuclidelist(inuc)%lambda !  it given in Bq
        massr(:,inuc) =  massr(:,inuc)/afac        !
     end do                                        !
   end if                                          



   ! sp added  wfac below: importance 12.05.2006
   ! if first,source  allocate and save a local copy of the weight 
   ! needed later for assigning the weight 
   if( .not. allocated(wfac)) then 
       allocate(wfac(nnuc)) 
       norm = 0.0d0 
       do j=1,nnuc 
        wfac(j) = nuclidelist(j)%importance 
        norm = norm + wfac(j) 
       end do 
       wfac = wfac/norm 
   end if 
 
   do j=1,nnuc 
   cmass(1,j) = 0.0 
   do i=2,ndp
    dt2 = (time(i)-time(i-1))/2.0d0 
    !cmass(i,j) = cmass(i-1,j) + (massr(i-1,j)+massr(i,j))*dt2 *wfac(j) 
    cmass(i,j) = cmass(i-1,j) + (massr(i-1,j)+massr(i,j))*dt2 
   end do 
   end do 


   do j=1,nnuc 

     m=m+1 

    ! error check on source  
    ! additional error checking happens in interpolation routines  
    if(maxval(cmass(:,j)) .ne. cmass(ndp,j) ) then 
      print *, 'cumulative source not monotonic: stopping' 
      print *, j, m, maxval(cmass(:,j)), cmass(ndp,j), maxloc(cmass(:,j))  
      stop 
    end if  

 
     if( cmass(ndp,j) .gt. 0.) then 
        norm=cmass(ndp,j) 
     else 
        norm=1.
     end if 

     if( uniform_in_time ) then 

      write(pagebuff(1),*) ndp
!sp v3.2.3 
!      write(pagebuff(2),*) time(ndp)
!      write(pagebuff(3),*) time(1)  
!      write(pagebuff(4),*) maxval(massr(:,j)) *wfac(j)/norm  
!      write(pagebuff(5),*) 0.0


      do i=1,ndp
        !write(pagebuff(i+1),'(2e12.5)') time(i),massr(i,j)/norm *wfac(j)
        write(pagebuff(i+1),'(2e12.5)') time(i),massr(i,j)
      end do


     else 

      write(pagebuff(1),*) ndp
!sp v3.2.3 
!      write(pagebuff(2),*) cmass(ndp,j)/norm 
!      write(pagebuff(3),*) 0.0  
!      write(pagebuff(4),*) time(ndp) 
!      write(pagebuff(5),*) time(1)  

      do i=1,ndp 
       !write(pagebuff(i+1),'(2e12.5)') cmass(i,j)/norm ,time(i) 
       write(pagebuff(i+1),'(2e12.5)') cmass(i,j),time(i) 
      end do 

     end if  


     !sp v3.2.3 shift to new interpolation routine 
     
     junit=findunit() 
     open(unit=junit,file="tmpfile") 
     do i=1,ndp+1 
       write(junit,*) pagebuff(i) 
     end do 
     rewind(junit) 
     srcobj(m) = interpolation1d_(junit) 
     close(unit=junit,status='delete') 
     !sp v3.2.3 



     ! calculate cumulative probability for selecting each 
      wf = wfac(j) 
      if( cmass(ndp,j) .le. tiny(1.0d0)) wf=0.0d0 
      if( m .eq. 1) then  
       cmprob(m) = wf
      else 
       cmprob(m) = cmprob(m-1) + wf
      end if

    end do  ! end nuclide j loop 

    deallocate(pagebuff) 
    deallocate(cmass) 
    deallocate(massr) 
    deallocate(time) 
 
   isrc = isrc + 1 
   end do  ! end source loop  

   nsources = isrc-1  


   close(iunit) 

   print *, 'initialized source module' 
   print *, nsources, ' sources found' 
   print *, 'number of active sources ',nsources

   if(nsources.lt.1)then
    print*,"No sources with valid release paths exist."
    print*,"Exiting ... "
    stop
   end if

   ! resize cmprob  
   allocate(temparr(nnuc*nsources))  
   temparr=cmprob/cmprob(nnuc*nsources) 
   deallocate(cmprob) 
   allocate(cmprob(nnuc*nsources) ) 
   cmprob= temparr 
   deallocate(temparr) 
   


  return 
 end subroutine init_source 
!**********************************************************************
 subroutine sample_source(apart,trjtag1,trjtag2,nnuc,npart)  
  integer                                   :: crnt_nuclide
  integer                                   :: nnuc,isrc,nsrc,jsrc 
  integer(kind=npk)                         :: npart !sp v3.2.5 
  type(particle)                            :: apart 
  real(kind=dp)                             :: time , r , weight 
  real(kind=dp)                             :: x , dt 
  real(kind=dp)                             :: norm 
  character(len=10)                         :: trjtag1  ! name of source
  integer                                   :: trjtag2  ! number of associated trajectory
  integer                                   :: imin, imax, itemp
   
  nsrc = size(cmprob) 

  ! select a nuclide from the cumulative probs 
  
  call random_number(r)
  do isrc=1,nsrc 
   if( r .lt. cmprob(isrc) ) exit 
  end do 

  if( isrc .eq. 1) then 
   norm = cmprob(isrc) 
  else 
   norm = cmprob(isrc)-cmprob(isrc-1) 
  end if 
  norm = norm*real(npart,dp) 

  ! V3.2.2 

  crnt_nuclide = mod(isrc-1,nnuc) + 1 

  call random_number(r)

  if( uniform_in_time) then 
    dt=(srcobj(isrc)%max1-srcobj(isrc)%min1)
    time = r*dt + srcobj(isrc)%min1  
    weight = interpolate(srcobj(isrc),time)*dt 
    weight = weight/norm 
  else 
    time = interpolate(srcobj(isrc), r*srcobj(isrc)%max1)  
    weight = 1.0d0  
    weight = weight * srcobj(isrc)%max1/norm
  end if 

  x=0.0 

  ! end V3.2.2 

  !sp V3.2.4  removed start segment assignment and next segment assignment down 
  !           to accommodate non-zero start segment 
  apart%time = time 
  apart%weight = weight 
  apart%nuclide = crnt_nuclide 
  apart%x = x 
  apart%xi = 0.0d0  ! starting position on next segment  

! activate the associated trajectory  
  jsrc = (isrc-1)/nnuc  + 1 

   trjtag1=srcid(jsrc)    ! trjtag1 contains name of source

   imin= srctrj(jsrc,1)   ! lower range of trajectories for source
   imax= srctrj(jsrc,2)   ! upper range of trajectories for source

! randomly select a possible trajectory

    do
       call random_number(r)
       trjtag2=int(r*(imax-imin+1)+imin)

!       call random_number(r)
!       itemp=nint(r*(imax-imin))

!       if(itemp.le.0)then
!         trjtag2=imin        ! trjtag2 containes number of trajectory
!       else                  ! assigned from the range available for 
!         trjtag2=imin+itemp  ! the source
!       end if

       !sp v3.2.4 
       apart%iseg = get_start_segment(trjtag2)  ! current segment  
       apart%nxtseg = apart%iseg+1  ! next segment  
       ! end v3.2.4 
  
       if(validate_trj(trjtag2).eq.1) exit  ! if the trajectory is valid return
                                            ! trjtag2 else resample
     end do

  return 
  end subroutine sample_source 
!**********************************************************************
end module source 

