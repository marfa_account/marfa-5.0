!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland) 
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


! *********************************************************************
! File Name        : retention_MD.f95
! Program Name     : marfa  
! Developed for    : SKB and POSIVA  
! Revision History : tdrw version 1.0 March 2006   Scott Painter
!                  : marfa version 3.1beta June 2006   Scott Painter 
!                  : marfa version 3.1     Dec  2006   Scott Painter 
!                  : marfa version 3.2     Oct  2007   James Mancillas
!                  :     Updated for ret parameters which change for each flow epoch
!                  :  marfa 3.2.1  Apr 2008 James Mancillas
!                  :         Update for format change to file "rocktypes.dat"
!                  :  marfa 3.2.5 Oct 2013 Scott Painter 
!                  :      Remove allocatable components from derived types 
! SRD Section      : Section 3 item 4 
! *********************************************************************


! module retention_MD_class 
! This module is the unlimited matrix diffusion retention model 
! Three public routines and one public structure are provided.
! Public routines are: 
!    init_retention_MD  a global initialization routine 
!    retention_MD_   a constructor for the class 
!    sample_rt_MD    returns a sample from the retention time distribution 


module retention_MD_class

  use precision 
  use interpolation_class
  implicit none 

  private 
 
  public sample_rt_MD,sample_rt2_MD 
  public init_retention_MD   
  public retention_MD_ 
  public retention_MD 

 
 !type retention_MD 
 !   real(kind=dp), dimension(:,:), pointer :: kappa  
 !end type retention_MD    

 
    type retention_MD 
       integer :: nstates 
       real(kind=dp), dimension(:,:), pointer :: kappa 
       real(kind=dp), dimension(:,:), pointer :: kcprime 
       real(kind=dp), dimension(:,:), pointer :: ka
       real(kind=dp), dimension(:), pointer :: kca
       character(len=10), dimension(:), pointer :: statelabel 
    end type retention_MD    


  integer :: nel
  type(interpolation1d) :: ffunc, fpntrt, frtrn  

  contains 
!**********************************************************************
    subroutine init_retention_MD(datadir) 

       ! reads globally defined data 
       character(len=80)  :: datadir 
       character(len=132) :: filename  
       integer :: il

       write(filename,'(2a)' ) trim(datadir),"ftable.dat" 
       il = len_trim(filename) 
       print *,'opening ', filename(1:il) 
       ffunc = interpolation1d_(filename(1:il))   

        write(filename,'(2a)' ) trim(datadir),"table2.dat" 
       il = len_trim(filename) 
       print *,'opening ', filename(1:il) 
       fpntrt = interpolation1d_(filename(1:il))   

       write(filename,'(2a)' ) trim(datadir),"table1.dat" 
       il = len_trim(filename) 
       print *,'opening ', filename(1:il) 
       frtrn = interpolation1d_(filename(1:il))   

       return 
    end subroutine init_retention_MD   
!**********************************************************************
    subroutine retention_MD_( aMD, iunit,kdunit, nelem ) 
    ! 3.2 made kappa dependent on flow period (epoch) 
    ! 3.2.5 construct kappa from physical variables 

    type(retention_MD), pointer :: aMD 
    integer :: iunit, kdunit, nelem 
    integer :: ielem, istate 
    integer :: nstates  
    integer :: e  
    real(kind=dp), dimension(:,:), allocatable ::  kd  
    real(kind=dp), dimension(:,:), allocatable ::  kc  
    real(kind=dp), dimension(:), allocatable   ::  Rm , por, deff 
    real(kind=dp) :: rhob , mc 
    character(len=80) :: aline 

     allocate(aMD) 

     allocate(por(nelem)) 
     allocate(deff(nelem)) 
     read(iunit,*) rhob, mc ! element-independent parameters
     do ielem=1,nelem
        read(iunit,'(a80)')  aline 
        read(aline,*,IOSTAT=e) por(ielem),deff(ielem) ! time independent, element-dependent
     end do 

     ! check for delimiter 
!     read(kdunit,*) delimiter  
!     if( trim(adjustl(delimiter)) .ne. 'kdbins' ) then 
!       print *, ' error in rocktypes.dat' 
!       print *, ' kdbins delimiter string not found' 
!     end if 

     read(kdunit,*) nstates
     aMD%nstates=nstates 
     if(nstates .lt. 1) then
       print *, 'problem in kdfile. nstates=' , nstates
       stop
     end if

     allocate(aMD%kappa(nelem,nstates))
     allocate(aMD%kcprime(nelem,nstates))
     allocate(aMD%ka(nelem,nstates))
     allocate(aMD%statelabel(nstates))
     allocate(aMD%kca(nstates))
     allocate(kd(nelem,nstates)) 
     allocate(kc(nelem,nstates)) 

!     do istate=1,nstates
!      read(kdunit,*) aMD%statelabel(istate)
!      read(kdunit,*) kd(:,istate) 
!      read(kdunit,*) kc(:,istate) !sorption colloids 
!     end do

     do istate=1,nstates 
     read(kdunit,*) aMD%statelabel(istate)
     read(kdunit,*) aMD%kca(istate) 
     do ielem=1,nelem 
      read(kdunit,*) kd(ielem,istate),kc(ielem,istate), & 
                     aMD%ka(ielem,istate) 
     end do
     end do 

     allocate(Rm(nelem)) 

     ! calculate Kd 
!     do ielem=1,nelem
!        Rm = 1.0d0+kd(ielem,:)*rhob/por(ielem) 
!        aMD%kappa(ielem,:) = sqrt(deff(ielem)*por(ielem)*Rm) 
!        aMD%kcprime(ielem,:) = kc(ielem,:)*mc !v3.2.5 
!     end do 
     do istate=1,nstates 
        Rm = 1.0d0+kd(:,istate)*rhob/por
        aMD%kappa(:,istate) = sqrt(deff*por*Rm) 
        aMD%kcprime(:,istate) = kc(:,istate)*mc !v3.2.5 
     end do

     deallocate(kd) 
     deallocate(kc) 
     deallocate(Rm) 
     deallocate(deff) 
     deallocate(por) 

     end subroutine retention_MD_ 
!**********************************************************************
    function sample_rt_MD(aobj, elem,beta,r,epoch)  result(rt) 
    ! 3.2
      type(retention_MD) :: aobj 
      integer :: elem,i, epoch 
      real(kind=dp) :: rt , r ,beta , foo , kappa 
      real(kind=dp) :: kcprime, kaeff,ka,kca  

      kappa = aobj%kappa(elem,epoch) 
      kappa = kappa/(1.0d0+aobj%kcprime(elem,epoch) )  

! function blows up at r=1.0 
!  but has known dependence there 
      if( r .ge. ffunc%max1) then 
        rt = ffunc%max2*((1.0d0-ffunc%max1)/(1.0d0-r))**2 
      else 
        rt = interpolate(ffunc,r) 
      end if 

!  kappa is grouping of constants (element dependent) 
!  could also sample these   
      foo = beta * kappa 
      rt = foo*foo/4.0d0 *rt 


      ka = aobj%ka(elem,epoch) 
      kca = aobj%kca(epoch) 
      kcprime=aobj%kcprime(elem,epoch) 
      kaeff = (ka + kca*kcprime)/(1.0d0+kcprime) 
      rt = rt + kaeff*beta ! add time due to surface sorption  
      
     end function sample_rt_MD
!**********************************************************************
     function sample_rt2_MD(aobj, elem,remt,r,istate,nxtstate)     result(rt) 
     ! 3.2 added epoch 
      type(retention_MD)          :: aobj 
      integer                     :: elem,i,istate,nxtstate 
      real(kind=dp)               :: z , rt , r , r1 
      real(kind=dp)               :: remt 
      real(kind=dp)               :: kappa,nextkappa 


      kappa = aobj%kappa(elem,istate)
      kappa = kappa/(1.0+aobj%kcprime(elem,istate)) 
      nextkappa = aobj%kappa(elem,nxtstate) 
      nextkappa = nextkappa/(1.0+aobj%kcprime(elem,nxtstate)) 
      

! sample penetration depth  
! function blows up at r=1.0 
      if( r .ge. fpntrt%max1) then 
        z = fpntrt%max2
      else 
        z = interpolate(fpntrt,r) 
      end if 

! return time  
! function blows up at r=1.0 
      call random_number(r1) 
      if( r1 .ge. frtrn%max1) then 
        rt = frtrn%max2
      else 
        rt = interpolate(frtrn,r1) 
      end if 

      rt = remt* z*z * rt  * (nextkappa/kappa)**2 
   
    end function sample_rt2_MD
!**********************************************************************
end module retention_MD_class


