!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy.
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


! *********************************************************************
! File Name        : streamline.f95  
! Program Name     : marfa  
! Developed for    : SKB and Posiva Oy  
! Revision History : Version 0.5  April 2006   Scott Painter
!                  : Version 3.1beta   June 2006   Scott Painter
!                  : Version 3.1       Dec  2006   Scott Painter
!                  : Version 3.1.1     Sept 2007   James Mancillas
!                  : 3.2               Nov  2007   James Mancillas
!                  :                               Scott Painter 
!                  : Version 3.2.1     April 2008  Scott Painter 
!                  :                   SCR716 Items 5 and 6 
!                  : Version 3.2.2     November 2009 Scott Painter 
!                                      SCR722 Item 3 
!                  : Version 3.2.3     Minor update  
!                  : Version 3.2.4     Allow for start segment to be anywhere 
!                                      on a trajectory 
!                  : Version 3.2.5     Removed allocatable comps from derived types 
! SRD Section      : Section 3 items 1 and 2 
! *********************************************************************


! module streamline 

! this module encapsulates steamline properties
! streamlines are made of several segments 
! segments are characterized by hydrodynamic variables 
!    and a pointer to a retention model 

! module also has several public methods 
!  init_streamline() reads streamline data 
!  advance_segment() gets the next segment properties  
!  scale_segment() is called upon reaching an epoch boundary 
!       and resets segment properties for new flow conditions 
!  check_forsplit() returns true if a splitting surface is reached  
!  sample_rt() returns retention time in a segment 
!     it determines the correct retention model and passes through to 
!     routine as determined by the appropriate retention module  


module streamline 

 use precision 
 use io 
 use particle_class 
 use hydrofacies
 use retention_MD_class  ! matrix diffusion  
 !use retention_SW_class
 !use retention_SWLD_class ! stagnant water, then matrix 
 use retention_ES_class  ! equilibrium sorption  
 use retention_LD_class  ! limited diffusion  
 use retention_TAB_class  ! tabular  

 implicit none 

 private 

 public check_for_rescalc 
 public init_streamline, advance_segment, check_forsplit, scale_segment
 public sample_rt , activate_trajectory, validate_trj, num_valid_trj 
 public sample_sw 
 public num_trajectories
 public epochbndr
 public sample_rt2
 public check_for_termination
 public get_start_segment  

 type segment 
   real(kind=dp) :: ds,tau0,beta0,cpmgradh 
   real(kind=dp), dimension(3) :: posi 
   type(hf),pointer :: ptr_to_hf 
   integer :: dir=0 
   character(len=15) :: faciesID !sp V3.2.1 changed 5 to 15 
   integer :: nstates ! 4amphos 
   integer, pointer, dimension(:) :: states ! 4amphos 
   real(kind=dp), pointer, dimension (:) :: t_statechange !4amphos 
 end type segment 


  type trajectory 
     type(segment), pointer, dimension(:) :: seg  
     integer :: numsegs 
     integer :: term_seg                    ! terminal segment
     integer :: start_seg                   !sp V3.2.3
     logical :: valid=.true.                ! connectflow use
     character(len=10) :: ID   
  end type trajectory 
  
  type old_traj_to_new
    integer, dimension(:), allocatable :: seg
  end type

  real(kind=dp), allocatable, dimension (:)        :: epoch, vscale
  real(kind=dp)                                    :: epochbndr
  integer                                          :: numepoch, crnt_epoch

  type(trajectory), dimension(:), allocatable,target  :: trjarr 
  type(old_traj_to_new), dimension(:), allocatable :: old_to_new_seg_list
  integer :: ntraj 

  type(trajectory), pointer :: crnt_trj

 contains 

!************************************************************
 function get_start_segment(itraj) result(start_seg) !sp 3.2.4 
   integer :: itraj, start_seg 
   start_seg = trjarr(itraj)%start_seg 
 end function get_start_segment !sp end 3.2.4 
!**********************************************************************
 function check_forsplit(apart) result(split) 

   ! this one currently does nothing 
   ! infrastructure for splitting is in the 
   ! main program and has been tested  
   ! add a proper check here to activate 

   logical ::  split 
   type(particle) :: apart 
  
   split = .false. 

 end function check_forsplit 
!********************************************************************** 
 subroutine init_streamline(nelem) 
 
  character(len=80)             :: datadir  
  character(len=80)             :: buffer  
  character(len=80)             :: filename 
  character(len=15)             :: faciesID  !sp V3.2.1 changed 5 to 15 

  integer                                           :: i ,nelem,j ,khf,nhf 
  integer                                           :: numsegs, nm1 
  integer                                           :: indxhf  ! hydrofacies index 
  integer                                           :: iunit   ! hydrofacies.dat funit
  integer                                           :: junit ! tmpfile 
  integer                       :: maxstates
 
  integer                       :: ix,idir,ndir 
  integer                       :: nmon 
  real(kind=dp)                 :: dpx,foo,theta,phi 
  real(kind=dp), parameter      :: deg2rad = 0.0174533 
  real(kind=dp), parameter      :: pi = 3.141592654

 
  real(kind=dp),dimension(3)    :: start,ecoord ,dvec,avec
  real(kind=dp)                 :: d2 
  real(kind=dp) :: channelfac 

  type(segment), pointer        :: seg   ! alias for convenience
  
  character(len=1)              :: firstletter
  integer                       :: ifirstletter
  logical                       :: option1  
  logical                       :: useDZs !sp v3.2.1
!****************************************************

  ! number of hydrofacies  
  ! hfset is public from hydrofacies module 
    nhf = size(hfset) 





  ! initialize the various retention models 


  ! first get the data directory 
  iunit=openread("rocktypes.dat") 
  read(iunit,'(a80)') datadir 
  close(iunit) 

  datadir=adjustl(datadir) 
  ! add final slashes if not present 
  i=len_trim(datadir) 
  if( index(datadir,'/') .gt. 0) then 
    if(datadir(i:i) .ne. '/') datadir(i+1:i+1)='/'    ! unix 
  else   
    if(datadir(i:i) .ne. '\') datadir(i+1:i+1)='\'    ! windows 
  endif 
   
  call init_retention_MD(datadir) 
  !call init_retention_SW(datadir)
  !call init_retention_SWLD(datadir) 
  call init_retention_ES(datadir)  
  call init_retention_LD(datadir)  
  call init_retention_TAB(datadir)  
  

  iunit=openread("trajectories.dat") 

!***********************************************************
! read next line to determine whether to read connectflow data
! or read data supplied in the current file.

  read(iunit,'(a80)') buffer 

  firstletter=buffer
  ifirstletter=iachar(firstletter)

  if(ifirstletter.ge.48 .and. ifirstletter.le.57)then
   read(buffer,*) ntraj
   option1=.true.
  else
   
   !sp V3.2.1 look for option to use Deformation Zones as rocktypes 
   filename=buffer 
   print *, 'using ', filename, ' for trajectories ' 
   useDZs = .false. 
   do 
    read(iunit,'(a80)',end=9999) buffer 
    if(trim(buffer) .eq. 'USE DEFZONES' ) exit 
   end do 
   useDZs = .true. 
   print *, 'Using DEFZONES' 
   9999 continue 
   !end V3.2.1 

   !sp V3.2.2 look for optional beta multiplier  
   close(iunit) 
   iunit=openread("trajectories.dat") 
   channelfac = 1.0d0 ! default 
   do 
    read(iunit,'(a80)',end=9998) buffer 
    if(index(buffer, 'CHANNELING') .ne. 0 ) exit 
   end do 
   ! if here then channeling factor keyword has been found 
   read(buffer(18:),*) channelfac 
   print *, 'Using channeling factor ', channelfac 

   9998 continue 
   !end V3.2.2 

    
   call getconnectflowdata(filename,useDZs,channelfac) !sp v3.2.1 useDZs added here 
   option1=.false.
  end if

!***********************************************************
  if(option1)then   !read trajectories from trajectories.dat                 
                    !else they are obtained from a specififed data file

  allocate(trjarr(ntraj)) 

  do j=1,ntraj 

    junit=findunit() !v3.2.3  
    open(unit=junit,file="tmpfile") 
    
    read(iunit,*) trjarr(j)%ID , start 

    ! count the number of segments 
    ! need an error check in there 
     numsegs=0 
     do 
       numsegs=numsegs+1 
       read(iunit,'(a80)') buffer  
       if(index(buffer,'END') .ne. 0) exit  
       write(junit,'(a80)') buffer !V3.2.3  
     end do 
     numsegs=numsegs-1 
 
    ! reposition to the beginning of the trajectory 
!    call positionto(trjarr(j)%ID,iunit) 

    trjarr(j)%numsegs = numsegs 
    trjarr(j)%term_seg = numsegs         !jm 11.02.07
    trjarr(j)%start_seg = 0              !sp V3.2.4 
    allocate(trjarr(j)%seg(numsegs)) 

    rewind(junit) !v3.2.3 
    do i=1,numsegs 
      
      seg => trjarr(j)%seg(i) ! alias for convenience 

      read(junit,*) ecoord, faciesID, seg%tau0, seg%beta0, seg%cpmgradh  
      seg%posi = ecoord 
     
      ! first assign the pointer to facies  
      do khf=1,nhf 
        if( faciesID .eq. hfset(khf)%faciesID ) exit  
      end do 

      if( khf .gt. nhf) then 
       print *, ' faciesID not found: seg ', i, 'traj ', j  
       stop 
      end if 

      seg%ptr_to_hf => hfset(khf)  

      ! now calculate the displacement size 
      dvec = ecoord-start  
      d2 = sum(dvec*dvec) 
      if( d2 .le. 0.) then 
        print *,'warning: zero length segment ', i
        print *,'resetting to 5 cm' 
        seg%ds = 0.05 
      else 
        seg%ds = sqrt(d2) 
      end if 


      ! if the facies is stochastic then 
      ! find closest direction from the direction set 
      if( seg%ptr_to_hf%stoch  ) then  

        dpx = 0.0 
        ndir = seg%ptr_to_hf%numdir 
        do idir=1,ndir 

          phi=seg%ptr_to_hf%phi(idir)
          theta=seg%ptr_to_hf%theta(idir)


          ! convert to radians 
          phi=phi*deg2rad 
          theta=theta*deg2rad 

          avec(1)=cos(theta)*cos(phi) 
          avec(2)=sin(theta)*cos(phi) 
          avec(3)=sin(phi) 
          foo = sum(dvec*avec) 
          if( foo .ge. dpx) then 
           ix = idir  
           dpx = foo 
          end if 

          ! check in opposite direction (symmetry) 
          theta=theta+pi 
          phi = -phi 
          avec(1)=cos(theta)*cos(phi) 
          avec(2)=sin(theta)*cos(phi) 
          avec(3)=sin(phi) 
          foo = sum(dvec*avec) 
          if( foo .ge. dpx) then 
           ix = idir  
           dpx = foo 
          end if 

        end do 

        if(ix .le. 0) then 
           print *, 'error in init_steamline' 
           print *, 'dir = ', ix , theta,phi 
           stop 
        end if 
        seg%dir = ix 
          
      end if 

      start = ecoord ! for next segment 

    end do 

    ! done with this trajectory. now check that end statement is 
    !    where it should be 
!    read(iunit,'(a80)') buffer  
!    if(index(buffer,'END') .eq. 0) then  
!     print *, 'error in init_streamline' , buffer 
!     print *, 'possibly more than one source with same name' 
!     stop 
!    end if 

    close(unit=junit,status='delete') 
      
  end do  ! end of trajectory loop 

  end if       ! end option1 true condition 

  close(iunit) 

! identify segment for resident concentration 

  iunit = findunit()  
  open(unit=iunit,file="monitored_list") 
  nmon=0 
  do j=1,ntraj 
    do i=1,trjarr(j)%numsegs 
      seg => trjarr(j)%seg(i) ! alias for convenience 
      if(seg%ptr_to_hf%rescalc) then 
       write(iunit,201) j,i, seg%posi
       nmon=nmon+1 
      end if 
    end do 
  end do 
  if(nmon .eq. 0) then 
    close(unit=iunit,status='delete') 
  else 
    close(iunit) 
  end if 
201 format(2I10,3E10.2) 

! end resident concentration updates 

!4amphos 
! get the chemistry state for each segment 
     maxstates = 1 
     do khf = 1, nhf 
       maxstates = max(maxstates, size( hfset(khf)%statelabel )) 
     end do 
     if( maxstates .eq. 1) then  
       call static_kd() 
       print *, 'static kds' 
     else 
       call get_kd_history("kdhistory.dat") 
       print *, 'found kd history' 
     end if 
    
     call  get_flow_changes("trajectories.dat")

! This is a good location to have user to invalidate trajectories
! only allows trajectories to be invalidated-- usr can not upgrade a trajectory to valid
! usr can only devaluate a trajectory
!

     call   get_valid_list("trajectories.dat")


  print *, 'initialized streamline module' 
  print *, ntraj, ' trajectories found' 
  print *, 'number of active trajectories ', num_valid_trj()


 end subroutine init_streamline 
!**********************************************************************
 subroutine advance_segment(apart, taubar, betabar, ell, ogparm) 
   
   ! returns average tau,beta for segment 
   !   also length and dispersivity for segment 
   !   handles a deterministic or stochastic segment 

   real(kind=dp) ::  xpos,ds   
   real(kind=dp) ::   taubar, betabar , ogparm, time  , disp  
   real(kind=dp) :: r,ell 
   integer :: i,iseg  ,idir,ivc,elem  
   integer :: istate, nstates 

   type(particle) :: apart 
   type(segment), pointer :: seg
   type(hf), pointer :: crnt_hf  , prvs_hf 
   type(segmentpool), pointer :: crnt_sp  
 
   logical :: stoch 


 ! record the previous hydrofacies 
 !  used later for check on a facies change  
   iseg = apart%iseg   
  
   if( iseg .gt. 0) prvs_hf => crnt_trj%seg(iseg)%ptr_to_hf  

 ! advance the segment counter 
   apart%iseg = apart%nxtseg 
   iseg=apart%iseg 


   seg => crnt_trj%seg(iseg)  

   crnt_hf => seg%ptr_to_hf    

! identify current epoch based on flow change 

   time= apart%time
   do i=1,numepoch
      if(time .lt. epoch(i)) exit
   end do
   crnt_epoch = i                ! shared in this module  
   epochbndr =epoch(crnt_epoch)  ! public 

!4amphos
! now time until chemistry changes 
   nstates=seg%nstates
   do istate=1,nstates
      if( time .lt. seg%t_statechange(istate)) exit
   end do
   istate = min(istate,nstates) 
   epochbndr = min( epochbndr, seg%t_statechange(istate) ) !public 



   stoch = crnt_hf%stoch 
   if( stoch ) then        ! stochastic block  

     ! direction of travel 
     idir = seg%dir 

     ! current velocity group  
     !  assign randomly for first segment or first segment in a facies 

     if( iseg .eq. 1 ) then                        ! first segment 
      call random_number(r) 
      ivc = floor( crnt_hf%numvc * r) + 1 
     else if( .not.associated(crnt_hf,prvs_hf)) then   ! facies change 
      call random_number(r) 
      ivc = floor( crnt_hf%numvc * r) + 1 
     else                                         ! use previous velocity group  
      ivc = apart%ivc  
     end if 

   ! can now point at appropriate segement pool 
     crnt_sp => crnt_hf%spset(idir,ivc) 

     ! sample a segment 
     call random_number(r) 
     i = floor( crnt_sp%nseg * r) +1 

     ! tau and beta 
     ! remember to scale by local gradient 
     taubar = crnt_sp%tau(i) 
     betabar = crnt_sp%beta(i) 
     taubar =   taubar*crnt_hf%gradh(idir)/seg%cpmgradh
     betabar = betabar*crnt_hf%gradh(idir)/seg%cpmgradh

     ! length of the displacement 
     ell = crnt_sp%ell(i) 

     ! next velocity group for the particle 
     apart%ivc = crnt_sp%next(i) 

     ! advance the segment index  
     ds = crnt_trj%seg(iseg)%ds 
     xpos = ds - apart%xi  !remaining distance on the segment  
     do 
       if( xpos .ge. ell .or. iseg .eq. crnt_trj%term_seg) exit 
       iseg = iseg + 1 
       ds = crnt_trj%seg(iseg)%ds 
       xpos = xpos+ds 
     end do  

     if( xpos .ge. ell) then      ! segment is not final one  
       apart%xi = ds - (xpos-ell)  
       apart%nxtseg= iseg         !sp 
     else                         ! segment is final one  
       apart%xi = 0.0d0 
       apart%nxtseg= iseg+1       !sp 
     endif 


     if(vscale(crnt_epoch) .le. 0.0) then
       betabar = 0.0
       taubar  = 0.0
     else
       betabar = betabar/vscale(crnt_epoch)
       taubar  = taubar/vscale(crnt_epoch)
     end if

   else  ! deterministic segment  

      betabar = seg%beta0
      taubar = seg%tau0


      ell=seg%ds

      if(vscale(crnt_epoch).ge.0)then
         apart%nxtseg = apart%nxtseg +1 
      else
         apart%nxtseg = apart%nxtseg -1
      end if  

      betabar = abs(betabar/vscale(crnt_epoch))
      taubar  = abs(taubar/vscale(crnt_epoch))
 
   end if 

   ! element 
   elem = apart%elem 
   disp = crnt_hf%alpha  !dispersivity 
   disp = disp*ell + taubar*crnt_hf%dm
   if( disp .gt. 1.0d-11) then 
     ogparm = ell*ell/disp 
   else 
     ogparm = 10000. !will cause dispersion to be skipped in main  
   endif 




   return  

 end subroutine advance_segment 
!**********************************************************************
 function sample_sw(apart, tau) result(rt) 
  
    type(segment), pointer :: seg 
    type(particle) :: apart 
    real(kind=dp) :: tau, rt , W, betaSW, r
    integer :: iseg,elem,epoch
    type(retention_LD), pointer :: aLD 

    iseg = apart%iseg  
    elem = apart%elem 
    seg => crnt_trj%seg(iseg) 

    W=seg%ptr_to_hf%W 
    if( W .le. 1.0d-6) then 
      rt = tau 
    else 
      nullify(aLD) 
      aLD => seg%ptr_to_hf%ptr_to_SW 
      betaSW = tau/W 
      call random_number(r) 
      epoch = 1
      rt = tau+sample_rt_LD(aLD,elem, betaSW, r, epoch) 
    end if 

 
 end function sample_sw
!**********************************************************************
 function sample_rt(apart, beta, r, tau) result(rt) 
  ! V3.2
  ! determines the active retention model for the segment 
  !  and passes through to corresponding retention module 

  real(kind=dp) :: beta ,r ,rt ,tau
  real(kind=dp) :: time !4amphos  
  integer :: istate,nstates !4amphos  
  type(particle) :: apart 
  type(retention_LD),pointer :: ptr_to_SW ! for surface water 

  type(retention_MD),pointer :: ptr_to_MD 
  !type(retention_SWLD),pointer :: ptr_to_SWLD 
  type(retention_ES),pointer :: ptr_to_ES
  type(retention_LD),pointer :: ptr_to_LD 
  type(retention_TAB),pointer :: ptr_to_TAB
  integer :: iseg ,elem 
  type(segment), pointer :: seg 


    iseg = apart%iseg  
    elem = apart%elem 

    seg => crnt_trj%seg(iseg) 

     !ptr_to_SW => seg%ptr_to_hf%ptr_to_SW

     ptr_to_MD => seg%ptr_to_hf%ptr_to_MD 
     ptr_to_ES => seg%ptr_to_hf%ptr_to_ES 
     !ptr_to_SWLD => seg%ptr_to_hf%ptr_to_SWLD 
     ptr_to_LD => seg%ptr_to_hf%ptr_to_LD 
     ptr_to_TAB => seg%ptr_to_hf%ptr_to_TAB

     !4amphos 
     time=apart%time 
     nstates=seg%nstates 
     do istate=1,nstates 
      if( time .lt. seg%t_statechange(istate)) exit 
     end do 
     istate = min(istate,nstates) 
     istate=seg%states(istate) 

    ! v3.2 added crnt_epoch to the call
    !4amphos removed epoch, replace with state 
    if( associated(ptr_to_MD) )  then 
      rt = sample_rt_MD(ptr_to_MD, elem, beta, r,istate) 
    else if( associated(ptr_to_ES) )  then 
      rt = sample_rt_ES(ptr_to_ES, elem, beta, r,istate) 
    !else if( associated(ptr_to_SW) )  then 
    !  rt = sample_rt_SW(ptr_to_SW, elem, beta, r,istate,tau) 
    !else if( associated(ptr_to_SWLD) )  then 
    !  rt = sample_rt_SWLD(ptr_to_SWLD, elem, tau,beta, r,istate) 
    else if( associated(ptr_to_LD) )  then 
      rt = sample_rt_LD(ptr_to_LD, elem, beta, r,istate) 
    else if( associated(ptr_to_TAB) )  then 
      rt = sample_rt_TAB(ptr_to_TAB, elem, beta, r,istate) 
    else 
      print *, " no associated retention model for segment ", iseg 
      stop 
    end if 
  
 end function sample_rt 
!********************************************************************** 
function sample_rt2(apart, remt, r) result(rt) 
  ! 3.2
  ! determines the active retention model for the segment 
  !  and passes through to corresponding retention module 

  real(kind=dp)                           :: remt ,r ,rt 
  type(particle)                          :: apart 
  integer                                 :: iseg, elem 
  integer :: istate, nstates, nxtstate  
  real(kind=dp) :: time 

  type(retention_LD),pointer :: ptr_to_SW

  type(retention_MD),pointer :: ptr_to_MD 
  type(retention_ES),pointer :: ptr_to_ES
  !type(retention_SW),pointer :: ptr_to_SW
  !type(retention_SWLD),pointer :: ptr_to_SWLD 
  type(retention_LD),pointer :: ptr_to_LD 
  type(retention_TAB),pointer :: ptr_to_TAB

  type(segment), pointer :: seg 
  logical :: chem_change 

    iseg = apart%iseg  
    elem = apart%elem 

    seg => crnt_trj%seg(iseg) 

     ptr_to_MD => seg%ptr_to_hf%ptr_to_MD 
     ptr_to_ES => seg%ptr_to_hf%ptr_to_ES
     !ptr_to_SW => seg%ptr_to_hf%ptr_to_SW 
     !ptr_to_SWLD => seg%ptr_to_hf%ptr_to_SWLD 
     ptr_to_LD => seg%ptr_to_hf%ptr_to_LD 
     ptr_to_TAB => seg%ptr_to_hf%ptr_to_TAB

     !4amphos 
     nstates=seg%nstates

     time=apart%time+remt !time out 
     do istate=1,nstates
      if( time .lt. seg%t_statechange(istate)) exit
     end do  
     ! watch for rare event when particle is hanging around at 1 billion years
     istate = min(istate,nstates) 
     nxtstate=seg%states(istate) 

     
     time=apart%time  !time in 
     do istate=1,nstates 
      if( time .lt. seg%t_statechange(istate)) exit
     end do  
     istate=min(istate,nstates) 
     istate=seg%states(istate) 

     !print *, istate, nxtstate, apart%time, apart%time+remt 

     !4amphos
     ! epochbndr = seg%t_statechange(istate) 

    ! v3.2 added crnt_epoch to the call 
    !4amphos changed crnt_epoch to istate 
    if( associated(ptr_to_MD) )  then 
      rt = sample_rt2_MD(ptr_to_MD, elem, remt,r,istate,nxtstate) 
    else if( associated(ptr_to_ES) )  then 
      rt = 0.0  ! sp 
    !else if( associated(ptr_to_SW) )  then 
    !  rt = sample_rt2_SW(ptr_to_SW, elem, remt,r,istate,nxtstate)
    !else if( associated(ptr_to_SWLD) )  then 
    !  rt = sample_rt2_SWLD(ptr_to_SWLD, elem, remt,r,istate,nxtstate)
    else if( associated(ptr_to_LD) )  then 
      rt = sample_rt2_LD(ptr_to_LD, elem, remt,r,istate,nxtstate) 
    else if( associated(ptr_to_TAB) )  then 
      rt = sample_rt2_TAB(ptr_to_TAB, elem, remt,r,istate,nxtstate) 
    else 
      print *, " no associated retention model for segment ", iseg 
      stop 
    end if 

 
 end function sample_rt2 
!**********************************************************************
 subroutine positionto(keyword,inunit)

  character(len=*) :: keyword
  character(len=80) :: buffer 
  character(len=10) :: key 
  integer :: ios,inunit 

  rewind(inunit)
  do
    read(inunit,'(a80)',end=1001) buffer
    read(buffer, *,iostat=ios) key
    if(ios .ne. 0) cycle
    if( key .eq. keyword) return
  end do

1001 print *, 'No ', keyword, ' Keyword:'

  print *, 'this cannot happen' 
  stop 

 end subroutine positionto
!**********************************************************************
 subroutine activate_trajectory( trjtag) 
! Modified subroutine to compare trjtag to ntraj

 integer :: trjtag 



 if(trjtag .gt. ntraj) then 
   print *, 'attempted to activate trajectory ',trjtag 
   print *, ntraj, ' trajectories defined '
   stop 
 end if 

  crnt_trj => trjarr(trjtag)

 end subroutine activate_trajectory 
!**********************************************************************
subroutine getconnectflowdata(filename,useDZs,channelfac)
!**********************************************************************
!  written by JFM 
!   review by SP 9/28/06 

!sp V3.2.1 useDZs logical variable added to arg list and below 
 logical                                             :: useDZs  

 character(len=80),intent(inout)                     :: filename
 character(len=512)                                  :: buffer  !sp v3.2.1 increased to allow long lines 
 character(len=15)                                   :: faciesID !sp v3.2.1 changed 5 to 15 
 character(len=15)                                   :: dzoneID !sp v3.2.1 changed 5 to 15 
 character(len=5)                                    :: STEPS,trjname 
 integer                                             :: iunit,i,j
 integer                                             :: tempntrj,realno
 integer                                             :: tempnumseg
 integer,dimension(4)                                :: idum !sp v3.2.3 changed size  
 real(kind=dp),dimension(3)                          :: start,ecoord,finish,dvec,avec
 type(trajectory),dimension(:),allocatable,target    :: temptrjarr
 
 real(kind=dp)                                       :: apt1, apt2,time0
 real(kind=dp)                                       :: tau0,beta0,gradH,ds
 real(kind=dp) :: channelfac ! V3.2.2 channeling factor 
 type(segment), pointer                              :: seg   
  
 integer                                             :: index1,index2,index3
 integer                                             :: counter, loop_count, seg_counter
 integer                                             :: boundaryflag
 integer                                             :: khf,nhf
 integer                                             :: firstletter
 logical                                             :: failed_read
 logical                                             :: oldversion 

 integer                                             :: ix,idir,ndir
 real(kind=dp)                                       :: dpx,foo,theta,phi
 real(kind=dp),parameter                             :: deg2rad=0.0174533
 real(kind=dp),parameter                             :: pi=3.141592654
 integer                                             :: zero_count   !jm 3.2.1 added parameter
! hfset is public from hydrofacies module 
    nhf = size(hfset) 
    iunit=openread(filename) 
  zero_count=0

! begin reading data from the file

    read(iunit,*) buffer   ! skip first line

    read(iunit,*) ntraj
 
    allocate(temptrjarr(ntraj))
    allocate(old_to_new_seg_list(ntraj))

!  begin looping through trajectories
    counter=0
      
    do index1=1,ntraj
      failed_read=.true.

      read(iunit,*,end=4321) trjname, realno, realno, time0, boundaryflag
      read(iunit,*,end=4321) start,finish
      read(iunit,*,end=4321) STEPS, tempnumseg 

!      temptrjarr(index1)%ID= '____'     ! connect flow does not provide this
      !4amphos 
      write( temptrjarr(index1)%ID,'(A4,i0.4)') 'PART',index1     ! connect flow does not provide this
      temptrjarr(index1)%numsegs= tempnumseg 
      allocate(temptrjarr(index1)%seg(tempnumseg))
      allocate(old_to_new_seg_list(index1)%seg(tempnumseg))

       if( boundaryflag .eq. 0)then
          temptrjarr(index1)%valid=.true.
       else
          temptrjarr(index1)%valid=.false.
       end if

      do index2=1,tempnumseg  

!sp V3.2.1 changes below SCR716 item 6 
!        read(iunit,*,end=4321)time0, ecoord, apt1,apt2,tau0,beta0,gradH,faciesID
        read(iunit,'(a)') buffer 
        oldversion=.true. 
        read(buffer,*,end=4320)time0, ecoord, apt1,apt2,tau0,beta0,gradH,faciesID,idum,dzoneID 
        oldversion=.false.  
4320    if( dzoneID .ne. 'None' .and. dzoneID(1:4) .ne. 'CAGE' .and. .not.oldversion .and. useDZs) then 
         faciesID=dzoneID 
        end if 
!end V3.2.1 changes 

        dvec=start-ecoord
        ds=sum((dvec)**2)
        if(ds .le. 0.0d0 .and. temptrjarr(index1)%valid )then
! sp 3.2.1 conditional removed around this block 
! jm 3.2.1 change
           if(zero_count.le.3)then
             if(zero_count.lt.3)then
             print *,'warning: zero length segment', index2,' in trajectory ',index1 
                  zero_count=zero_count+1
             else  
             print*, 'Several zero length warnings have been generated.'
             print*, 'Further warnings will be suppressed.' 
                  zero_count=zero_count+1
             end if
           end if
! end 3.2.1 change
            ds=0.05
! end 3.2.1 change 
        else
          ds=sqrt(ds)
        end if

        seg =>temptrjarr(index1)%seg(index2)         

      ! first assign the pointer to facies
      do khf=1,nhf
        if( faciesID .eq. hfset(khf)%faciesID ) exit
      end do

      if( khf .gt. nhf) then
       print *, ' faciesID ', faciesID,' not found: seg ', index2, 'traj ', index1 
       stop
      end if

        seg%ptr_to_hf => hfset(khf)
        seg%ds=ds
        seg%tau0=tau0
        seg%beta0=beta0*channelfac ! V3.2.2 channelfac multiplier added 
        ! ensure that for the ES model beta eq tau 
         if(associated(seg%ptr_to_hf%ptr_to_ES))seg%beta0=tau0   ! jm change 3.2.1
        seg%cpmgradH=abs(gradH)  ! sp added  
        seg%faciesID=faciesID

      ! if the facies is stochastic then
      ! find closest direction from the direction set
      if( seg%ptr_to_hf%stoch  ) then

        dpx = 0.0
        ndir = seg%ptr_to_hf%numdir
        do idir=1,ndir

          phi=seg%ptr_to_hf%phi(idir)
          theta=seg%ptr_to_hf%theta(idir)


          ! convert to radians
          phi=phi*deg2rad
          theta=theta*deg2rad

          avec(1)=cos(theta)*cos(phi)
          avec(2)=sin(theta)*cos(phi)
          avec(3)=sin(phi)
          foo = sum(dvec*avec)
          if( foo .ge. dpx) then
           ix = idir
           dpx = foo
          end if

          ! check in opposite direction (symmetry)
          theta=theta+pi
          phi = -phi
          avec(1)=cos(theta)*cos(phi)
          avec(2)=sin(theta)*cos(phi)
          avec(3)=sin(phi)
          foo = sum(dvec*avec)
          if( foo .ge. dpx) then
           ix = idir
           dpx = foo
          end if

        end do

        if(ix .le. 0) then
           print *, 'error in init_steamline'
           print *, 'dir = ', ix , theta,phi
           stop
        end if
        seg%dir = ix

      end if

        start=ecoord   ! set for next segment

      end do
       failed_read=.false.
    end do


   4321 continue
     if(failed_read)then
      print*,"The file ",trim(filename)," appears incomplete"
      print*,"Only found ",index1-1," complete trajectories"
      stop
     end if



! connect flow has some discrete to DFN transitions which produce unuseable segments
! some of the trajectory segments have zero residence time and need to be filtered off

         allocate(trjarr(ntraj))

    do index1=1,ntraj 

          trjarr(index1)%valid=temptrjarr(index1)%valid
          trjarr(index1)%ID=temptrjarr(index1)%ID
          trjarr(index1)%term_seg=0                        ! default terminal segment =0
          trjarr(index1)%start_seg=0                       ! default start  segment =0

! determine the number of useable segments  !
          seg_counter=0
       do index2=1,temptrjarr(index1)%numsegs
         if(temptrjarr(index1)%seg(index2)%tau0.gt.0.0d0)then
           seg_counter=seg_counter+1
         end if
       end do

       allocate(trjarr(index1)%seg(seg_counter))
       trjarr(index1)%numsegs=seg_counter 
       trjarr(index1)%term_seg=seg_counter
! copy  seg data to trjarr%seg and assign hf

      loop_count=0
      do index3=1,temptrjarr(index1)%numsegs 

        if(temptrjarr(index1)%seg(index3)%tau0.gt.0.0d0)then

             loop_count=loop_count+1

             trjarr(index1)%seg(loop_count)%ds    = temptrjarr(index1)%seg(index3)%ds
             trjarr(index1)%seg(loop_count)%tau0  = temptrjarr(index1)%seg(index3)%tau0
             trjarr(index1)%seg(loop_count)%beta0 = temptrjarr(index1)%seg(index3)%beta0
             trjarr(index1)%seg(loop_count)%cpmgradh = temptrjarr(index1)%seg(index3)%cpmgradh
             trjarr(index1)%seg(loop_count)%faciesID = temptrjarr(index1)%seg(index3)%faciesID
             trjarr(index1)%seg(loop_count)%dir = temptrjarr(index1)%seg(index3)%dir

             do khf=1,nhf 
               if( trjarr(index1)%seg(loop_count)%faciesID .eq. hfset(khf)%faciesID ) exit  
             end do 

             if( khf .gt. nhf) then 
               print *, ' faciesID not found: seg ', loop_count, 'traj ', index1 
                stop 
             end if 

             trjarr(index1)%seg(loop_count)%ptr_to_hf => hfset(khf)   
    
             old_to_new_seg_list(index1)%seg(index3) = loop_count
         else
             old_to_new_seg_list(index1)%seg(index3) = -1
         end if
      end do

    end do

   deallocate(temptrjarr)


    return
end subroutine getconnectflowdata
!**********************************************************************
SUBROUTINE  get_valid_list(filename)
INTEGER                                                :: iunit
CHARACTER(len=180)                                     :: aline
INTEGER                                                :: trj_num, term_seg , start_seg !sp 3.2.4 
CHARACTER(len=20)                                      :: string2, string3
CHARACTER(len=*)                                       :: filename
INTEGER                                                :: index1, index2, zero_count ! jm 3.2.1 changes added parameter
INTEGER                                                :: err2_count !sp 3.2.4 
integer :: e !sp 3.2.4 
LOGICAL,dimension(:), allocatable                      :: temp_valid

allocate (temp_valid(ntraj))
iunit= openread(filename)
string2="VALID TRAJECTORIES"
string3="END"


zero_count=0      ! jm 3.2.1 change
err2_count=0      !sp 3.2.4 change

    do 

        read(iunit,'(a80)',end=150) aline
 
        if( index(aline,string2).eq.0) cycle   ! look for another line  

        print*, "Using -- VALID TRAJECTORIES -- list"

        do index1=1,ntraj
           temp_valid(index1)=trjarr(index1)%valid
        end do    

        do index1=1,ntraj                    ! invalidates all trajectories
          trjarr(index1)%valid= .false.      ! 
        end do                               !

        do 
!          call skipheader(iunit)
          read(iunit,'(a80)',end=150) aline    !  get usr supplied list
          if(index(aline,string3) .ne. 0) exit   ! reached end 
          
          read(aline,*,IOSTAT=e) trj_num, term_seg, start_seg   !sp 3.2.4 start seg added    
          if( e .ne. 0) then 
            start_seg=0 
            read(aline,*,IOSTAT=e) trj_num, term_seg 
            if( e .ne. 0) then 
              term_seg=0 
              read(aline,*,IOSTAT=e) trj_num
              if( e .ne. 0) then 
                 print *, 'error in reading trajectory number' 
                 stop 
              end if 
            end if 
          end if 
 
          start_seg=start_seg-1 !sp 3.2.4 

          if(.not. temp_valid(trj_num)) cycle      ! validates only previously valid trajectory 
     
           trjarr(trj_num)%valid=.true.       

           if( trjarr(trj_num)%numsegs .lt. term_seg .or. term_seg .le. 0) then 
              trjarr(trj_num)%term_seg=trjarr(trj_num)%numsegs
! jm 3.2.1 change
            if(zero_count .le. 3)then
              if(zero_count .lt. 3)then
                print*,"       <<<<<WARNING>>>>>        "
                print*,"Trajectory ",trj_num, " has ", trjarr(trj_num)%numsegs, " segments."
                print*,"The user has specified the terminal segment as segment ", term_seg
                print*,"Setting terminating segment to segment ",trjarr(trj_num)%numsegs
                zero_count=zero_count+1
              else
                print*,"      <<<<WARNING>>>>>         "
                print*,"Multiple warnings have been issued for terminating segment assignments" 
                print*,"Further warnings will be suppressed"
                zero_count=zero_count+1
              end if 
            end if
! end 3.2.1 change
              term_seg = trjarr(trj_num)%numsegs 
           end if 
           trjarr(trj_num)%term_seg=term_seg 

!sp 3.2.4 
           trjarr(trj_num)%start_seg=start_seg 
           if( trjarr(trj_num)%numsegs .lt. start_seg .or. start_seg .lt. 0) then 
             trjarr(trj_num)%start_seg=0 
             if(err2_count .lt. 3)then
                print*,"       <<<<<WARNING>>>>>        "
                print*,"Trajectory ",trj_num, " has ", trjarr(trj_num)%numsegs, " segments."
                print*,"The user has specified the start segment as segment ", start_seg
                print*,"Setting starting segment to segment ",trjarr(trj_num)%start_seg 
                err2_count=err2_count+1
             else if( err2_count .eq. 3) then 
                print*,"      <<<<WARNING>>>>>         "
                print*,"Multiple warnings have been issued for start segment assignments" 
                print*,"Further warnings will be suppressed"
                err2_count=err2_count+1
             end if 
           end if 
!end 3.2.4 changes 

        end do

        exit 

   end do 

   150 continue

   deallocate(temp_valid)    
   return 

end subroutine get_valid_list
!**********************************************************************
function  validate_trj(index) result(state)
integer                                       :: index
integer                                       :: state

  if(trjarr(index)%valid)then
      state=1
  else
      state=0
  end if

end function validate_trj
!**********************************************************************
function num_valid_trj()  result(nvt)
integer                                      :: index,num,nvt                          
                                 
   num=size(trjarr)
   nvt=0
       do index=1,num
          if(trjarr(index)%valid)nvt=nvt+1
       end do
   return

end function num_valid_trj
!**********************************************************************
function num_trajectories()  result (num)
integer                              :: num
     num=ntraj
     return
end function num_trajectories
!**********************************************************************
 subroutine scale_segment(apart,timeout,tau,beta,deltaT,rt)
! 3.2  allow reverse flow conditions
! called upon reaching an epoch boundary
! resets tau and beta for new flow conditions

   type(particle)                                         :: apart

   type(segment), pointer :: seg !sp2.3.4 
   type(hf), pointer :: crnt_hf  !sp2.3.4  
   integer :: iseg !sp2.3.4 
   integer :: istate, nstates !4amphos 
   logical :: esflag !sp2.3.4 

     real(kind=dp)                                       :: tau, beta
     real(kind=dp)                                       :: timein, timeout 
     real(kind=dp)                                       :: deltaT,rt,frac
     integer                                             :: next

     !sp 2.3.4 different scaling for es and other retention models 
     iseg=apart%iseg 
     seg => crnt_trj%seg(iseg)  
     
     crnt_hf => crnt_trj%seg(iseg)%ptr_to_hf 
     esflag=.false. 
     if( associated(crnt_hf%ptr_to_ES) ) esflag=.true. 

      crnt_epoch=0 
      timein=apart%time 
      do
       crnt_epoch=crnt_epoch+1 
       if(epoch(crnt_epoch) .gt. timein) exit
      end do

      next=0 
      do
       next=next+1
       if(epoch(next) .gt. timeout) exit
      end do

      epochbndr =epoch(next)  ! public 
      !4amphos
      ! now time until chemistry changes
      nstates=seg%nstates
      do istate=1,nstates
      if( timeout .lt. seg%t_statechange(istate)) exit
      end do
      istate=min(nstates,istate) 
      epochbndr = min( epochbndr, seg%t_statechange(istate) ) !public

      if(vscale(next).gt.0)then
        if(esflag) then  !sp 3.2.4 
          frac=1.0d0-deltaT/rt  ! fraction remaining, moving forward
        else 
          frac=1.0d0-sqrt(deltaT)/sqrt(rt)  ! fraction remaining, moving forward
        end if 
      else
        if(esflag) then !sp 3.2.4  
          frac= deltaT/rt       ! fraction elapsed, moving backwards
        else 
          frac= sqrt(deltaT)/sqrt(rt)       ! fraction elapsed, moving backwards
        end if 
        apart%nxtseg=apart%iseg-1         ! retreating particle
      end if

      tau = frac*abs(tau*(vscale(crnt_epoch)/vscale(next)))    ! tau scaled and prorated
      beta = frac*abs(beta*(vscale(crnt_epoch)/vscale(next)))  ! beta scaled and prorated

      crnt_epoch=next   ! shared in this module 

end subroutine scale_segment
!**********************************************************************
SUBROUTINE  get_flow_changes(filename)
INTEGER                                               :: iunit,i,i1 
CHARACTER(len=180)                                    :: aline
CHARACTER(len=12),parameter                           :: string2="FLOW CHANGES" 
CHARACTER(len=*)                                      :: filename
INTEGER                                               :: nm1

iunit= openread(filename)


    nm1=0
    do  ! read line by line exit upon finding flow change or at end of file  

      read(iunit,'(a80)',end=150) aline

      i1 = index(aline,string2) 
      if( i1 .ne. 0) then ! usr has supplied a list of flow changes
        read(aline(i1+12:),*,end=150)  nm1
        print*, "Using -- FLOW CHANGE -- list"
        print*, "Number of flow changes:", nm1
        exit 
      end if

    end do 

   150 continue  ! end of file or numepoch 

      numepoch =nm1+1     ! number of constant velocity periods

      allocate(  epoch(numepoch))
      allocate( vscale(numepoch))

      ! data stored as end of epoch. This is the reason for the i-1 shift
      vscale(1)=1.00
      epoch(numepoch)=5.0e20  !maximum allowed simulation time
      do i=2,numepoch 
        read(iunit,*)epoch(i-1),vscale(i)
      end do

     close(iunit)

     return 

end subroutine get_flow_changes                                   
!*********************************************************************
function check_for_rescalc(apart) result(calc) 
 logical :: calc 
 integer :: iseg 
 type(particle) :: apart 
 type(segment), pointer :: seg 

 calc=.false. 

 iseg=apart%iseg 
 seg => crnt_trj%seg(iseg)  
 calc = seg%ptr_to_hf%rescalc 
! if( apart%iseg .eq. crnt_trj%monitored) calc=.true. 

end function check_for_rescalc 
!**********************************************************************
!*********************************************************************
FUNCTION check_for_termination(apart) result(last)
!**********************************************************************
! 3.2 added to allow for reverse flow 
LOGICAL                                      :: last
LOGICAL                                      :: stoch

type(particle)                               :: apart

      last = .false. 

      !sp v3.2.3 crnt_epoch has already been updated for next segment 
      ! simply check that nextseg .gt. 0 and .le. term_seg  
      if(apart%nxtseg .gt. crnt_trj%term_seg)last = .true.  ! terminal segment
      if(apart%nxtseg .eq. 0) last =.true. !first segment with negative velocity


end FUNCTION check_for_termination
!*********************************************************************
!*********************************************************************
! get kd history for each segment of each trajectory 
! file must be structured the same as the trajectories file 
!*********************************************************************
subroutine get_kd_history(filename ) 
character(len=*)  :: filename
integer :: iunit , ntraj1 , itraj, iseg, final_iseg, nsegs 
integer :: i, j 
character(len=10) :: ID   
character(len=10) :: flag 
character(len=10),dimension(:), allocatable :: cstates 
type(segment), pointer :: seg 

   
  iunit=openread(filename) 
  read(iunit,*) ntraj1 
  if( ntraj1 .ne. ntraj) then 
    print *, ' stopping. kdhistory and trajectories files are not consistent' 
    print *,  ntraj, ntraj1 
    stop 
  end if 

  do itraj=1,ntraj 
    read(iunit, *) ID 
    if(adjustl(ID) .ne. adjustl(trjarr(itraj)%ID) ) then 
       print *, ' stopping. kdhistory and trajectories files'  
       print *, ' have inconsistent trajectory names' 
       print *, adjustl(ID) , adjustl( trjarr(itraj)%ID ) 
       stop 
    end if 
    
    if (allocated(old_to_new_seg_list)) then
        nsegs = size(old_to_new_seg_list(itraj)%seg)
    else
        nsegs = trjarr(itraj)%numsegs
    end if
    
    do iseg=1,nsegs
    
      if (allocated(old_to_new_seg_list)) then
        final_iseg = old_to_new_seg_list(itraj)%seg(iseg)
      
        if (final_iseg == -1) then
          ! If the segment was skipped, skip the lines as well
          read(iunit,*)
          read(iunit,*)
          read(iunit,*)
          cycle
        end if
      else
        final_iseg = iseg
      end if
      
      seg => trjarr(itraj)%seg(final_iseg)
      read(iunit,*) seg%nstates 
      allocate( seg%states(seg%nstates) )  
      allocate( cstates(seg%nstates) ) 
      read(iunit,*) cstates 
      ! convert from characters to integer 
      do i=1,seg%nstates 
        do j=1,size( seg%ptr_to_hf%statelabel ) 
         if( cstates(i) .eq. seg%ptr_to_hf%statelabel(j) ) exit 
        end do 
        if( j .gt. size( seg%ptr_to_hf%statelabel )) then 
          print *, 'error in kdhistory string matching' 
          print *, cstates 
          print *, seg%ptr_to_hf%statelabel
          stop 
         end if 
        seg%states(i)= j 
      end do 
      deallocate(cstates) 
      
      allocate( seg%t_statechange(seg%nstates) )  
      read(iunit,*) seg%t_statechange(1:seg%nstates-1) 
      seg%t_statechange(seg%nstates)=5.0d20
    end do 
    read(iunit,*) flag 
    if( trim(adjustl(flag)) .ne. 'END') then 
      print *, ' no END flag in kdhistory.dat '  
      print *, itraj, ' ',  trim(adjustl(flag))  , iseg,nsegs 
      stop 
    end if 
  end do 
  

  close(iunit) 


end subroutine get_kd_history  

subroutine static_kd() 
integer :: itraj, iseg, nsegs 
type(segment), pointer :: seg 

   
  do itraj=1,ntraj 
    nsegs=trjarr(itraj)%numsegs 
    do iseg=1,nsegs 
      seg => trjarr(itraj)%seg(iseg) 
      seg%nstates=1 
      allocate( seg%states(seg%nstates) )  
      seg%states(1)= 1 
      allocate( seg%t_statechange(seg%nstates) )  
      seg%t_statechange(seg%nstates)=5.0d20 
    end do 
  end do 
  
end subroutine static_kd 

end module streamline 

