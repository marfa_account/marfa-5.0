!Copyright 2011.  Swedish Nuclear Fuel and Waste Management Company (SKB) and Posiva Oy (Finland). 
!All rights reserved.

!Redistribution and use in source and binary forms, with or without
!modification, are permitted provided that the following conditions are
!met:

!  Redistributions of source code must retain the above copyright notice,
!  this list of conditions and the following disclaimer.

!  Redistributions in binary form must reproduce the above copyright notice,
!  this list of conditions and the following disclaimer in the documentation
!  and/or other materials provided with the distribution.

!  Neither the names of the copyright holders, nor the names of its contributors may be
!  used to endorse or promote products derived from this software without
!  specific prior written permission.

!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
!AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
!THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
!CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
!EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
!PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
!OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
!WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
!OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
!ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

! *********************************************************************
! File Name        : particle_class.f95
! Program Name     : marfa  
! Developed for    : SKB and POSIVA  
! Revision History : tdrw version 1.0  March 2006   Scott Painter
!                  : marfa version 3.1beta June 2006  Scott Painter 
!                  : marfa version 3.1     Dec  2006  Scott Painter 
!                  : marfa version 3.1.1   Sept 2007  James Mancillas 
!                  : Version 3.2 November 2007 James Mancillas 
! SRD Section      : Section 3 
! *********************************************************************

! module particle_class 
! data structure for particle 
! v3.2 initializes ivc to 0 

module particle_class 


use precision 

private 
public particle 

type particle 
  logical :: splitable              ! eligible to split?  
  integer :: nuclide                ! current nuclide 
  integer :: elem                   ! current element 
  real(kind=dp) :: time             ! particle clock  
  real(kind=dp) :: x                ! position  along trajectory 
  real(kind=dp) :: weight           ! statistical weight  
  integer :: iseg                   ! current segment along trajectory  
  integer :: nxtseg                 ! next segment along trajectory  
  real(kind=dp) :: xi               ! starting position from beginning of next seg 
  integer :: ivc = 0                ! current velocity group (not used in deterministic mode)  
end type  particle 

end module particle_class 
