!results.md
!file created on: 20190908 at 055807.737
!MARFA Developer Version
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 500000
!aseed=           54       12528                                                        
      500000
           2
 A1          0.69299999999999995           250353
 B1          0.69299999999999995           249647
