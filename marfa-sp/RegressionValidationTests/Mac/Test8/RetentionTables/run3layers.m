(* to invoke type "math -script thisfile" *) 
(* or "MathKernel -script thisfile" *) 
(* use full filepath in file names *) 

density=2700.; 
delta1=0.01; (* size of layer 1 *) 
delta2=0.01; (* size of layer 2 *) 
delta3=0.0275;   (* size of layer 3 *) 
file1="./kd.dat"; (* kd, porosity, deff for layer 1  *) 
file2="./kd.dat"; (* layer 2 *) 
file3="./kd.dat"; (* layer 3  *) 
fracparms="./kc.dat"; (* fracture parameters *) 
dirname="../RetentionTables/" 

<<"/Users/spainter/Workstuff/SKB_WFO_Project/MARFA/Multilayer/ThreeLayerRetention.m" 
<<"/Users/spainter/Workstuff/SKB_WFO_Project/MARFA/Multilayer/3LayerNoInterface.m" 
