!results.md
!file created on: 20201127 at 142024.429
!MARFA Developer Version
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 1000000
!aseed=          735       17640                                                        
     1000000
           2
 A            6.9314700000000002E-005     1000000
 B            6.9314700000000002E-005           0
