!results.md
!file created on: 20201213 at 114439.605
!MARFA Developer Version
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 1000000
!aseed=          680      927520                                                        
     1000000
           2
 A1          0.69299999999999995           499670
 B1          0.69299999999999995           500330
