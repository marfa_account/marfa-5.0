!results.md
!file created on: 20200120 at 140726.439
!MARFA Developer Version
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 5000000
!aseed=          601      111786                                                        
     5000000
           2
 Anuc         9.4049999999999996E-005     5000000
 Bnuc         2.8759999999999999E-005           0
