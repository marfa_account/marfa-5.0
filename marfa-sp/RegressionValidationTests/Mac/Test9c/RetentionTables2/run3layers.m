(* to invoke type "math -script thisfile" *) 
(* or "MathKernel -script thisfile" *) 
(* use full filepath in file names *) 

dirname="/Users/scottpainter/WorkStuff/MARFA/marfa-dev_git/marfa-sp/RegressionValidationTests/Mac/Test9c/RetentionTables2/"
density=2700.; 
delta1=0.0025; (* size of layer 1 *) 
delta2=0.004; (* size of layer 2 *) 
delta3=0.11;   (* size of layer 3 *) 
file1=dirname <> "kd1.dat"; (* kd, porosity, deff for layer 1  *) 
file2=dirname <> "kd2.dat"; (* layer 2 *) 
file3=dirname <> "kd3.dat"; (* layer 3  *) 
fracparms=dirname <> "ka.dat"; (* in-fracture parameters, ka and kc, etc *)

<<"../../../../../AuxiliaryCodes/ThreeLayerDiffusion/ThreeLayerRetention.m" ; 
<<"../../../../../AuxiliaryCodes/ThreeLayerDiffusion/3LayerNoInterface.m" 

