!results.md
!file created on: 20201213 at 114254.135
!MARFA Developer Version
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 10000000
!aseed=          613     1382315                                                        
    10000000
           2
 A            6.9314700000000002E-005    10000000
 B            6.9314700000000002E-005           0
