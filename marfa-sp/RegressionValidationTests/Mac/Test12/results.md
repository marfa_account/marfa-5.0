!results.md
!file created on: 20190908 at 055753.490
!MARFA Developer Version
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 500000
!aseed=          807     2391948                                                        
      500000
           2
 Cs137        7.2800000000000000E-019      250050
 Sr90         7.6300000000000000E-019      249950
