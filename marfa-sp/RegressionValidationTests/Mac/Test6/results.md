!results.md
!file created on: 20201213 at 112539.933
!MARFA Developer Version
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 1000000
!aseed=          536     1142752                                                        
     1000000
           2
 A            1.0000000000000000E-003     1000000
 B            1.0000000000000000E-004           0
