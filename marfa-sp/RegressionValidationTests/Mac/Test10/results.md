!results.md
!file created on: 20190908 at 055739.161
!MARFA Developer Version
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 500000
!aseed=          941      199492                                                        
      500000
           2
 A            6.9314700000000001E-007      500000
 B            6.9314700000000002E-005           0
