!C:\Users\jsalglas\Desktop\arxius\MARFA\repositoris\marfa-dev_git\marfa-sp\RegressionValidationTests\Windows\Test12d\results.md
!file created on: 20210415 at 110023.822
!MARFA Developer Version
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 1000000
!aseed=          225      783225                                                        
     1000000
           2
 A1          0.693000000000000           500801
 B1          0.693000000000000           499199
