!C:\Users\jsalglas\Desktop\arxius\MARFA\repositoris\marfa-dev_git\marfa-sp\RegressionValidationTests\Windows\Test12b\results.md
!file created on: 20210415 at 105927.974
!MARFA Developer Version
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 1000000
!aseed=           90      250560                                                        
     1000000
           2
 Cs137       7.280000000000000E-019      500205
 Sr90        7.630000000000000E-019      499795
