!results.md
!file created on: 20200121 at 155343.020
!MARFA Developer Version
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 1000000
!aseed=          433      940909                                                        
     1000000
           2
 A            6.9314700000000002E-005     1000000
 B            6.9314700000000002E-005           0
