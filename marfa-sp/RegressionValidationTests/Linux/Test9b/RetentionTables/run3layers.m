(* to invoke type "math -script thisfile" *) 
(* or "MathKernel -script thisfile" *) 
(* use full filepath in file names *) 

density=2700.; 
delta1=0.0025; (* size of layer 1 *) 
delta2=0.004; (* size of layer 2 *) 
delta3=0.11;   (* size of layer 3 *) 
file1="./kd1.dat"; (* kd, porosity, deff for layer 1  *) 
file2="./kd2.dat"; (* layer 2 *) 
file3="./kd3.dat"; (* layer 3  *) 
dirname="../RetentionTables/" 

<<"../../../ThreeLayerDiffusion/ThreeLayerRetention.m" 
<<"../../../ThreeLayerDiffusion/3LayerNoInterface.m" 
