!results.md
!file created on: 20200121 at 155435.048
!MARFA Developer Version
!Migration Analysis of Radionuclides in the FAr field
!Developed by Scott Painter and James Mancillas
!Center for Nuclear Waste Regulatory Analyses
!Southwest Research Institute
!and Los Alamos National Laboratory
!Copyright 2011. Swedish Nuclear Fuel and Waste Management Company and Posiva Oy (Finland)
!npart= 1000000
!aseed=          832     1482624                                                        
     1000000
           2
 Cs137        7.2800000000000000E-019      500420
 Sr90         7.6300000000000000E-019      499580
